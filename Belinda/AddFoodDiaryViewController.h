//
//  AddFoodDiaryViewController.h
//  Belinda
//
//  Created by Nivendru on 03/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <AssetsLibrary/AssetsLibrary.h>

@protocol addfooddiaryprotocal
- (void)buttonTappeddelete:(int)indexpath;
- (void)buttonTappedsubmit;
@end

@interface AddFoodDiaryViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,SWRevealViewControllerDelegate>
{
    AppDelegate *app;
    CGRect screenBounds,storerect;
    CGPoint pointtrack,pointtrackcontentoffset;
    UIImagePickerController *ImagePicker; 
     UIImage *selectedimage;
     NSData *imageDataPicker;
    UIDatePicker *datePicker;
}
- (IBAction)cameraButton:(id)sender;
- (IBAction)galleryButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *uploadedImage;
@property (strong, nonatomic) IBOutlet UITextField *foodName;
@property (strong, nonatomic) IBOutlet UITextField *foodDesc;
- (IBAction)dateTaken:(id)sender;
- (IBAction)timeTaken:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *dateTakenLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeTakenLabel;
@property (strong, nonatomic) IBOutlet UIButton *datetakenbutton;
@property (strong, nonatomic) IBOutlet UIButton *timetakenbutton;

@property (strong, nonatomic) UIButton *activebutton;
@property (strong,nonatomic) UITextField *activeTextField;

@property (strong,nonatomic) NSMutableArray *fooddiarypageshowingdata;
@property int sendindex;

- (IBAction)didTapSubmit:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *submitbutton;
@property (strong, nonatomic) IBOutlet UIButton *btncamera;
@property (strong, nonatomic) IBOutlet UIButton *btngallery;
@property (strong, nonatomic) IBOutlet UILabel *uploadlabel;
- (IBAction)didTapUpdate:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *updatebutton;
@property (strong, nonatomic) IBOutlet UILabel *feellabel;
 @property (strong, nonatomic) IBOutlet UIImageView *smileyimageview;
@property (strong, nonatomic) IBOutlet UILabel *fooddiarylabel;
@property (strong, nonatomic) IBOutlet UITextField *feeltextfield;
@property (strong, nonatomic) IBOutlet UIView *innerviewoffeeling;
@property (strong, nonatomic) IBOutlet UIImageView *calendericon;
@property (strong, nonatomic) IBOutlet UIImageView *loadingicon;

@property (strong, nonatomic) IBOutlet UIImageView *downarrayimageview;

@property (assign) id <addfooddiaryprotocal> delegate;

@end
