//
//  shoppinglistViewController.h
//  Belinda
//
//  Created by Nivendru on 11/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Shoppinglistcell.h"

@interface shoppinglistViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    UIAlertView *alertx;
}
@property (strong, nonatomic) IBOutlet UITableView *aTableview;
- (IBAction)didTapaddingradients:(id)sender;

@property(strong,nonatomic)UIButton *tracksectiondelete;
@property(strong,nonatomic)UIButton *trackrowdelete;
@end
