//
//  Fooddiarylistingcell.m
//  Belinda
//
//  Created by Nivendru on 31/10/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "Fooddiarylistingcell.h"

@implementation Fooddiarylistingcell

- (void)awakeFromNib {
    // Initialization code
    
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight |    UIViewAutoresizingFlexibleWidth;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
