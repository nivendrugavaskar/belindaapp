//
//  SidebarViewController.m
//  SidebarDemo
//
//  Created by Simon on 29/6/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "SidebarViewController.h"
#import "SWRevealViewController.h"
#import "essentialGoalListViewController.h"



@interface SidebarViewController ()
{

    NSIndexPath *lastindexpath;
    AppDelegate *app;
    CGRect screenBounds;
    
    BOOL notchangeindexcolor;
    
}

@end

@implementation SidebarViewController
{
    NSArray *menuItems;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
     [super viewDidLoad];
     app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    // new code
    notchangeindexcolor=FALSE;
    
    
     screenBounds = [[UIScreen mainScreen] bounds];
    if(screenBounds.size.height==480||screenBounds.size.height==568)
    {
        self.tableView.scrollEnabled=YES;
    }
    else
    {
       self.tableView.scrollEnabled=NO;
    }
    
    
    menuItems = @[@"Home",@"FoodDiary",@"Assessment", @"Goals", @"Results", @"ShoppingList", @"Recipes", @"MyInfo",@"Settings",@"Contact",@"Feedback",@"Logout"];
    //self.tableView.backgroundColor=[UIColor colorWithRed:220/255.0f green:25/255.0f blue:124/255.0f alpha:1.0f];
     self.tableView.backgroundColor=[UIColor colorWithRed:33/255.0f green:32/255.0f blue:32/255.0f alpha:1.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [menuItems objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.contentView.backgroundColor=[UIColor colorWithRed:33/255.0f green:32/255.0f blue:32/255.0f alpha:1.0];
    if(!lastindexpath)
    {
    if(indexPath.row==0)
    {
        
         cell.contentView.backgroundColor = [UIColor darkGrayColor];
         lastindexpath=indexPath;
        
    }
    }
    else
    {
       // NSLog(@"%ld",(long)lastindexpath.row);
        if(lastindexpath.row==indexPath.row)
        {
       cell.contentView.backgroundColor = [UIColor darkGrayColor];
        }
        else
        {
         cell.contentView.backgroundColor = [UIColor colorWithRed:33/255.0f green:32/255.0f blue:32/255.0f alpha:1.0];
        }
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath   *)indexPath
{
    // new code
    
    if(!lastindexpath)
    {
        lastindexpath=0;
    }
    if(!notchangeindexcolor)
    {
    if (![indexPath compare:lastindexpath] == NSOrderedSame)
    {
        [tableView cellForRowAtIndexPath:lastindexpath].contentView.backgroundColor = [UIColor colorWithRed:33/255.0f green:32/255.0f blue:32/255.0f alpha:1.0];
    }
    [tableView cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor darkGrayColor];
    lastindexpath=indexPath;
    }
    
    if(indexPath.row==11)
    {
        [self webservicelogout];
    }

    
}

-(void)webservicelogout
{
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    if(!app->devicetokenstring)
    {
       app->devicetokenstring=@"";
    }
    
    NSString *path =[NSString stringWithFormat:@"%@1473",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",@"ios",@"devicetype",app->devicetokenstring,@"devicetoken",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Loggingout.."];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"error"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
         }
         else
         {
            [self performSegueWithIdentifier:@"seguetoviewcontroller" sender:self];
         }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [SVProgressHUD dismiss];
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
    
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    //seguetoviewcontroller
    // Set the title of navigation bar by using the menu items
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    UINavigationController *destViewController = (UINavigationController*)segue.destinationViewController;
    destViewController.title = [[menuItems objectAtIndex:indexPath.row] capitalizedString];
    
//    // Set the photo if it navigates to the PhotoView
//    if ([segue.identifier isEqualToString:@"showPhoto"]) {
//        PhotoViewController *photoController = (PhotoViewController*)segue.destinationViewController;
//        NSString *photoFilename = [NSString stringWithFormat:@"%@_photo.jpg", [menuItems objectAtIndex:indexPath.row]];
//        photoController.photoFilename = photoFilename;
//    }
    
    // new code
    if([destViewController isKindOfClass:[essentialGoalListViewController class]])
    {
        [self checkassesmentdaytodayservice:segue];
    }
    else
    {
    notchangeindexcolor=FALSE;
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] )
    {
        //made true customappear
        app->checkcustompush=TRUE;
        
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc)
        {
            
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
            [navController setViewControllers: @[dvc] animated: NO ];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
        
    }
    else
    {
        app->statusactive=FALSE;
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logOut];
        NSUserDefaults *credentialdefault=[NSUserDefaults standardUserDefaults];
        [credentialdefault setValue:nil forKey:@"LoginType"];
        [credentialdefault setValue:nil forKey:@"appid"];
        [credentialdefault synchronize];
        
        // universal set in case of logout
        app->loginType=@"";
    }
}
}

-(void)hudshow
{
     [SVProgressHUD showWithStatus:@"Checking For Assessment.."];
}

#pragma check on same date any assesment is created or not?
-(void)checkassesmentdaytodayservice:(UIStoryboardSegue *)segue
{
//    NSString *path =[NSString stringWithFormat:@"%@1309",app->parentUrl];
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",nil];
//    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
//    [SVProgressHUD showWithStatus:@"Checking Your Assesment ..."];
//    [manager POST:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
//     {
//         
//         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
//         NSLog(@"%@",responseStr);
//         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
//         NSLog(@"%@",dict);
//         //here is place for code executed in success case
//         if([[dict valueForKey:@"status"]integerValue]==0)
//         {
//             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"error"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//             [alert show];
//             [SVProgressHUD dismiss];
//             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
//            // [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
//             
//             // new code
//              [self.activetableview cellForRowAtIndexPath:lastindexpath].contentView.backgroundColor = [UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
//             [self.activetableview cellForRowAtIndexPath:notchangeindex].contentView.backgroundColor = [UIColor colorWithRed:33/255.0f green:32/255.0f blue:32/255.0f alpha:1.0];
//             
//         }
//         else
//         {
//             //made true customappear
//             notchangeindexcolor=TRUE;
//             app->checkcustompush=TRUE;
//             SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
//             swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc)
//             {
//                 
//                 UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
//                 [navController setViewControllers: @[dvc] animated: NO ];
//                 [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
//             };
//         }
//         
//         
//     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
//         
//         //here is place for code executed in success case
//         [SVProgressHUD dismiss];
//         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
//         NSLog(@"Error: %@", [error localizedDescription]);
//         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//         [alert show];
//     }];
    
    NSString *post =@"";
    post=[NSString stringWithFormat:@"userid=%@",app->userid];
    NSString *url;// [NSString stringWithFormat:@"action=%@&city=%@&apartment=%@",@"",txtUsrName.text,txtPwd.text,txtFirstname.text,txtLastname.text,txtEmail.text,txtPhone.text];
    //NSlog(@"PostString=%@",post);
    //url=@"http://gaschecker.co.uk/gas-engineers/xmlv2/user_details.php";
    url=[NSString stringWithFormat:@"%@1309",app->parentUrl];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    // synchoronous call
    NSURLResponse* response;
    NSError* error = nil;
    // hud start
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [self performSelectorInBackground:@selector(hudshow) withObject:self];
   
    NSData* result = [NSURLConnection sendSynchronousRequest:request  returningResponse:&response error:&error];
    NSString *tempresponse=[[NSString alloc]initWithData:result encoding:NSUTF8StringEncoding];
    if(tempresponse)
    {
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:result options:NSJSONReadingAllowFragments error:NULL];
    if([[dict valueForKey:@"status"]integerValue]==0)
    {
         notchangeindexcolor=TRUE;
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"error"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag=100;
        [alert show];
           
    }
    else
    {
        
                 notchangeindexcolor=FALSE;
                 app->checkcustompush=TRUE;
                 SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
                 swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc)
                 {
    
                     UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
                     [navController setViewControllers: @[dvc] animated: NO ];
                     //[self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
                 };
             }
    [SVProgressHUD dismiss];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
    else
    {
        [SVProgressHUD dismiss];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                 NSLog(@"Error: %@", [error localizedDescription]);
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
     [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==100)
    {
         [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    }
}

#pragma mark - RowSelectionDelegate Method
-(void)selectRowWithHeading:(int)index
{
    NSLog(@"THE CURRENT SELECTION IS: %d", index);
    lastindexpath=[NSIndexPath indexPathForRow:index inSection:0];;
   // NSLog(@"%ld",(long)lastindexpath.row);
   // self.tableView.delegate=self;
   [self.tableView reloadData];
}
@end
