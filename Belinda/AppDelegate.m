//
//  AppDelegate.m
//  Belinda
//
//  Created by Nivendru on 16/10/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

-(void)allocation
{
    
    NSUserDefaults *devicetoken=[NSUserDefaults standardUserDefaults];
    if(!userid)
    {
        userid=[[NSString alloc]init];
        if([devicetoken valueForKey:@"appid"])
        {
            userid=[[devicetoken valueForKey:@"appid"] mutableCopy];
        }
        else
        {
            userid=@"";
        }
         //[credentialdefault setValue:app->userid forKey:@"appid"];
    }
    else
    {
        if([devicetoken valueForKey:@"appid"])
        {
            userid=[[devicetoken valueForKey:@"appid"] mutableCopy];
        }
        else
        {
            userid=@"";
        }
    }
    if(!clickedstatusgoal)
    {
        clickedstatusgoal=[[NSString alloc]init];
    }
    if(!loginType)
    {
        loginType=[[NSString alloc]init];
    }
    if(!devicetokenstring)
    {
        devicetokenstring=[[NSString alloc]init];
        if([devicetoken valueForKey:@"DeviceToken"])
        {
            devicetokenstring=[[devicetoken valueForKey:@"DeviceToken"] mutableCopy];
        }
        else
        {
            devicetokenstring=@"";
        }
    }
    else
    {
       
        if([devicetoken valueForKey:@"DeviceToken"])
        {
        devicetokenstring=[[devicetoken valueForKey:@"DeviceToken"] mutableCopy];
        }
        else
        {
         devicetokenstring=@"";
        }
        
    }
//    if(!pushdictionary)
//    {
//        pushdictionary=[[NSMutableDictionary alloc]init];
//    }
//    // testing purpose
//    NSUserDefaults *default1=[NSUserDefaults standardUserDefaults];
//    _testingpurposearray=[[default1 valueForKey:@"globalarray"] mutableCopy];
    if(!_testingpurposearray)
    {
        _testingpurposearray=[[NSMutableArray alloc]init];
    }
    // false by default
    checkcustompush=FALSE;
    fromregistration=FALSE;
    
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    [NSThread sleepForTimeInterval:1.0];
    // hide for testing
   // parentUrl=@"http://dev.businessprodemo.com/belindasbootcamps/live1/?page_id=";
   // parentUrl=@"http://test.businessprodemo.com/belindasbootcamps/php/?page_id=";
     parentUrl=@"http://belindacarusi.com/?page_id=";
    // parentUrl=@"http://dev.businessprodemo.com/belindasbootcamps/phpint/?page_id=";
    //http://test.businessprodemo.com/belindasbootcamps/php/wp-admin
    //http://192.168.1.248/belindasbootcamps/php/
    [self allocation];
    if(application.applicationIconBadgeNumber>0)
    {
        [self badgeservice];
    }
    
#pragma for ios 8 compatability
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                             |UIRemoteNotificationTypeSound
                                                                                             |UIRemoteNotificationTypeAlert) categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    else
    {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [application registerForRemoteNotificationTypes:myTypes];
    }
    
    if(launchOptions)
    {
        if(application.applicationIconBadgeNumber>0)
        {
            [self badgeservice];
        }
        
        pushdictionary=[[launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey] mutableCopy];

    }
    
    
    return YES;

}

#pragma for ios8 ans pushwork
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSLog(@"My token is: %@", deviceToken);
    NSString *tokenString = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    tokenString = [tokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
     NSLog(@"My tokenstring is: %@", tokenString);
    // new code
     devicetokenstring=tokenString;
    NSUserDefaults *devicetokensave=[NSUserDefaults standardUserDefaults];
    [devicetokensave setValue:tokenString forKey:@"DeviceToken"];
    [devicetokensave synchronize];
   
    //[self.appdelegate sendtoken:devicetokenstring];
    
//    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"DeviceToken" message:tokenString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
}

// new code
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==100)
    {
        //new code
        NSUserDefaults *default1= [NSUserDefaults standardUserDefaults];
        NSString *loginType1=[default1 valueForKey:@"LoginType"];
        if([loginType isEqualToString:@""]||!loginType1)
        {
            pushdictionary=nil;
        }
        else
        {
            UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SWRevealViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"Drawer"];
            [(UINavigationController*)self.window.rootViewController pushViewController:ivc animated:NO];
        }
    }
    else if(alertView.tag==101)
    {
        statusactive=FALSE;
        // new code
       
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logOut];
         NSUserDefaults *credentialdefault=[NSUserDefaults standardUserDefaults];
        [credentialdefault setValue:nil forKey:@"LoginType"];
        [credentialdefault setValue:nil forKey:@"appid"];
        [credentialdefault synchronize];
        
        // universal set in case of logout
        loginType=@"";
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
        [(UINavigationController*)self.window.rootViewController pushViewController:vc animated:NO];
    }
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    pushdictionary=[userInfo mutableCopy];
    NSString *alertstring,*titlestring;
//    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"didrecievecall" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
    // new code to send user id to service
    NSUserDefaults *devicetoken=[NSUserDefaults standardUserDefaults];
    if(!userid)
    {
        userid=[[NSString alloc]init];
        if([devicetoken valueForKey:@"appid"])
        {
            userid=[[devicetoken valueForKey:@"appid"] mutableCopy];
        }
        else
        {
            userid=@"";
        }
        //[credentialdefault setValue:app->userid forKey:@"appid"];
    }
    else
    {
        {
            if([devicetoken valueForKey:@"appid"])
            {
                userid=[[devicetoken valueForKey:@"appid"] mutableCopy];
            }
            else
            {
                userid=@"";
            }
        }
    }
    if(!devicetokenstring)
    {
        devicetokenstring=[[NSString alloc]init];
        if([devicetoken valueForKey:@"DeviceToken"])
        {
            devicetokenstring=[[devicetoken valueForKey:@"DeviceToken"] mutableCopy];
        }
        else
        {
            devicetokenstring=@"";
        }
    }
    else
    {
        
        if([devicetoken valueForKey:@"DeviceToken"])
        {
            devicetokenstring=[[devicetoken valueForKey:@"DeviceToken"] mutableCopy];
        }
        else
        {
            devicetokenstring=@"";
        }
        
    }
    // end
    if(application.applicationIconBadgeNumber>0)
    {
        
      // parentUrl=@"http://dev.businessprodemo.com/belindasbootcamps/live1/?page_id=";
       // parentUrl=@"http://test.businessprodemo.com/belindasbootcamps/php/?page_id=";
       // http://dev.businessprodemo.com/belindasbootcamps/php/?page_id=1655
       // parentUrl=@"http://192.168.1.248/belindasbootcamps/php/?page_id=";
        parentUrl=@"http://belindacarusi.com/?page_id=";
        [self badgeservice];
    }
    if ( application.applicationState == UIApplicationStateActive )
    {
        // testing
        if([[[pushdictionary valueForKey:@"aps"] valueForKey:@"type"]isEqualToString:@"goal"])
        { // app was already in the foreground
           
            titlestring=@"Goal Remainder";
        }
        else if([[[pushdictionary valueForKey:@"aps"] valueForKey:@"type"]isEqualToString:@"assessment"])
        {
           titlestring=@"Assessment Remainder";
        }
        else if([[[pushdictionary valueForKey:@"aps"] valueForKey:@"type"]isEqualToString:@"food"])
        {
            titlestring=@"Food-diary Remainder";
        }
        alertstring=[[pushdictionary valueForKey:@"aps"] valueForKey:@"alert"];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:titlestring message:alertstring delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag=100;
        [alert show];
        
    }
    else
    {
//            // app was just brought from background to foreground
//#pragma real to check authenticate
//        pushdictionary=[userInfo mutableCopy];
//        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        ViewController *mvc = [storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
//        [(UINavigationController*)self.window.rootViewController pushViewController:mvc animated:YES];
//
        //new code
        NSUserDefaults *default1= [NSUserDefaults standardUserDefaults];
        NSString *loginType1=[default1 valueForKey:@"LoginType"];
        if([loginType isEqualToString:@""]||!loginType1)
        {
            pushdictionary=nil;
        }
        else
        {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SWRevealViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"Drawer"];
        [(UINavigationController*)self.window.rootViewController pushViewController:ivc animated:NO];
        }

    }
}
#pragma badgemaintain
-(void)badgeservice
{
    NSString *path =[NSString stringWithFormat:@"%@1655",parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:devicetokenstring,@"devicetoken",userid,@"userid",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"status"]integerValue]==0)
         {
//             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"error"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//             [alert show];
             
         }
         else
         {
             [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
         }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
//         NSLog(@"Error: %@", [error localizedDescription]);
//         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//         [alert show];
     }];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.

    // application.applicationIconBadgeNumber = 0;
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
   
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if(application.applicationIconBadgeNumber>0)
    {
      // parentUrl=@"http://dev.businessprodemo.com/belindasbootcamps/live1/?page_id=";
       // parentUrl=@"http://test.businessprodemo.com/belindasbootcamps/php/?page_id=";
        // http://dev.businessprodemo.com/belindasbootcamps/php/?page_id=1655
      //  parentUrl=@"http://192.168.1.248/belindasbootcamps/php/?page_id=";
         parentUrl=@"http://belindacarusi.com/?page_id=";
        [self badgeservice];
    }
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
    
    if(statusactive)
    {
    
    NSUserDefaults *default1= [NSUserDefaults standardUserDefaults];
    NSString *login=[default1 valueForKey:@"LoginType"];
    
    if([login isEqualToString:@"Applogin"])
    {
       [self checkuserstatus];
        
    }
    else if([login isEqualToString:@"FBlogin"])
    {
       [self checkuserstatus];
    }
    }
    
}

#pragma userstatus
-(void)checkuserstatus
{
    NSLog(@"%@",userid);
    
    NSString *path =[NSString stringWithFormat:@"%@3516",parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:userid,@"userid",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"status"]integerValue]==0)
         {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"error"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            // alert.tag=101;
            [alert show];
             
         }
         else
         {
             NSArray *arr=[dict valueForKey:@"result"];
             NSString *statu=[[arr objectAtIndex:0] valueForKey:@"wpduact_app_status"];
             if([statu isEqualToString:@"inactive"])
             {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Access Denied! Please contact Administrator" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             alert.tag=101;
             [alert show];
             }
         }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         //         NSLog(@"Error: %@", [error localizedDescription]);
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Request Timed Out" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
     }];
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "BPD.Belinda" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Belinda" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Belinda.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


#pragma mark facebook login
// not using right now
- (BOOL)returnInternetConnectionStatus
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
    {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
        NSLog(@"There IS NO internet connection");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Please make sure your device is connected to internet" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    else
    {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
        NSLog(@"There IS internet connection");
        return YES;
    }
}




- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

@end
