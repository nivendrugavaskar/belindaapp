//
//  ViewController.m
//  Belinda
//
//  Created by Nivendru on 16/10/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "RegistrationViewController.h"

@interface ViewController ()
{
    CGRect screenBounds;
    AppDelegate *app;
    UITapGestureRecognizer *tap;
    BOOL remembermetrack,loadermaintain,loadermaintainclickloginbutton;
    UIAlertView *globalalert;
   
}
@end
@implementation ViewController
#pragma updated
- (void)viewDidLoad
{
    [super viewDidLoad];
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    remembermetrack=FALSE;
    [self registerForKeyboardNotifications];
    pointtrackcontentoffset=self.aScrollView.contentOffset;
    screenBounds = [[UIScreen mainScreen] bounds];
    [self.emailIdText setValue:[UIColor lightTextColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.passwordText setValue:[UIColor lightTextColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    [self.emailIdText setTintColor:[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f]];
    [self.passwordText setTintColor:[UIColor colorWithRed:189/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f]];
    
    
//   if(app->fromregistration)
//    {
//        remembermetrack=YES;
//        _tickuntickimageview.image=[UIImage imageNamed:@"checkbox.png"];
//        _emailIdText.text=emailtext;
//        _passwordText.text=passwordtext;
//        // new code
//        [self didTapLogin:nil];
//    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
}
//-(void)sendtoken:(NSString *)devicetoken
//{
//
//   
//    
//  
//}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self customcolour];
    //if(!app->pushdictionary)
    //{
        // check userdefault
        if(!app->fromregistration)
        {
            NSUserDefaults *storedcredential=[NSUserDefaults standardUserDefaults];
            NSString *username=[storedcredential valueForKey:@"emailid"];
            NSString *password=[storedcredential valueForKey:@"password"];
            if(([username length]>0)&&([password length]>0))
            {
                remembermetrack=YES;
                _tickuntickimageview.image=[UIImage imageNamed:@"checkbox.png"];
                _emailIdText.text=username;
                _passwordText.text=password;
            }
            else
            {
                remembermetrack=NO;
                _tickuntickimageview.image=[UIImage imageNamed:@"uncheckbox.png"];
                _emailIdText.text=@"";
                _passwordText.text=@"";
            }
            
            //new code
            NSUserDefaults *default1= [NSUserDefaults standardUserDefaults];
            NSString *loginType=[default1 valueForKey:@"LoginType"];
            
            if([loginType isEqualToString:@"Applogin"])
            {
                loadermaintainclickloginbutton=TRUE;
                [self didTapLogin:nil];
                
            }
            if([loginType isEqualToString:@"FBlogin"])
            {
//                if (![FBSDKAccessToken currentAccessToken])
//                {
                loadermaintainclickloginbutton=TRUE;
                [self didTapFacebooklogin:_fbloginbutton];
               // }
                
            }
            
        }
//        else
//        {
//            remembermetrack=YES;
//            _tickuntickimageview.image=[UIImage imageNamed:@"checkbox.png"];
//            _emailIdText.text=emailtext;
//            _passwordText.text=passwordtext;
//            // new code
//            [self didTapLogin:nil];
//        }
    
    }
//}


-(void)customcolour
{
    _emailIdText.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    _passwordText.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    
    _forgetpwdButton.titleLabel.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    _regButton.titleLabel.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    _remembermebutton.titleLabel.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    
   _imageviewsignup.backgroundColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    _imgviewforgetpassword.backgroundColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    

    
}
-(void)dismisskeyboard
{
    [self.activeTextField resignFirstResponder];
    [self.view removeGestureRecognizer:tap];
}
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
//    NSDictionary* info = [aNotification userInfo];
//    kbsize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.aScrollView setContentOffset:pointtrackcontentoffset animated:NO];
    [self.view removeGestureRecognizer:tap];
}

// # Text Field Delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
    pointtrack = textField.frame.origin;
    NSLog(@"%f",screenBounds.size.height);
    // dismiss when outside click
    tap = [[UITapGestureRecognizer alloc]
           initWithTarget:self
           action:@selector(dismisskeyboard)];
    tap.enabled=YES;
    tap.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:tap];

    if(screenBounds.size.height==480)
    {
    [self.aScrollView setContentOffset:CGPointMake(0, pointtrack.y-30) animated:YES];
    }
    else if(screenBounds.size.height==568)
    {
    [self.aScrollView setContentOffset:CGPointMake(0, pointtrack.y-80) animated:YES];
    }
    else if(screenBounds.size.height==667)
    {
    [self.aScrollView setContentOffset:CGPointMake(0, pointtrack.y-136) animated:YES];
    }
    else if(screenBounds.size.height==736)
    {
    [self.aScrollView setContentOffset:CGPointMake(0, pointtrack.y-173) animated:YES];
    }
    else
    {
    [self.aScrollView setContentOffset:CGPointMake(0, pointtrack.y-300) animated:YES];
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    if (textField ==_emailIdText || textField==_passwordText)
    {
        if ([string isEqualToString:@" "])
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}
- (IBAction)didTapLogin:(id)sender
{
    
//    [self performSegueWithIdentifier:@"segueToHome" sender:nil];
//    app->userid=@"9";
    
    if(_emailIdText.text.length<1)
    {
        UIAlertView *al=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please enter your Email Id" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [al show];
        [_emailIdText becomeFirstResponder];
        return;
    }
    else if(_passwordText.text.length<1)
    {
        UIAlertView *al=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please enter your Password" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [al show];
        [_passwordText becomeFirstResponder];
        
    }
    else
    {
        if ([self validateEmail:_emailIdText.text]==NO)
        {
            UIAlertView *al=[[UIAlertView alloc]initWithTitle:@"! Wrong " message:@"Please enter Correct Email Id" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [al show];
            [_emailIdText becomeFirstResponder];
            return;
        }
        else
        {
            [self.activeTextField resignFirstResponder];
            [self webservicenormallogin];
        }
        
    }
    
}
- (BOOL) validateEmail:(NSString *) emailid
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailid];
}
-(void)webservicenormallogin
{
    // testing purpose
   // app->devicetokenstring=@"72d1ac8f7aa70dfdf425c360340feca04442d3c88ec87a9dc64aba54681dfddd";
    if(!app->devicetokenstring)
    {
    app->devicetokenstring=@"";
    }
    NSString *path =[NSString stringWithFormat:@"%@1471",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:_emailIdText.text,@"useremail",_passwordText.text,@"password",@"ios",@"devicetype",app->devicetokenstring,@"devicetoken",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Authenticating.."];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {

         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"error"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
             app->pushdictionary=nil;
         }
         else
         {

              app->statusactive=TRUE;
              app->userid=[[dict valueForKey:@"result"] valueForKey:@"userid"];
             // new code for remember
             NSUserDefaults *credentialdefault=[NSUserDefaults standardUserDefaults];
             if(remembermetrack==YES)
             {
                 [credentialdefault setValue:_emailIdText.text forKey:@"emailid"];
                 [credentialdefault setValue:_passwordText.text forKey:@"password"];
                 // new code for authorization
                 [credentialdefault setValue:@"Applogin" forKey:@"LoginType"];
                 [credentialdefault setValue:app->userid forKey:@"appid"];
                 [credentialdefault setValue:_emailIdText.text forKey:@"futureuseemail"];
                 [credentialdefault setValue:_passwordText.text forKey:@"futureusepassword"];
                 //end
                 [credentialdefault synchronize];
             }
             else
             {
                 [credentialdefault setValue:@"" forKey:@"emailid"];
                 [credentialdefault setValue:@"" forKey:@"password"];
                 // new code for authorization
                 [credentialdefault setValue:@"" forKey:@"LoginType"];
                 [credentialdefault setValue:@"" forKey:@"appid"];
                 [credentialdefault setValue:@"" forKey:@"futureuseemail"];
                 [credentialdefault setValue:@"" forKey:@"futureusepassword"];
                 //end
                 [credentialdefault synchronize];
             }
             
             app->fromregistration=FALSE;
             app->loginType=@"Applogin";
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
             [self performSegueWithIdentifier:@"segueToHome" sender:nil];
            
         }
     }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
          app->pushdictionary=nil;
          [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==100)
    {
        if(buttonIndex==0)
        {
            [self performSegueWithIdentifier:@"segueToHome" sender:nil];
        }
    }
    if(alertView.tag==501)
    {
        if (buttonIndex==1)
        {
            NSString *trimmed=[[[globalalert textFieldAtIndex:0]text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if([trimmed length]>0)
            {
                if ([self validateEmail:trimmed]==NO)
                {
                    [[globalalert textFieldAtIndex:0] becomeFirstResponder];
                    UIAlertView *al=[[UIAlertView alloc]initWithTitle:@"! Wrong " message:@"Please enter Correct Email Id" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [al show];
                   
                }
                else
                {
                  [[globalalert textFieldAtIndex:0] resignFirstResponder];
                    [self webserviceforgetpassword];
                }
            }
            else
            {
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Email Address Can't Empty" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            
        }
    }
}

-(void)webserviceforgetpassword
{
    NSString *path =[NSString stringWithFormat:@"%@1015",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[[globalalert textFieldAtIndex:0]text],@"useremail",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Loading"];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"error"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
         }
         else
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"result"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
         }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}

- (IBAction)didTapRegistration:(id)sender
{
     [self performSegueWithIdentifier:@"segueToReg" sender:self];
}

- (IBAction)didTapForgetPassword:(id)sender
{
    globalalert=[[UIAlertView alloc]initWithTitle:@"\nEnter Email Address" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    globalalert.tag=501;
    [globalalert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [globalalert textFieldAtIndex:0].delegate=self;
    [globalalert show];
}


- (IBAction)didTaprememberme:(id)sender
{

    if(remembermetrack==NO)
    {
        remembermetrack=YES;
        _tickuntickimageview.image=[UIImage imageNamed:@"checkbox.png"];
        
       
    }
    else
    {
        remembermetrack=NO;
        _tickuntickimageview.image=[UIImage imageNamed:@"uncheckbox.png"];
    }
#pragma For ios 8 we can use
        
//      UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"!Required" message:nil preferredStyle:UIAlertControllerStyleAlert];
//        
//        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
//                                    {
//            NSLog(@"User clicked button called %@ or tapped elsewhere",action.title);
//        }
//                                    ]];
//        
//        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
//                                    {
//            NSLog(@"User clicked button called %@",action.title);
//        }
//                                    ]];
//        
//        [alertController addAction:[UIAlertAction actionWithTitle:@"Other" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
//        {
//            NSLog(@"User clicked button called %@",action.title);
//        }
//                                    ]];
//        
//        alertController.popoverPresentationController.sourceView = self.view;
//        
//        [self presentViewController:alertController animated:YES
//                         completion:nil];
        
}
- (IBAction)didTapFacebooklogin:(id)sender
{
  //  if (![FBSDKAccessToken currentAccessToken])
     NSUserDefaults *default1= [NSUserDefaults standardUserDefaults];
    NSString *loginType=[default1 valueForKey:@"LoginType"];
    if(![loginType isEqualToString:@"FBlogin"])
    {
        //[SVProgressHUD showWithStatus:@"Please Wait...."];
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        // [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
        [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
         {
             [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
             if (error) {
                 [SVProgressHUD dismiss];
                 // Process error
             } else if (result.isCancelled) {
                 // Handle cancellations
                 [SVProgressHUD dismiss];
             } else {
                 // If you ask for multiple permissions at once, you
                 // should check if specific permissions missing
                 NSLog(@"%@",result);
                 if ([result.grantedPermissions containsObject:@"email"])
                 {
                     // Do work
                     NSLog(@"Granted all permission");
                     
                     //NSLog(@"fetched Email : %@",[result.grantedPermissions valueForKey:@"contact_email"]);
                     if ([FBSDKAccessToken currentAccessToken])
                     {
                         NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
                         [parameters setValue:@"id,name,email" forKey:@"fields"];
                         
                         [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
                          {
                              if (!error)
                              {
                                  NSDictionary *user=(NSDictionary *)result;
                                  NSLog(@"fetched user when no login:%@  and Email : %@", result,result[@"email"]);
                                  [self webServiceMethodForFaceBookLogin:user];
                              }
                          }];
                     }
                     else
                     {
                         NSLog(@"Not granted");
                     }
                     
                 }
             }
         }];
    }
    else
    {
        [self webServiceMethodForFaceBookLogin:nil];
//        NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
//        [parameters setValue:@"id,name,email" forKey:@"fields"];
//        
//        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
//         {
//             if (!error)
//             {
//                 NSDictionary *user=(NSDictionary *)result;
//                 NSLog(@"fetched user when no login:%@  and Email : %@", result,result[@"email"]);
//                 [self webServiceMethodForFaceBookLogin:user];
//                 
//             }
//         }];
    }
   
}
- (void)loginViaFacebook:(NSNotification *) notification
{
   
    NSDictionary *user = notification.object;
    NSLog(@"notificationdict: %@", user);
    [[NSNotificationCenter defaultCenter] removeObserver:self];
   // NSDictionary *parameter=[NSDictionary dictionaryWithObjectsAndKeys:accountId,@"fb_id",email,@"email", nil];
//    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
//    [SVProgressHUD dismiss];
    [self webServiceMethodForFaceBookLogin:user];
    //[self performSegueWithIdentifier:@"segueToHome" sender:nil];
   // app->userid=@"9";
    
}


-(void)webServiceMethodForFaceBookLogin: (NSDictionary *)parameter
{
    if(!app->devicetokenstring)
    {
        app->devicetokenstring=@"";
    }
     NSString *path =[NSString stringWithFormat:@"%@1558",app->parentUrl];
    NSDictionary *params;
    if(parameter==nil)
    {
        NSUserDefaults *credentialdefault=[NSUserDefaults standardUserDefaults];
         params = [NSDictionary dictionaryWithObjectsAndKeys:[credentialdefault valueForKey:@"fbemailid"],@"email",@"ios",@"devicetype",app->devicetokenstring,@"devicetoken",nil];
    }
    else
    {
        params = [NSDictionary dictionaryWithObjectsAndKeys:[parameter valueForKey:@"email"],@"email",@"ios",@"devicetype",app->devicetokenstring,@"devicetoken",nil];
    }
    
   
   
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    if((!loadermaintain))
    {
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Authenticating.."];
    }
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"error"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
         }
         else
         {
             app->statusactive=TRUE;
             app->userid=[[dict valueForKey:@"result"] valueForKey:@"userid"];
             // new code for remember
             NSUserDefaults *credentialdefault=[NSUserDefaults standardUserDefaults];
             if(remembermetrack==YES)
             {
                [credentialdefault setValue:_emailIdText.text forKey:@"emailid"];
                [credentialdefault setValue:_passwordText.text forKey:@"password"];
             }
             else
             {
                 [credentialdefault setValue:@"" forKey:@"emailid"];
                 [credentialdefault setValue:@"" forKey:@"password"];
             }
             
             [credentialdefault setValue:[parameter valueForKey:@"email"] forKey:@"fbemailid"];
             // new code for authorization
             [credentialdefault setValue:@"FBlogin" forKey:@"LoginType"];
             [credentialdefault setValue:app->userid forKey:@"appid"];
             [credentialdefault setValue:[parameter valueForKey:@"email"] forKey:@"futureusefbemail"];
             app->fromregistration=FALSE;
             app->loginType=@"FBlogin";
             [credentialdefault synchronize];
             //end
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
             [self performSegueWithIdentifier:@"segueToHome" sender:nil];
             
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}
@end
