//
//  main.m
//  Belinda
//
//  Created by Nivendru on 16/10/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool
    {
       // [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
