//
//  essentialGoalListViewController.h
//  Belinda
//
//  Created by Nivendru on 05/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "PreviewImageViewController.h"


@interface essentialGoalListViewController : UIViewController<UITextFieldDelegate,UIScrollViewDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate,SWRevealViewControllerDelegate>
{
     CGPoint pointtrack,pointtrackcontentoffset;
    UIImagePickerController *ImagePicker;
    UIImage *selectedimage,*selectedimage1,*selectedimage2;
    NSData *imageDataPicker;
    UIDatePicker *datePicker;
}
@property (strong, nonatomic) IBOutlet UILabel *startedGoallabel;
@property (strong, nonatomic) IBOutlet UILabel *measurementlabel;
@property (strong, nonatomic) IBOutlet UIScrollView *aScrollView;
@property(strong,nonatomic) UITextField *activeTextField;
@property(strong,nonatomic) UIButton *activebutton;
@property (strong, nonatomic) IBOutlet UIScrollView *questionsScrollView;
- (IBAction)didTapFront:(id)sender;
- (IBAction)didTapLeft:(id)sender;
- (IBAction)didTapback:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *frontbutton;
@property (strong, nonatomic) IBOutlet UIButton *leftbutton;
@property (strong, nonatomic) IBOutlet UIButton *backbutton;

@property (strong, nonatomic) IBOutlet UIView *imageContainerView;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutlet UIImageView *frontimageupload;
@property (strong, nonatomic) IBOutlet UIImageView *leftsideimageupload;
@property (strong, nonatomic) IBOutlet UIImageView *backimageupload;
@property (strong, nonatomic) IBOutlet UITextField *armtextfield;
@property (strong, nonatomic) IBOutlet UITextField *chesttextfield;
@property (strong, nonatomic) IBOutlet UITextField *waisttextfield;
@property (strong, nonatomic) IBOutlet UITextField *hiptextfield;
@property (strong, nonatomic) IBOutlet UITextField *thightextfield;
@property (strong, nonatomic) IBOutlet UITextField *weighttextfield;
@property (strong, nonatomic) IBOutlet UITextField *timetextfield;
@property (strong, nonatomic) IBOutlet UITextField *firstanswertextfield;
@property (strong, nonatomic) IBOutlet UITextField *secondanswertextfield;
@property (strong, nonatomic) IBOutlet UITextField *thirdanswertextfield;
@property (strong, nonatomic) IBOutlet UILabel *fitneslabeltest;

@property (strong, nonatomic) IBOutlet UILabel *firstquestionlabel;
@property (strong, nonatomic) IBOutlet UILabel *secondquestionlabel;
@property (strong, nonatomic) IBOutlet UILabel *thirdquestionlabel;

@property (strong, nonatomic) IBOutlet UIImageView *firstquesimageview;
@property (strong, nonatomic) IBOutlet UIImageView *secondquestionimageview;
@property (strong, nonatomic) IBOutlet UIImageView *thirdquestionimageview;
- (IBAction)didTapSubmit:(id)sender;

@property (strong,nonatomic) NSMutableArray *assementarraydata;

@end
