//
//  NSString+Validation.m
//  Belinda
//
//  Created by Nivendru on 27/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "NSString+Validation.h"

@implementation NSString (Validation)

+(BOOL)validation:(NSString *)sendstring
{
    NSString *trimmed=sendstring;
    trimmed=[trimmed stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if([trimmed length]==0)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}
@end
