//
//  MyInfoViewController.h
//  Belinda
//
//  Created by Nivendru on 13/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface MyInfoViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,UIAlertViewDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate,SWRevealViewControllerDelegate>
{
    CGPoint pointtrack,pointtrackcontentoffset;
    UIImagePickerController *ImagePicker;
    UIImage *selectedimage;
    NSData *imageDataPicker;
    UIDatePicker *datePicker;
}
@property (strong, nonatomic) IBOutlet UIView *innerviewscrollviewimagecontentview;
- (IBAction)didTappicchangebutton:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *aScrollview;
- (IBAction)didtapoffspecialnotification:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *offbuttonspecialnotification;
- (IBAction)didtaponspecialnotification:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *onbuttonspecialnotification;
- (IBAction)didtapoffgoalnotification:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *offbuttongoalnotification;
- (IBAction)didtapongoalnotification:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *onbuttongoalnotification;
- (IBAction)didTapupdate:(id)sender;
@property(strong,nonatomic) UITextField *activeTextField;
@property (strong, nonatomic) IBOutlet UIImageView *uploadimage;
@property (strong, nonatomic) IBOutlet UIButton *changepicbutton;
@property (strong, nonatomic) IBOutlet UITextField *fullname;
@property (strong, nonatomic) IBOutlet UITextField *mobileno;
@property (strong, nonatomic) IBOutlet UITextField *oldpasswordtextfield;
@property (strong, nonatomic) IBOutlet UITextField *newpasswordtextfield;
@property (strong, nonatomic) IBOutlet UITextField *confirmpasswordtextfield;
@property (strong, nonatomic) IBOutlet UILabel *emailshow;

@end
