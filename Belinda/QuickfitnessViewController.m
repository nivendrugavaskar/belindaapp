//
//  QuickfitnessViewController.m
//  Belinda
//
//  Created by Nivendru on 06/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "QuickfitnessViewController.h"

@interface QuickfitnessViewController ()
{
    NSString *storeweekormonth,*selectednewgoal,*selectedgoaltype,*alertmessage;
    UITapGestureRecognizer *tap;
    NSDate *selectedstartdate,*selectedenddate;
    
}

@end

@implementation QuickfitnessViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    screenBounds = [[UIScreen mainScreen] bounds];
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:220/255.0f green:25/255.0f blue:124/255.0f alpha:1.0f];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.title=@"Goals";
    // Hide navigation bar and customize
    self.navigationItem.hidesBackButton=YES;
    [self setcustomproperties];
    [self setCustomNavigationButton];
    // new code
    if(_specificgoaldata)
    {
        
        NSLog(@"Populatedata");
        [self loadData];
    }
}
-(void)loadData
{
   // NSLog(@"array=%@",_specificgoaldata);
    if([[_specificgoaldata valueForKey:@"goal_duration"]isEqualToString:@"Weekly"])
    {
        [self didTapweekly:nil];
    }
    else if([[_specificgoaldata valueForKey:@"goal_duration"]isEqualToString:@"Monthly"])
    {
        [self didTapmonthly:nil];
    }
    if([[NSString stringWithFormat:@"%@",[_specificgoaldata valueForKey:@"goal_type"]]isEqualToString:@"75"])
    {
        [self didTapexercise:nil];
    }
    else if([[NSString stringWithFormat:@"%@",[_specificgoaldata valueForKey:@"goal_type"]]isEqualToString:@"76"])
    {
        [self didTapFood:nil];
    }
    else if([[NSString stringWithFormat:@"%@",[_specificgoaldata valueForKey:@"goal_type"]]isEqualToString:@"77"])
    {
        [self didTaplifestyle:nil];
    }
    else if([[NSString stringWithFormat:@"%@",[_specificgoaldata valueForKey:@"goal_type"]]isEqualToString:@"78"])
    {
        [self didTappersonal:nil];
    }
    NSString *startdate=[_specificgoaldata valueForKey:@"goal_start"];
    NSString *enddate=[_specificgoaldata valueForKey:@"goal_end"];
    _startdatelabel.text=startdate;
    _enddatelabel.text=enddate;
    _goalname.text=[_specificgoaldata valueForKey:@"goalname"];
    _whatisyourgoaltextfield.text=[_specificgoaldata valueForKey:@"goalansw"];
    
    NSString *imagepath=[_specificgoaldata valueForKey:@"goal_image"];
    if([imagepath isEqualToString:@""])
    {
       _picturestatuslabel.text=@"No Uploaded Image";
      _uploadimage.image=[UIImage imageNamed:@"no_image.png"];
    }
    else
    {
        _picturestatuslabel.text=@"Uploaded Image";
        
        __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityIndicator.color=[UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
        activityIndicator.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
        activityIndicator.hidesWhenStopped = YES;
        
        __block BOOL successfetchimage=FALSE;
        __weak UIImageView *setimage = _uploadimage;
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imagepath]];
        [_uploadimage setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"no_image.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             setimage.contentMode=UIViewContentModeScaleAspectFit;
             setimage.image=image;
             successfetchimage=TRUE;
             [activityIndicator removeFromSuperview];
         }
                                      failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             [activityIndicator removeFromSuperview];
         }];
        
        if(!successfetchimage)
        {
            [_uploadimage addSubview:activityIndicator];
            [activityIndicator startAnimating];
        }
    }
//    id v=[_specificgoaldata objectAtIndex:6];
//    if([v isKindOfClass:[NSData class]])
//    {
//        _uploadimage.image=[UIImage imageWithData:[_specificgoaldata objectAtIndex:6]];
//    }
//    else
//    {
//        _uploadimage.image=[UIImage imageNamed:@"no_image.png"];
//    }
   
    // close interaction
    [self closeInteraction];
    
}
-(void)closeInteraction
{
    _weeklybutton.userInteractionEnabled=NO;
    _monthlybutton.userInteractionEnabled=NO;
    _exercisebutton.userInteractionEnabled=NO;
    _foodbutton.userInteractionEnabled=NO;
    _lifestylebutton.userInteractionEnabled=NO;
    _personalbutton.userInteractionEnabled=NO;
    _startdatebutton.userInteractionEnabled=NO;
    _goalname.userInteractionEnabled=NO;
    _whatisyourgoaltextfield.userInteractionEnabled=NO;
    _camerabtn.userInteractionEnabled=NO;
    _submitbtn.hidden=YES;
    // new code
    self.lastimagecontainerview.frame=CGRectMake(self.lastimagecontainerview.frame.origin.x, self.lastimagecontainerview.frame.origin.y+30, self.lastimagecontainerview.frame.size.width, self.lastimagecontainerview.frame.size.height);
}
-(void)setcustomproperties
{
    for (id v in self.view.subviews)
    {
       if([v isKindOfClass:[UILabel class]])
       {
           UILabel *label=(UILabel *)v;
           label.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
       }
        if([v isKindOfClass:[UITextField class]])
        {
            UITextField *customtext=(UITextField *)v;
            [customtext setValue:[UIColor lightTextColor] forKeyPath:@"_placeholderLabel.textColor"];
            customtext.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
        }
    }
    
    _goaltypelabel.textColor=[UIColor whiteColor];
    // set border
    _imagecontainerview.layer.borderWidth=1.0;
    _imagecontainerview.layer.borderColor=[UIColor whiteColor].CGColor;
    
    _createnewgoalview.backgroundColor=[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f];
    _goaltypeview.backgroundColor=[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f];
    
    storeweekormonth=[[NSString alloc]init];
    selectednewgoal=@"";
    selectedgoaltype=@"";
    
}
-(void)setCustomNavigationButton
{
    
    UIButton *Menubutton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 28)];
    UIImageView *buttonImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 20, 18)];
    buttonImage.image=[UIImage imageNamed:@"menu_icon.png" ];
    buttonImage.clipsToBounds=YES;
    buttonImage.contentMode=UIViewContentModeScaleAspectFit;
    [Menubutton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    // set swrevealcontroller delegate
    self.revealViewController.delegate =self;
    [Menubutton addSubview:buttonImage];
    UIBarButtonItem *menubtn=[[UIBarButtonItem alloc]initWithCustomView:Menubutton];
    self.navigationItem.leftBarButtonItem=menubtn;
    
    
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"back_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    
    UIBarButtonItem *backbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombutton];
    //[backbarbutton setAction:@selector(back)];
    //[backbarbutton setTarget:self];
    backbarbutton.style = UIBarButtonItemStylePlain;
    // new code
    self.navigationItem.rightBarButtonItems = @[backbarbutton];
    
}
#pragma swrevealview delegate
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    [self dismisskeyboard];
}
-(void)back
{
    NSLog(@"Back");
    [self.activeTextField resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(void)dismisskeyboard
{
    [self.view removeGestureRecognizer:tap];
    CGRect rect=self.view.frame;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    rect.origin.y=pointtrackcontentoffset.y;
    self.view.frame=rect;
    [UIView commitAnimations];
    [self.activeTextField resignFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Set the gesture
    pointtrackcontentoffset=self.view.frame.origin;
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    // new code
    hiderevealButton=[[UIButton alloc]initWithFrame:self.view.bounds];
    [hiderevealButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    hiderevealButton.backgroundColor=[UIColor clearColor];
    [self.view addSubview:hiderevealButton];
    [hiderevealButton setHidden:YES];
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
    pointtrack =textField.frame.origin;
    NSLog(@"%f",screenBounds.size.height);
    
    tap = [[UITapGestureRecognizer alloc]
           initWithTarget:self
           action:@selector(dismisskeyboard)];
    tap.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:tap];
    CGRect rect=self.view.frame;
    rect.origin=pointtrackcontentoffset;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    if(screenBounds.size.height==480)
    {
        rect.origin.y-=pointtrack.y-105;
    }
    else if(screenBounds.size.height==568)
    {
       rect.origin.y-=pointtrack.y-145;
    }
    else if(screenBounds.size.height==667)
    {
       rect.origin.y-=pointtrack.y-195;
    }
    else if(screenBounds.size.height==736)
    {
       rect.origin.y-=pointtrack.y-200;
    }
    else
    {
    
    }
    self.view.frame=rect;
    [UIView commitAnimations];
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didTaptakepicture:(id)sender
{
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Upload Image \nFrom:" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Gallery", nil];
    alert.tag=100;
    [alert show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==100)
    {
        if(buttonIndex==0)
        {
            //_uploadimage.image=[UIImage imageNamed:@"no_image.png"];
        }
        if(buttonIndex==1)
        {
            //[self performSelector:@selector(takepicture) withObject:nil afterDelay:0.3];
            [self takepicture];
        }
        else if(buttonIndex==2)
        {
            [self selectgallery];
        }
    }
    else if(alertView.tag==101)
    {
        [self back];
    }
    

}


-(void)change:(id)sender
{
    NSLog(@"change");
}
// new code
- (void)changeDate1:(UIBarButtonItem *)sender
{
  //  self.activebutton=(UIButton *)sender;
   
    NSDateFormatter *format=[[NSDateFormatter alloc]init];
    [format setDateFormat:@"dd-MM-yyyy"];
    selectedstartdate=datePicker.date;
    _startdatelabel.text=[NSString stringWithFormat:@"%@",[format stringFromDate:datePicker.date ]];
    [self dismissDatePicker:nil];
    
    // create alert of new goal
    if([selectednewgoal isEqualToString:@"Weekly"])
    {
        [self didTapweekly:nil];
    }
    else
    {
       [self didTapmonthly:nil];
    }
}

- (void)removeViews:(id)object
{
    [[self.view viewWithTag:10] removeFromSuperview];
    [[self.view viewWithTag:11] removeFromSuperview];
}

- (void)dismissDatePicker:(id)sender
{
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, self.view.bounds.size.width, 216);
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];
}

-(void)selectgallery
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    ImagePicker=[[UIImagePickerController alloc]init];
    ImagePicker.delegate=(id)self;
    ImagePicker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    // imagepic.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:ImagePicker animated:YES completion:nil];
}
-(void)takepicture
{
    /******************** code for camera ***********************/
    
    ImagePicker=[[UIImagePickerController alloc]init];
    if    (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
    }
    else
    {
        
        ImagePicker.delegate=(id)self;
        ImagePicker.modalPresentationStyle=UIModalPresentationFullScreen;
       // ImagePicker.modalPresentationStyle = UIModalPresentationCurrentContext;
        
       // [ImagePicker setAllowsEditing:YES];
        ImagePicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        // imagepic.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:ImagePicker animated:YES completion:NULL];
        NSLog(@"camera");
        
    }
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    // new code
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [picker dismissViewControllerAnimated:YES completion:nil];
    // [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    selectedimage=[info objectForKey:UIImagePickerControllerOriginalImage];
    selectedimage=[self scaleAndRotateImage:selectedimage];
    _uploadimage.contentMode=UIViewContentModeScaleAspectFit;
    _uploadimage.image=selectedimage;
    [picker dismissViewControllerAnimated:YES completion:nil];
}
// new method
- (UIImage *)scaleAndRotateImage:(UIImage *)image
{
    int kMaxResolution = 640; // Or whatever
    CGImageRef imgRef = image.CGImage;
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}
- (IBAction)didTapsubmit:(id)sender
{
    [self dismisskeyboard];
    if([selectednewgoal isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Please select Weekly or Monthly" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([selectedgoaltype isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Please select Goal Type" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([_startdatelabel.text isEqualToString:@"Start Date"])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Please select start date" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if(![NSString validation:_goalname.text])
    {
        NSLog(@"Required");
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"! Required" message:@"Please Fill goal name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
   else  if(![NSString validation:_whatisyourgoaltextfield.text])
    {
        NSLog(@"Required");
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"! Required" message:@"Please Fill what is your goal?" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    [self webservicegoaladd];
   // [self.goallistingdelegate sendarraydata:app.testingpurposearray];
    // end
    //[self back];
    
}
-(void)webservicegoaladd
{
    
    NSString *path =[NSString stringWithFormat:@"%@1400",app->parentUrl];
    NSString *goaltype;
    if( [selectedgoaltype isEqualToString:@"Exercise"])
    {
    goaltype=@"75";
    }
    else if( [selectedgoaltype isEqualToString:@"Food"])
    {
    goaltype=@"76";
    }
    else if( [selectedgoaltype isEqualToString:@"Lifestyle"])
    {
    goaltype=@"77";
    }
    else if( [selectedgoaltype isEqualToString:@"Personal"])
    {
    goaltype=@"78";
    }
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Creating Goal.."];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    imageDataPicker=(NSData *)UIImagePNGRepresentation(selectedimage);
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",_goalname.text,@"goalname",selectednewgoal,@"goalduration",_startdatelabel.text,@"startdate",_enddatelabel.text,@"enddate",_whatisyourgoaltextfield.text,@"goalansw",goaltype,@"goaltype",nil];
    AFHTTPRequestOperation *op = [manager POST:path parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //do not put image inside parameters dictionary as I did, but append it!
        if((NSData *)UIImagePNGRepresentation(selectedimage))
        {
            [formData appendPartWithFileData:(NSData *)UIImagePNGRepresentation(selectedimage) name:@"frontimage" fileName:@"goalfrontphoto.png" mimeType:@"image/png"];
            
        }
    }
                                       success:^(AFHTTPRequestOperation *operation, id responseObject)
                                  {
                                      NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
                                      NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
                                      NSLog(@"%@",responseStr);
                                      NSDictionary *dict1 = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
                                      NSLog(@"%@",dict1);
                                      //here is place for code executed in success case
                                      if([[dict1 valueForKey:@"status"]integerValue]==0)
                                      {
                                          UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict1 valueForKey:@"error"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                          [alert show];
                                          [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                          [SVProgressHUD dismiss];
                                      }
                                      else
                                      {
                                          
                                          UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Goal Successfully Created" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                          [alert show];
                                          alert.tag=101;
                                          // delegate call to make boolean false
                                          [self.goallistingdelegate sendflagtrue:FALSE];
                                          //
                                          [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                          [SVProgressHUD dismiss];
                                      }
                                      
                                  }
                                       failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                  
                                  {
                                      [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                      [SVProgressHUD dismiss];
                                      NSLog(@"Error: %@", [error localizedDescription]);
                                      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                      [alert show];
                                  }
                                  ];
    [op start];
}

- (IBAction)didTapweekly:(id)sender
{
    _weeklybutton.backgroundColor=[UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
    _monthlybutton.backgroundColor=[UIColor clearColor];
    selectednewgoal=@"Weekly";
    // new code
    if(![_startdatelabel.text isEqualToString:@"Start Date"])
    {
        // create alert of new goal
       
        storeweekormonth=@"1";
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *comps = [NSDateComponents new];
        comps.day = 7*[storeweekormonth intValue];
        selectedenddate = [calendar dateByAddingComponents:comps toDate:selectedstartdate options:0];
        selectedenddate = [selectedenddate dateByAddingTimeInterval:-24*60*60];
        
        NSDateFormatter *format=[[NSDateFormatter alloc]init];
        [format setDateFormat:@"dd-MM-yyyy"];
        _enddatelabel.text=[NSString stringWithFormat:@"%@",[format stringFromDate:selectedenddate]];
    }
    
}

- (IBAction)didTapmonthly:(id)sender
{
    _weeklybutton.backgroundColor=[UIColor clearColor];
    _monthlybutton.backgroundColor=[UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
    selectednewgoal=@"Monthly";
    
    // new code
    if(![_startdatelabel.text isEqualToString:@"Start Date"])
    {
        storeweekormonth=@"1";
        NSDateComponents *dateComponents = [NSDateComponents new];
        dateComponents.month = 1*[storeweekormonth intValue];
        selectedenddate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:selectedstartdate options:0];
        selectedenddate = [selectedenddate dateByAddingTimeInterval:-24*60*60];
        NSDateFormatter *format=[[NSDateFormatter alloc]init];
        [format setDateFormat:@"dd-MM-yyyy"];
        _enddatelabel.text=[NSString stringWithFormat:@"%@",[format stringFromDate:selectedenddate]];
    }
}

- (IBAction)didTapexercise:(id)sender
{
    _exercisebutton.backgroundColor=[UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
    _foodbutton.backgroundColor=[UIColor clearColor];
    _lifestylebutton.backgroundColor=[UIColor clearColor];
    _personalbutton.backgroundColor=[UIColor clearColor];
    selectedgoaltype=@"Exercise";
}

- (IBAction)didTapFood:(id)sender
{
    _exercisebutton.backgroundColor=[UIColor clearColor];
    _foodbutton.backgroundColor=[UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
    _lifestylebutton.backgroundColor=[UIColor clearColor];
    _personalbutton.backgroundColor=[UIColor clearColor];
    selectedgoaltype=@"Food";
}

- (IBAction)didTaplifestyle:(id)sender
{
    _lifestylebutton.backgroundColor=[UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
    _foodbutton.backgroundColor=[UIColor clearColor];
    _exercisebutton.backgroundColor=[UIColor clearColor];
     _personalbutton.backgroundColor=[UIColor clearColor];
    selectedgoaltype=@"Lifestyle";
}

- (IBAction)didTappersonal:(id)sender
{
    _personalbutton.backgroundColor=[UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
    _foodbutton.backgroundColor=[UIColor clearColor];
    _exercisebutton.backgroundColor=[UIColor clearColor];
    _lifestylebutton.backgroundColor=[UIColor clearColor];
    selectedgoaltype=@"Personal";
}
- (IBAction)didTapstartdate:(id)sender
{
   
    if([selectednewgoal isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Please select Weekly or Monthly" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height-216-44, self.view.bounds.size.width, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height-216, self.view.bounds.size.width, 216);
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height+44,self.view.bounds.size.width, 216)];
    datePicker.tag = 10;
    datePicker.backgroundColor=[UIColor whiteColor];
    datePicker.datePickerMode=UIDatePickerModeDate;
    
#pragma  new code to show max 6 day(1 week )
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:0];
    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    comps.day=6;
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    [datePicker setMinimumDate:minDate];
    [datePicker setMaximumDate:maxDate];
    // end
    if(![_startdatelabel.text isEqualToString:@"Start Date"])
    {
        NSDateFormatter *format=[[NSDateFormatter alloc]init];
        [format setDateFormat:@"dd-MM-yyyy"];
        NSDate *senddate=[format dateFromString:_startdatelabel.text];
        datePicker.date=senddate;
    }
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    [self.view addSubview:datePicker];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44)];
    toolBar.tag = 11;
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.backgroundColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    
    UIBarButtonItem *Cancel=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(dismissDatePicker:)];
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
   
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(changeDate1:)];
    [toolBar setItems:[NSArray arrayWithObjects:Cancel,spacer, doneButton, nil]];
    
    [self.view addSubview:toolBar];
    
    [UIView beginAnimations:@"MoveIn" context:nil];
    toolBar.frame = toolbarTargetFrame;
    datePicker.frame = datePickerTargetFrame;
    [UIView commitAnimations];
    
}
@end
