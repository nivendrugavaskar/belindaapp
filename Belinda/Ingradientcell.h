//
//  Ingradientcell.h
//  Belinda
//
//  Created by Nivendru on 24/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ingradientcell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *ingradientnamelabel;
@property (strong, nonatomic) IBOutlet UILabel *quantity;
@property (strong, nonatomic) IBOutlet UIImageView *seperatorLabel;

@end
