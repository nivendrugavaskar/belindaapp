//
//  ResultslistViewController.h
//  Belinda
//
//  Created by Nivendru on 18/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Resultlistcell.h"
#import "essentialGoalListViewController.h"

@interface ResultslistViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    AppDelegate *app;
}
@property (strong, nonatomic) IBOutlet UITableView *aTableView;
@end
