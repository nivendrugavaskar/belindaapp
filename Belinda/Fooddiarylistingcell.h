//
//  Fooddiarylistingcell.h
//  Belinda
//
//  Created by Nivendru on 31/10/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Fooddiarylistingcell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *foodName;
@property (strong, nonatomic) IBOutlet UILabel *date;
@property (strong, nonatomic) IBOutlet UILabel *seperatorlabel;

@end
