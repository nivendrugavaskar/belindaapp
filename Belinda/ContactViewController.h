//
//  ContactViewController.h
//  Belinda
//
//  Created by Nivendru on 10/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface ContactViewController : UIViewController<UIWebViewDelegate,MKMapViewDelegate,MKAnnotation>
{
    AppDelegate *app;
}
@property (strong, nonatomic) IBOutlet UITextView *contactaddresstextview;
@property (strong, nonatomic) IBOutlet UITextView *phonetextview;
@property (strong, nonatomic) IBOutlet UITextView *emailtextview;
- (IBAction)didTapmap:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *mapbutton;
- (IBAction)didTapphone:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *phonebutton;
- (IBAction)didTapemail:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *emailbutton;
@property (strong, nonatomic) IBOutlet MKMapView *mapview;



@end
