//
//  MainViewController.h
//  Belinda
//
//  Created by Nivendru on 17/10/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "SWRevealViewController.h"

@protocol RowSelectionDelegate<NSObject>
-(void)selectRowWithHeading:(int)index;
@end

@interface MainViewController : UIViewController
- (IBAction)didTapfooddiary:(id)sender;
- (IBAction)didTapessentialgoal:(id)sender;
- (IBAction)didTapQuickfitness:(id)sender;
- (IBAction)didTapResults:(id)sender;
- (IBAction)didTapshoppinglist:(id)sender;
- (IBAction)didTapRecipe:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *innerview;
@property (retain) id<RowSelectionDelegate> rowSelectionDelegate;
@end
