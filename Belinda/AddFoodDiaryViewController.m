//
//  AddFoodDiaryViewController.m
//  Belinda
//
//  Created by Nivendru on 03/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "AddFoodDiaryViewController.h"



@interface AddFoodDiaryViewController ()
{
    UITapGestureRecognizer *tap;
    NSMutableArray *feelingarray,*smileyarray;
    UIPickerView *feelingpickerview;
}

@end

@implementation AddFoodDiaryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    feelingarray=[[NSMutableArray alloc] initWithObjects:@"Energized",@"Good",@"Bloated",@"Ok",@"Tired",@"Sluggish", nil];
    smileyarray=[[NSMutableArray alloc] initWithObjects:@"energysmiley.jpg",@"smiley1.jpg",@"smiley2.jpg",@"smiley3.jpg",@"smiley4.jpg",@"smiley5.jpg", nil];
    _smileyimageview.image=[UIImage imageNamed:@"no_image.png"];
    //self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:220/255.0f green:25/255.0f blue:124/255.0f alpha:1.0f];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.title=@"Food Diary";
    // Hide navigation bar and customize
    self.navigationItem.hidesBackButton=YES;
    
    // set place holder colour
    for (UIView *v in self.view.subviews)
    {
        if([v isKindOfClass:[UILabel class]])
        {
            UILabel *typecast=(UILabel *)v;
            typecast.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
        }
        else if([v isKindOfClass:[UITextField class]])
        {
            UITextField *typecast=(UITextField *)v;
            typecast.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
            [typecast setValue:[UIColor lightTextColor] forKeyPath:@"_placeholderLabel.textColor"];
            typecast.tintColor=[UIColor colorWithRed:189/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
            
        }
    }
    [self setCustomNavigationButton];
    if(_fooddiarypageshowingdata)
    {
        //new code added
        _feellabel.hidden=NO;
        _feeltextfield.hidden=NO;
        _smileyimageview.hidden=NO;
        [_feeltextfield setValue:[UIColor lightTextColor] forKeyPath:@"_placeholderLabel.textColor"];
        _feeltextfield.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
        _feeltextfield.tintColor=[UIColor colorWithRed:189/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
        //end
        
        [_submitbutton setTitle:@"Delete" forState:UIControlStateNormal];
        _submitbutton.frame=CGRectMake(15, _submitbutton.frame.origin.y,_submitbutton.frame.size.width, _submitbutton.frame.size.height);
       // _updatebutton.frame=CGRectMake((_updatebutton.frame.size.width+_updatebutton.frame.origin.x), _updatebutton.frame.origin.y,_updatebutton.frame.size.width, _updatebutton.frame.size.height);
        [self loaddata];
    }
    else
    {
        // new code
        _feellabel.hidden=YES;
        _feeltextfield.hidden=YES;
        _smileyimageview.hidden=YES;
        _downarrayimageview.hidden=YES;
       
        //end
        
        _updatebutton.hidden=YES;
        [_submitbutton setTitle:@"Submit" forState:UIControlStateNormal];
        _submitbutton.frame=CGRectMake(_submitbutton.frame.origin.x-20, _submitbutton.frame.origin.y-20,_submitbutton.frame.size.width+40, _submitbutton.frame.size.height);
        
        // new frame set
        _calendericon.frame=CGRectMake(_calendericon.frame.origin.x, _calendericon.frame.origin.y-40, _calendericon.frame.size.width, _calendericon.frame.size.height);
         _dateTakenLabel.frame=CGRectMake(_dateTakenLabel.frame.origin.x, _dateTakenLabel.frame.origin.y-40, _dateTakenLabel.frame.size.width, _dateTakenLabel.frame.size.height);
         _datetakenbutton.frame=CGRectMake(_datetakenbutton.frame.origin.x, _datetakenbutton.frame.origin.y-40, _datetakenbutton.frame.size.width, _datetakenbutton.frame.size.height);
         _loadingicon.frame=CGRectMake(_loadingicon.frame.origin.x, _loadingicon.frame.origin.y-40, _loadingicon.frame.size.width, _loadingicon.frame.size.height);
         _timeTakenLabel.frame=CGRectMake(_timeTakenLabel.frame.origin.x, _timeTakenLabel.frame.origin.y-40, _timeTakenLabel.frame.size.width, _timeTakenLabel.frame.size.height);
         _timetakenbutton.frame=CGRectMake(_timetakenbutton.frame.origin.x, _timetakenbutton.frame.origin.y-40, _timetakenbutton.frame.size.width, _timetakenbutton.frame.size.height);
        
        // end
        
    }
    
    if(!feelingpickerview) {
        feelingpickerview=[[UIPickerView alloc]initWithFrame:CGRectMake(0, self.view.bounds.size.height+44,self.view.bounds.size.width, 216)];
        feelingpickerview.tag=1001;
        feelingpickerview.dataSource =self;
        feelingpickerview.delegate =self;
        [feelingpickerview setShowsSelectionIndicator:YES];
    }
    [_feeltextfield setInputView:feelingpickerview];
    
    // for now
    _smileyimageview.hidden=TRUE;
    
}
-(void)loaddata
{
    //new code added
    _feeltextfield.text=[_fooddiarypageshowingdata valueForKey:@"food_feel"];
    
    // new
    for(int i=0;i<[feelingarray count];i++)
    {
        if([_feeltextfield.text isEqualToString:[feelingarray objectAtIndex:i]])
        {
            _smileyimageview.image=[UIImage imageNamed:[smileyarray objectAtIndex:i]];
        }
    }
    
    _fooddiarylabel.text=[_fooddiarypageshowingdata valueForKey:@"foodname"];
    _foodName.text=[_fooddiarypageshowingdata valueForKey:@"foodname"];
    _foodDesc.text=[_fooddiarypageshowingdata valueForKey:@"fooddescription"];
    _dateTakenLabel.text=[_fooddiarypageshowingdata valueForKey:@"date"];
    _timeTakenLabel.text=[[_fooddiarypageshowingdata valueForKey:@"time"] stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSString *imageurl=[_fooddiarypageshowingdata valueForKey:@"image"];
    NSString *trimmed=[imageurl stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if([trimmed length]>0)
    {
//        __weak UIImageView *setimage = _uploadedImage;
//        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imageurl]];
//        [setimage setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"no_image.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
//         {
//             
//            _uploadlabel.text=@"Change photo from:";
//            setimage.image=image;
//             
//         }
//         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
//         {
//            setimage.image=[UIImage imageNamed:@"no_image.png"];
//         }];
        
//        trimmed=[[dict valueForKey:@"result"] valueForKey:@"user_profile_pic"];
        //new code for image load
        __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
       // 255-105-180
        activityIndicator.color=[UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
        activityIndicator.center = CGPointMake(CGRectGetMidX(_uploadedImage.bounds), CGRectGetMidY(_uploadedImage.bounds));
        activityIndicator.hidesWhenStopped = YES;
        
        __block BOOL successfetchimage=FALSE;
        __weak UIImageView *setimage = _uploadedImage;
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:trimmed]];
        [_uploadedImage setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"no_image.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             setimage.contentMode=UIViewContentModeScaleAspectFit;
             setimage.image=image;
             successfetchimage=TRUE;
             [activityIndicator removeFromSuperview];
         }
                                     failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             [activityIndicator removeFromSuperview];
         }];
        if(!successfetchimage)
        {
            if(![trimmed isEqualToString:@""]||trimmed)
            {
                [_uploadedImage addSubview:activityIndicator];
                [activityIndicator startAnimating];
            }
        }
        // end
 
    }
    else
    {
        _uploadedImage.image=[UIImage imageNamed:@"no_image.png"];
    }
    
    
    
}
-(void)setCustomNavigationButton
{
    
    UIButton *Menubutton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 28)];
    UIImageView *buttonImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 20, 18)];
    buttonImage.image=[UIImage imageNamed:@"menu_icon.png" ];
    buttonImage.clipsToBounds=YES;
    buttonImage.contentMode=UIViewContentModeScaleAspectFit;
    [Menubutton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    // set swrevealcontroller delegate
    self.revealViewController.delegate =self;
    
    [Menubutton addSubview:buttonImage];
    UIBarButtonItem *menubtn=[[UIBarButtonItem alloc]initWithCustomView:Menubutton];
    self.navigationItem.leftBarButtonItem=menubtn;
    
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"back_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    
    UIBarButtonItem *backbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombutton];
   // [backbarbutton setAction:@selector(back)];
   // [backbarbutton setTarget:self];
    backbarbutton.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItems = @[backbarbutton];
    
    // new code
    selectedimage=nil;

}

#pragma swrevealview delegate
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    [self dismisskeyboard];
}

-(void)back
{
   
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     pointtrackcontentoffset=self.view.frame.origin;
     storerect=self.view.frame;
    screenBounds = [[UIScreen mainScreen] bounds];
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    // new code
    hiderevealButton=[[UIButton alloc]initWithFrame:self.view.bounds];
    [hiderevealButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    hiderevealButton.backgroundColor=[UIColor clearColor];
    [self.view addSubview:hiderevealButton];
    [hiderevealButton setHidden:YES];
}
-(void)dismisskeyboard
{
    [self.view removeGestureRecognizer:tap];
    CGRect rect=self.view.frame;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    rect.origin.y=pointtrackcontentoffset.y;
    self.view.frame=rect;
    [UIView commitAnimations];
    //[[self.view viewWithTag:1000] removeFromSuperview];
    [self.activeTextField resignFirstResponder];
}
// picker
-(void)openpicker
{
    UIToolbar *feelingtoolbar;
    feelingtoolbar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    feelingtoolbar.tag = 1000;
    UIBarButtonItem *Clear=[[UIBarButtonItem alloc]initWithTitle:@"Clear" style:UIBarButtonItemStyleBordered target:self action:@selector(clearevent)];
    UIBarButtonItem *Cancel2=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(dismissDatePicker1:)];
     UIBarButtonItem *space2=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *done2=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pickerselection)];
    [feelingtoolbar setItems:[NSArray arrayWithObjects:Clear, Cancel2,space2,done2,nil]];
    [_feeltextfield setInputAccessoryView:feelingtoolbar];
}
-(void)clearevent
{
    _feeltextfield.text=@"";
    _smileyimageview.image=[UIImage imageNamed:@"no_image.png"];
    [self dismissDatePicker1:nil];
   // [self dismisskeyboard];
    
}

- (void)dismissDatePicker1:(id)sender
{
   
    [self dismisskeyboard];
    // new added
    [[self.view viewWithTag:1000] removeFromSuperview];
    [[self.view viewWithTag:1001] removeFromSuperview];
    
    UIPickerView   *picker = (UIPickerView*)[self.view viewWithTag:1001];
    if(picker)
        NSLog(@"Have");
    //[UIView commitAnimations];
}
-(void)pickerselection
{
    int index = (int)[feelingpickerview selectedRowInComponent:0];
    NSLog(@"index: %d", index);
    if(self.activeTextField ==_feeltextfield)
    {
        
        _feeltextfield.text=[feelingarray objectAtIndex:index];
        _smileyimageview.image=[UIImage imageNamed:[smileyarray objectAtIndex:index]];
           
        
    }
    [self dismissDatePicker1:nil];
    //[self dismisskeyboard];
}
#pragma mark -

#pragma mark Picker Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{

    return [NSString stringWithFormat:@"%@", [feelingarray objectAtIndex:row]];
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [feelingarray count];
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
    pointtrack =textField.frame.origin;
   
    // set up gesture
    tap = [[UITapGestureRecognizer alloc]
           initWithTarget:self
           action:@selector(dismisskeyboard)];
    tap.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:tap];
    //end
    if(self.activeTextField==_feeltextfield)
    {
        pointtrack =_innerviewoffeeling.frame.origin;
        [self openpicker];
    }

    CGRect rect=storerect;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    if(screenBounds.size.height==480)
    {
        rect.origin.y-=pointtrack.y-110;
    }
    else if(screenBounds.size.height==568)
    {
       rect.origin.y-=pointtrack.y-140;
    }
    else if(screenBounds.size.height==667)
    {
        if(pointtrack.y>460)
        {
            pointtrack.y=460;
        }
        rect.origin.y-=pointtrack.y-200;
    }
    else if(screenBounds.size.height==736)
    {
        if(pointtrack.y>580)
        {
            pointtrack.y=530;
        }
       rect.origin.y-=pointtrack.y-270;
    }
    else
    {
        
    }
    self.view.frame=rect;
    [UIView commitAnimations];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cameraButton:(id)sender
{
    [self takepicture];
}
-(void)takepicture
{
    /******************** code for camera ***********************/
    
    ImagePicker=[[UIImagePickerController alloc]init];
    if    (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
    }
    else
    {
        
        ImagePicker.delegate=(id)self;
        ImagePicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        // imagepic.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:ImagePicker animated:YES completion:nil];
        NSLog(@"camera");
    }
    
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    // new code
     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [picker dismissViewControllerAnimated:YES completion:nil];
    // [[UIApplication sharedApplication] setStatusBarHidden:YES];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    selectedimage=[info objectForKey:UIImagePickerControllerOriginalImage];
    selectedimage=[self scaleAndRotateImage:selectedimage];
    _uploadedImage.contentMode=UIViewContentModeScaleAspectFit;
    _uploadedImage.image=selectedimage;
    
    if(ImagePicker.sourceType==UIImagePickerControllerSourceTypeCamera)
    {
        // new code
        NSDate *date = [NSDate date];
        // format it
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"dd-MM-yyyy"];
        
        NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"h:mm a"];
        
        // convert it to a string
        NSString *dateString = [dateFormat stringFromDate:date];
        NSString *dateStringtime = [formatter stringFromDate:date];
        _dateTakenLabel.text=dateString;
        _timeTakenLabel.text=dateStringtime;
    }
    else
    {
    __block NSString *dateOriginalImage = @"";
    if ([info valueForKey:UIImagePickerControllerReferenceURL] != nil)
    {
        NSURL *imageURL = [info valueForKey:UIImagePickerControllerReferenceURL];
        
        if(imageURL)
        {
            ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
            [assetslibrary assetForURL:imageURL
                           resultBlock:^(ALAsset *myasset)
             {
                 ALAssetRepresentation *representation = [myasset defaultRepresentation];
                 NSDictionary *metadata = representation.metadata;
                 
                 NSDictionary *exifData = [metadata objectForKey:@"{Exif}"];
                 NSLog(@"Date dictionary=%@",[metadata objectForKey:@"{Exif}"]);
                 NSString *dateTimeOriginal = [exifData objectForKey:@"DateTimeOriginal"];
                 
                 if ([dateTimeOriginal length] == 0)
                 {
                     dateOriginalImage = @"";
                 }
                 else
                 {
                     dateOriginalImage = dateTimeOriginal;
                 }
                 
                 if(![dateOriginalImage isEqualToString:@""])
                 {
                 NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
                 [dateFormatter setDateFormat:@"yyyy:MM:dd HH:mm:ss"];
                 NSDate * dateNotFormatted = [dateFormatter dateFromString:dateTimeOriginal];
                 [dateFormatter setDateFormat:@"dd-MM-yyyy"];
                 NSString * dateFormatted = [dateFormatter stringFromDate:dateNotFormatted];
                 _dateTakenLabel.text=dateFormatted;
                 
                 NSDateFormatter * dateFormatter1 = [[NSDateFormatter alloc]init];
                 [dateFormatter1 setDateFormat:@"yyyy:MM:dd HH:mm:ss"];
                 NSDate * timeNotFormatted = [dateFormatter1 dateFromString:dateTimeOriginal];
                 [dateFormatter1 setDateFormat:@"hh:mm a"];
                 NSString * timeFormatted = [dateFormatter1 stringFromDate:timeNotFormatted];
                 _timeTakenLabel.text=timeFormatted;
                 }
                 else
                 {
                       _dateTakenLabel.text=@"Date Taken";
                       _timeTakenLabel.text=@"Time Taken";
                 }
                 
             }
                failureBlock:^(NSError *myerror)
             {
                 NSLog(@"%@",myerror);
             }];
        }
    }
    }

    [picker dismissViewControllerAnimated:YES completion:nil];
    
    
}
// new method
- (UIImage *)scaleAndRotateImage:(UIImage *)image
{
    int kMaxResolution = 640; // Or whatever
    CGImageRef imgRef = image.CGImage;
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

- (IBAction)galleryButton:(id)sender
{
    /******************** code for Photo Library ***********************/
     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    ImagePicker=[[UIImagePickerController alloc]init];
    ImagePicker.delegate=(id)self;
    ImagePicker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    // imagepic.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:ImagePicker animated:YES completion:nil];
}
- (IBAction)dateTaken:(id)sender
{
    self.activebutton=(UIButton *)sender;

    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height-216-44, self.view.bounds.size.width, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height-216, self.view.bounds.size.width, 216);
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height+44,self.view.bounds.size.width, 216)];
    datePicker.tag = 10;
    datePicker.backgroundColor=[UIColor whiteColor];
    
    if(self.activebutton.tag==20)
    {
         datePicker.datePickerMode=UIDatePickerModeDate;
    // new code
        if(![_dateTakenLabel.text isEqualToString:@"Date Taken"])
        {
            // Convert string to date object
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"dd-MM-yyyy"];
            NSDate *date = [dateFormat dateFromString:_dateTakenLabel.text];
            datePicker.date=date;
        }
   
    }
    else
    {
        // new code
        if(![_timeTakenLabel.text isEqualToString:@"Time Taken"])
        {
            // Convert string to date object
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"hh:mm a"];
            NSDate *date = [dateFormat dateFromString:_timeTakenLabel.text];
            datePicker.date=date;
        }
    datePicker.datePickerMode=UIDatePickerModeTime;
    }
    //[datePicker addTarget:self action:@selector(changeDate:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:datePicker];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44)];
    toolBar.tag = 11;
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.backgroundColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    UIBarButtonItem *Cancel=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(dismissDatePicker:)];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(changeDate:)];
    [toolBar setItems:[NSArray arrayWithObjects:Cancel,spacer, doneButton, nil]];
    [self.view addSubview:toolBar];
    
    [UIView beginAnimations:@"MoveIn" context:nil];
    toolBar.frame = toolbarTargetFrame;
    datePicker.frame = datePickerTargetFrame;
    [UIView commitAnimations];
}

- (IBAction)timeTaken:(id)sender
{
    self.activebutton=(UIButton *)sender;
    [self dateTaken:sender];
    
}

// new code
- (void)changeDate:(id)sender
{
    if(self.activebutton.tag==20)
    {
    NSDateFormatter *format=[[NSDateFormatter alloc]init];
    [format setDateFormat:@"dd-MM-yyyy"];
    _dateTakenLabel.text=[NSString stringWithFormat:@"%@",[format stringFromDate:datePicker.date ]];
    }
    else
    {
        NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"h:mm a"];
        _timeTakenLabel.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date ]];
    }
    [self dismissDatePicker:nil];
    
}

- (void)removeViews:(id)object
{
    [[self.view viewWithTag:10] removeFromSuperview];
    [[self.view viewWithTag:11] removeFromSuperview];
    
    
    
   
}

- (void)dismissDatePicker:(id)sender
{
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, self.view.bounds.size.width, 216);
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];
}
// dynamic method
-(BOOL)validation:(NSString *)sendstring
{
    NSString *trimmed=sendstring;
    trimmed=[trimmed stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if([trimmed length]==0)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}
-(void)deletefoodwebservice
{
    NSString *path =[NSString stringWithFormat:@"%@1017",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",[_fooddiarypageshowingdata valueForKey:@"ID"],@"ID",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Deleting Food Entry.. " maskType:SVProgressHUDMaskTypeNone];
    //[SVProgressHUD showWithStatus:@"Please wait a moment..."];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"error"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
              [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
         }
         else
         {
             [self.delegate buttonTappeddelete:_sendindex];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
             UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Food Entry Deleted" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             alert.tag=101;
             [alert show];
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}

- (IBAction)didTapSubmit:(id)sender
{
    if([_submitbutton.titleLabel.text isEqualToString:@"Delete"])
    {
        [self deletefoodwebservice];
        
    }
    else
    {
   
    if(![self validation:_foodName.text])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please Fill Food Name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if(![self validation:_foodDesc.text])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please Fill Food Desc" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([_dateTakenLabel.text isEqualToString:@"Date Taken"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please Fill Date Taken" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([_timeTakenLabel.text isEqualToString:@"Time Taken"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please Fill Time Taken" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
     [self webservicesubmitaddfooddetail];
    }
    
}
-(void)webservicesubmitaddfooddetail
{
    
    NSString *path =[NSString stringWithFormat:@"%@816",app->parentUrl];
    // AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://server.url"]];
   // AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
     [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Adding To Diary.."];
   imageDataPicker=(NSData *)UIImagePNGRepresentation(selectedimage);
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",_foodName.text,@"foodname",_foodDesc.text,@"fooddescription",_dateTakenLabel.text,@"date",_timeTakenLabel.text,@"time",nil];
    AFHTTPRequestOperation *op = [manager POST:path parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //do not put image inside parameters dictionary as I did, but append it!
        if(imageDataPicker)
        {
        [formData appendPartWithFileData:imageDataPicker name:@"image" fileName:@"photo.png" mimeType:@"image/png"];
        }
    }
    success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
        NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",responseStr);
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
        NSLog(@"%@",dict);
        //here is place for code executed in success case
        if([[dict valueForKey:@"status"]integerValue]==0)
        {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                    [SVProgressHUD dismiss];
        }
        else
        {
                  [self.delegate buttonTappedsubmit];
                   UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Added to Diary" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                     alert.tag=101;
                    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                    [SVProgressHUD dismiss];
        }
                                     
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                  
    {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
        NSLog(@"Error: %@", [error localizedDescription]);
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
                                  ];
    [op start];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==101)
    {
        [self back];
    }
}

- (IBAction)didTapUpdate:(id)sender
{
    if(![self validation:_foodName.text])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please Fill Food Name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if(![self validation:_foodDesc.text])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please Fill Food Desc" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([_dateTakenLabel.text isEqualToString:@"Date Taken"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please Fill Date Taken" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if([_timeTakenLabel.text isEqualToString:@"Time Taken"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please Fill Time Taken" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    [self webserviceupdatefooddiary];
}
-(void)webserviceupdatefooddiary
{
     //userid,foodname,fooddescription,date,time,ID,image(this for image.you have to send image file)
    
    NSString *path =[NSString stringWithFormat:@"%@1053",app->parentUrl];
    // AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://server.url"]];
    // AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Updating Your Food Entry.."];
    imageDataPicker=(NSData *)UIImagePNGRepresentation(selectedimage);
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",_foodName.text,@"foodname",_foodDesc.text,@"fooddescription",_dateTakenLabel.text,@"date",_timeTakenLabel.text,@"time",[_fooddiarypageshowingdata valueForKey:@"ID"],@"ID",_feeltextfield.text,@"food_feel",nil];
    AFHTTPRequestOperation *op = [manager POST:path parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //do not put image inside parameters dictionary as I did, but append it!
        if(imageDataPicker)
        {
            [formData appendPartWithFileData:imageDataPicker name:@"image" fileName:@"photo.png" mimeType:@"image/png"];
        }
    }
                                       success:^(AFHTTPRequestOperation *operation, id responseObject)
                                  {
                                      NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
                                      NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
                                      NSLog(@"%@",responseStr);
                                      NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
                                      NSLog(@"%@",dict);
                                      //here is place for code executed in success case
                                      if([[dict valueForKey:@"status"]integerValue]==0)
                                      {
                                          UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                          [alert show];
                                          [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                          [SVProgressHUD dismiss];
                                      }
                                      else
                                      {
                                          [self.delegate buttonTappedsubmit];
                                          UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Food Entry Updated" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                          [alert show];
                                          alert.tag=101;
                                          [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                          [SVProgressHUD dismiss];
                                      }
                                      
                                  }
                                       failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                  
                                  {
                                      [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                      [SVProgressHUD dismiss];
                                      NSLog(@"Error: %@", [error localizedDescription]);
                                      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                      [alert show];
                                  }
                                  ];
    [op start];
}
@end
