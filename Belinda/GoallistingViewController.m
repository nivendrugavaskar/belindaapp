//
//  GoallistingViewController.m
//  Belinda
//
//  Created by Nivendru on 18/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "GoallistingViewController.h"

@interface GoallistingViewController ()
{
    NSMutableArray *goalarray,*statusarray;
     BOOL maintainwillappearservice;
    
    NSString *extendstatus,*sendacheivestatus;
    NSIndexPath *globalindex;
    // new code
    NSString *goalidvalue,*goalname;
}

@end

@implementation GoallistingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    maintainwillappearservice=FALSE;
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:220/255.0f green:25/255.0f blue:124/255.0f alpha:1.0f];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.title=@"Goals";
   #pragma testing with local data without server
   // NSLog(@"Global array count=%lu",(unsigned long)[app.testingpurposearray count]);
   // goalarray=app.testingpurposearray;
   // statusarray=[[NSMutableArray alloc]initWithObjects:@"Completed",@"Incompleted",@"In-Progress", nil];
    
    // Hide navigation bar and customize
    self.navigationItem.hidesBackButton=YES;
    [self setCustomNavigationButton];
    
    
    
}

-(void)webservicegoallisting
{
    NSString *path =[NSString stringWithFormat:@"%@1562",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
   
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Listing Your Goals.."];

    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         //here is place for code executed in success case
         if([[dict valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No Goals Created" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
         }
         else
         {
             goalarray=[dict valueForKey:@"result"];
             [self.aTableView reloadData];
             
         }
         if([goalarray count]==0)
         {
              _lblnogoal.hidden=NO;
         }
         else
         {
             _lblnogoal.hidden=YES;
         }
         // new code
         maintainwillappearservice=TRUE;
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
         // new code for push notification
         if(app->pushdictionary)
         {
             [self pushservice];
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}
-(void)setCustomNavigationButton
{
    
    UIButton *Menubutton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 28)];
    UIImageView *buttonImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 20, 18)];
    buttonImage.image=[UIImage imageNamed:@"menu_icon.png" ];
    buttonImage.clipsToBounds=YES;
    buttonImage.contentMode=UIViewContentModeScaleAspectFit;
    [Menubutton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [Menubutton addSubview:buttonImage];
    UIBarButtonItem *menubtn=[[UIBarButtonItem alloc]initWithCustomView:Menubutton];
    self.navigationItem.leftBarButtonItem=menubtn;
    
    // Adding right navigation bar button
    UIBarButtonItem *addbarbutton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addfoodlibrary)];
    addbarbutton.tintColor=[UIColor whiteColor];
    
    
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"back_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    
    UIBarButtonItem *backbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombutton];
    //[backbarbutton setAction:@selector(back)];
    // [backbarbutton setTarget:self];
    backbarbutton.style = UIBarButtonItemStylePlain;
    // end
    
    // new code
    if(app->checkcustompush)
    {
        self.navigationItem.rightBarButtonItems = @[addbarbutton];
    }
    else
    {
        self.navigationItem.rightBarButtonItems = @[addbarbutton,backbarbutton];
    }
    
}

-(void)back
{
    NSLog(@"Back");
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(void)addfoodlibrary
{
    NSLog(@"Add");
    [self performSegueWithIdentifier:@"seguetogoalform" sender:nil];
    
   
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Set the gesture
    app->clickedstatusgoal=@"";
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    // new code
    hiderevealButton=[[UIButton alloc]initWithFrame:self.view.bounds];
    [hiderevealButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    hiderevealButton.backgroundColor=[UIColor clearColor];
    [self.view addSubview:hiderevealButton];
    [hiderevealButton setHidden:YES];
    // call foodlisting webservice
    if(!maintainwillappearservice)
    {
    [self webservicegoallisting];
    }
}

#pragma tableviewdelegates and datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"Cell";
    Goallistingcell *cell=(Goallistingcell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    //    // new
    //    cell.delegate=(id)self;
    
    if(cell==nil)
    {
        cell=[[Goallistingcell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    // set text colour and seperator colour
    cell.foodName.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    cell.status.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    cell.seperatorlabel.backgroundColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    
    @try
    {
        cell.status.frame=CGRectMake(self.view.bounds.size.width-130,cell.status.frame.origin.y, cell.status.frame.size.width,cell.status.frame.size.height);
        cell.seperatorlabel.frame=CGRectMake(0, cell.seperatorlabel.frame.origin.y, self.aTableView.frame.size.width, cell.seperatorlabel.frame.size.height);
        cell.foodName.text=[[goalarray objectAtIndex:indexPath.row] valueForKey:@"goalname"];
        NSString *status=[[goalarray objectAtIndex:indexPath.row] valueForKey:@"goal_status"];
        if([status isEqualToString:@"0"])
        {
            status=@"In Progress";
        }
        else if([status isEqualToString:@"1"])
        {
            status=@"Acheived";
        }
        else if([status isEqualToString:@"2"])
        {
            status=@"Not-Acheived";
        }
        else if([status isEqualToString:@"3"])
        {
            status=@"Not-Started";
        }
        else if([status isEqualToString:@"4"])
        {
            status=@"In Progress(Ext)";
        }
        else if([status isEqualToString:@"5"]||[status isEqualToString:@"6"])
        {
            status=@"Update Status";
        }
        cell.status.text=status;
    }
    @catch (NSException *exception)
    {
        NSLog(@"Description=%@",exception.description);
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [goalarray count];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"indexpathrow=%ld",(long)indexPath.row);
//  [self performSegueWithIdentifier:@"segueToAddFoodDiary" sender:indexPath];
    globalindex=indexPath;
    [self webservicegoalperiodchecking];
    
}

#pragma via push notification
-(void)pushservice
{
    [self webservicegoalperiodchecking];
}

#pragma checking period
-(void)webservicegoalperiodchecking
{
    
    
    if(!app->pushdictionary)
    {
      goalidvalue=[[goalarray objectAtIndex:globalindex.row] valueForKey:@"ID"];
      goalname=[[goalarray objectAtIndex:globalindex.row] valueForKey:@"goalname"];
    }
    else
    {
     goalidvalue=[[app->pushdictionary valueForKey:@"aps"] valueForKey:@"id"];
        // new code added
       for(int i=0;i<[goalarray count];i++)
        {
           if([[[goalarray objectAtIndex:i] valueForKey:@"ID"]isEqualToString:goalidvalue])
           {
               globalindex=[NSIndexPath indexPathForRow:i inSection:0];
               goalname=[[goalarray objectAtIndex:i] valueForKey:@"goalname"];
           }
        }
        
    }
    
        NSString *path =[NSString stringWithFormat:@"%@1442",app->parentUrl];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",goalidvalue,@"goalid",nil];
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        [SVProgressHUD showWithStatus:@"Checking Your Goal Status..."];
        [manager POST:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
             NSLog(@"%@",responseStr);
             NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
             NSLog(@"%@",dict);
             //here is place for code executed in success case
             if([[dict valueForKey:@"status"]integerValue]==0)
             {
                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"error"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 [alert show];
    
             }
             else
             {
                 app->clickedstatusgoal=[[dict valueForKey:@"result"] valueForKey:@"status"];
                 extendstatus=[[dict valueForKey:@"result"] valueForKey:@"extendstatus"];
                 if([app->clickedstatusgoal isEqualToString:@"firststate"]&&[extendstatus isEqualToString:@"0"])
                 {
                 [self performSegueWithIdentifier:@"seguetogoalform" sender:globalindex];
                 }
                 else
                 {

                  if([app->clickedstatusgoal isEqualToString:@"secondstate"]&&[extendstatus isEqualToString:@"0"])
                  {
                    if([[[goalarray objectAtIndex:globalindex.row] valueForKey:@"goal_status"] isEqualToString:@"5"])
                    {
                        
                    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Have you acheived %@",goalname] message:nil delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:@"Yes",@"No",@"Goal Details",nil];
                    alert.tag=100;
                    [alert show];
                    }
                    else
                    {
                    //[self performSegueWithIdentifier:@"seguetogoalform" sender:globalindex];
                        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Want to see %@",goalname] message:nil delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:@"Goal Details",@"Final Comments", nil];
                        alert.tag=103;
                        [alert show];
                    }
                    }
                   else if([app->clickedstatusgoal isEqualToString:@"secondstate"]&&[extendstatus isEqualToString:@"1"])
                   {
                    NSLog(@"Only Show Data");
                    [self performSegueWithIdentifier:@"seguetogoalform" sender:globalindex];
                       
                  }
                 else if([app->clickedstatusgoal isEqualToString:@"thirdstate"]&&[extendstatus isEqualToString:@"1"])
                {
                if([[[goalarray objectAtIndex:globalindex.row] valueForKey:@"goal_status"] isEqualToString:@"6"]||[[[goalarray objectAtIndex:globalindex.row] valueForKey:@"goal_status"] isEqualToString:@"0"])
                {
                 UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Have you acheived %@",goalname] message:nil delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:@"Yes",@"No",@"Goal Details", nil];
                alert.tag=100;
                [alert show];
                }
                else
                {
                    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Want to see %@",goalname] message:nil delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:@"Goal Details",@"Final Comments", nil];
                    alert.tag=103;
                    [alert show];
                }
                }
                else
                {
                    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@ has not yet started",goalname] message:nil delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:@"Goal Details", nil];
                    alert.tag=102;
                    [alert show];
                }
                }
             }
             [SVProgressHUD dismiss];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             // new code to maintain push
             if(app->pushdictionary)
             {
                 app->pushdictionary=nil;
             }
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error){
             
             //here is place for code executed in success case
             [SVProgressHUD dismiss];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             NSLog(@"Error: %@", [error localizedDescription]);
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
         }];
   
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==100)
    {
        if(buttonIndex==3)
        {
           [self performSegueWithIdentifier:@"seguetogoalform" sender:globalindex];
        }
        else if(buttonIndex==1)
        {
            NSLog(@"Yes");
            sendacheivestatus=@"Acheived";
            [self performSegueWithIdentifier:@"seguetocommentsection" sender:sendacheivestatus];
        }
        else if(buttonIndex==2)
        {
            NSLog(@"No");
            if([extendstatus isEqualToString:@"1"])
            {
                NSLog(@"No option of extend");
                sendacheivestatus=@"NotAcheived";
               [self performSegueWithIdentifier:@"seguetocommentsection" sender:sendacheivestatus];
            }
            else
            {
                UIAlertView *extendalert=[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Do You Want To Extend\n %@",goalname] message:nil delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:@"Yes",@"Not Acheived",nil];
                extendalert.tag=101;
                [extendalert show];
            }
        }
       
    }
    
    else if(alertView.tag==101)
    {
        if(buttonIndex==0)
        {
            NSLog(@"Dismiss");
        }
        else if(buttonIndex==1)
        {
            sendacheivestatus=@"extend";
            NSLog(@"Extended");
            [alertView dismissWithClickedButtonIndex:1 animated:YES];
            [self performSelectorInBackground:@selector(loadhudextend) withObject:self];
            [self webserviceextendcase];
            
        }
        else if(buttonIndex==2)
        {
            NSLog(@"No option of extend");
            sendacheivestatus=@"NotAcheived";
            [self performSegueWithIdentifier:@"seguetocommentsection" sender:sendacheivestatus];
        }
    }
    else if(alertView.tag==102)
    {
        if(buttonIndex==1)
        {
            [self performSegueWithIdentifier:@"seguetogoalform" sender:globalindex];
        }
        else
        {
            NSLog(@"Dismiss");
        }
    }
    else if(alertView.tag==103)
    {
        if(buttonIndex==0)
        {
            NSLog(@"Dismiss");
        }
        else if(buttonIndex==1)
        {
            [self performSegueWithIdentifier:@"seguetogoalform" sender:globalindex];
            
        }
        else if(buttonIndex==2)
        {
            //pushtocommentpage
             [self performSegueWithIdentifier:@"pushtocommentpage" sender:@"yes"];
        }
    }
    
}
// new code hud maintain
-(void)loadhudextend
{
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Checking Your extended status.."];
}

// extendapi
-(void)webserviceextendcase
{
    
        NSString *path =[NSString stringWithFormat:@"%@1458",app->parentUrl];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",[[goalarray objectAtIndex:globalindex.row] valueForKey:@"ID"],@"goalid",@"3",@"status",nil];
        [manager POST:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
             NSLog(@"%@",responseStr);
             NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
             NSLog(@"%@",dict);
             //here is place for code executed in success case
             if([[dict valueForKey:@"status"]integerValue]==0)
             {
                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"error"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 [alert show];
                 
             }
             else
             {
                maintainwillappearservice=FALSE;
                [self viewWillAppear:NO];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Your Goal has Extended.." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
             }
             [SVProgressHUD dismiss];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error){
             
             //here is place for code executed in success case
             [SVProgressHUD dismiss];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             NSLog(@"Error: %@", [error localizedDescription]);
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
         }];
        
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"seguetogoalform"])
    {
    QuickfitnessViewController *instance=(QuickfitnessViewController *)segue.destinationViewController;
    if([sender isKindOfClass:[NSIndexPath class]])
    {
        NSIndexPath *indexpath=(NSIndexPath *)sender;
       // NSMutableArray *testdata=[[NSMutableArray alloc] initWithObjects:@"test1",@"test2", nil];
        instance.specificgoaldata=[goalarray objectAtIndex:indexpath.row];
        
    }
    else
    {
        instance.goallistingdelegate=self;
    }
    }
   // seguetocommentsection
   else if([segue.identifier isEqualToString:@"seguetocommentsection"])
    {
        Goalstatusviewcontroller *instance=(Goalstatusviewcontroller *)segue.destinationViewController;
        if([sender isKindOfClass:[NSString class]])
        {
            NSString *statusstring=(NSString *)sender;
            // NSMutableArray *testdata=[[NSMutableArray alloc] initWithObjects:@"test1",@"test2", nil];
            instance.commentstatus=statusstring;
            instance.fetchgoalid=[[goalarray objectAtIndex:globalindex.row] valueForKey:@"ID"];
            instance.goalstatusdelegate=self;
        }
        else
        {
           instance.goalstatusdelegate=self;
        }
    }
  //pushtocommentpagewithshow
   else if([segue.identifier isEqualToString:@"pushtocommentpage"])
   {
       Goalstatusviewcontroller *instance=(Goalstatusviewcontroller *)segue.destinationViewController;
       if([sender isKindOfClass:[NSString class]])
       {
         NSMutableArray *testdata=[[goalarray objectAtIndex:globalindex.row] valueForKey:@"commentdetails"];
           instance.fetchdataDict=testdata;
//           instance.goalstatusdelegate=self;
       }
       else
       {
           //instance.goalstatusdelegate=self;
       }
   }
    
}
// Quick fitness protocol
-(void)sendflagtrue:(BOOL)flag
{
   // new code
    maintainwillappearservice=flag;
}
// Goalstatusprotocol
-(void)sendflagtrueingoalstatussuccess:(BOOL)flag
{
    // new code
    maintainwillappearservice=flag;
}

//// new testing code through protocol
//-(void)sendarraydata:(NSMutableArray *)array
//{
//    //NSLog(@"Array=%@",array);
//    goalarray=array;
//    [self.aTableView reloadData];
//}

@end
