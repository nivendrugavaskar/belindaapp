//
//  PreviewImageViewController.m
//  Belinda
//
//  Created by Nivendru on 20/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "PreviewImageViewController.h"

@interface PreviewImageViewController ()
{
    UIDocumentInteractionController *_docController;
    UIImage *sharedimage;
}

@end

@implementation PreviewImageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
//    if(_previewimagestring)
//    {
//        _bigImageView.image=[UIImage imageNamed:_previewimagestring];
//    }
#pragma for testing
//    int location=(int)[_previewimagestring rangeOfString:@"."].location;
//    NSString *imagename=[_previewimagestring substringToIndex:location];
//    NSString *filetype=[_previewimagestring substringFromIndex:location+1];
//    NSString *imagepath=[[NSBundle mainBundle] pathForResource:imagename ofType:filetype];
//    NSURL *myImagePath = [NSURL fileURLWithPath:imagepath];
    // end
    
    
    __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.color=[UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
    activityIndicator.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    activityIndicator.hidesWhenStopped = YES;
    
     __block BOOL successfetchimage=FALSE;
    __weak UIImageView *setimage = _bigImageView;
     NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:_previewimagestring]];
    [_bigImageView setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"no_image.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         setimage.contentMode=UIViewContentModeScaleAspectFit;
         setimage.image=image;
         successfetchimage=TRUE;
         [activityIndicator removeFromSuperview];
     }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
     {
      [activityIndicator removeFromSuperview];
     }];
    
    if(!successfetchimage)
    {
    [_bigImageView addSubview:activityIndicator];
    [activityIndicator startAnimating];
    }
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//UIImage *instaImage = wallpaperImageView.image;
//NSString* imagePath = [NSString stringWithFormat:@"%@/image.png",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]];
//[[NSFileManager defaultManager] removeItemAtPath:imagePath error:nil];
//
//[UIImagePNGRepresentation(instaImage) writeToFile:imagePath atomically:YES];
//
//self._docController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:imagePath]];
//self._docController.delegate=self;
//[self._docController presentOptionsMenuFromRect:self.view.frame inView:self.view animated:YES];

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didTapdone:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)loadhud
{
    [SVProgressHUD showWithStatus:@"Please wait"];
}

- (IBAction)didtapshare:(id)sender
{
    BOOL success;
    [self performSelectorInBackground:@selector(loadhud) withObject:self];
    sharedimage=_bigImageView.image;
    
#pragma if you want to share image on instagram
   /* UIImage *imageToUse = [UIImage imageNamed:@"imageToShare.png"];
    NSString *documentDirectory=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *saveImagePath=[documentDirectory stringByAppendingPathComponent:@"Image.igo"];
    NSData *imageData=UIImagePNGRepresentation(imageToUse);
    [imageData writeToFile:saveImagePath atomically:YES];
    NSURL *imageURL=[NSURL fileURLWithPath:saveImagePath];
    _docController = [UIDocumentInteractionController interactionControllerWithURL:imageURL];
    _docController.delegate = self;
    _docController.annotation = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"This is the users caption that will be displayed in Instagram"], @"InstagramCaption", nil];
    _docController.UTI = @"com.instagram.exclusivegram";
    [_docController presentOpenInMenuFromRect:CGRectMake(1, 1, 1, 1) inView:self.view animated:YES];*/
    
    
   // UIImage *instaImage = [UIImage imageNamed:_previewimagestring];
    NSString* imagePath = [NSString stringWithFormat:@"%@/%@.png",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],_imagetype];
    [[NSFileManager defaultManager] removeItemAtPath:imagePath error:nil];
    success=[UIImagePNGRepresentation(sharedimage) writeToFile:imagePath atomically:YES];
   
    if(success)
    {
        [SVProgressHUD dismiss];
    _docController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:imagePath]];
    _docController.delegate=self;
    [_docController presentOptionsMenuFromRect:self.view.frame inView:self.view animated:YES];
    }
}
@end
