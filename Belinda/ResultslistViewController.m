//
//  ResultslistViewController.m
//  Belinda
//
//  Created by Nivendru on 18/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "ResultslistViewController.h"

@interface ResultslistViewController ()
{
    NSMutableArray *goalnamearray,*startimagearray,*alldataarray;
    NSDictionary *dict;
}

@end

@implementation ResultslistViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:220/255.0f green:25/255.0f blue:124/255.0f alpha:1.0f];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.title=@"Results";
    
    // Hide navigation bar and customize
    self.navigationItem.hidesBackButton=YES;
    [self setCustomNavigationButton];
    
    // call resultassesmentlisting webservice
    [self webserviceresultlisting];
    
    // for testing hardcoaded
   // goalnamearray=[[NSMutableArray alloc] initWithObjects:@"Assesment (10-11-2014)",@"Assesment (12-11-2014)",@"Assesment (13-11-2014)",@"Assesment (14-11-2014)", nil];
   // startimagearray=[[NSMutableArray alloc] initWithObjects:@"test.png",@"no_image.png",@"test.png",@"no_image.png", nil];

}
#pragma results listing
-(void)webserviceresultlisting
{
    NSString *path =[NSString stringWithFormat:@"%@1283",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Loading Results.." maskType:SVProgressHUDMaskTypeNone];
    //[SVProgressHUD showWithStatus:@"Please wait a moment..."];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"error"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
         }
         else
         {
            
             alldataarray=[dict valueForKey:@"result"];
            // goalnamearray=[[NSMutableArray alloc] init];
             goalnamearray=[[dict valueForKey:@"result"] valueForKey:@"ass_name_with_date"];
             //startimagearray=[[NSMutableArray alloc] init];
             startimagearray=[[dict valueForKey:@"result"] valueForKey:@"frontimage"];
             [self.aTableView reloadData];
         }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}

-(void)setCustomNavigationButton
{
    
    UIButton *Menubutton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 28)];
    UIImageView *buttonImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 20, 18)];
    buttonImage.image=[UIImage imageNamed:@"menu_icon.png" ];
    buttonImage.clipsToBounds=YES;
    buttonImage.contentMode=UIViewContentModeScaleAspectFit;
    [Menubutton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [Menubutton addSubview:buttonImage];
    UIBarButtonItem *menubtn=[[UIBarButtonItem alloc]initWithCustomView:Menubutton];
    self.navigationItem.leftBarButtonItem=menubtn;
    
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"back_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    
    UIBarButtonItem *backbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombutton];
    backbarbutton.style = UIBarButtonItemStylePlain;
    // end
    
    // new code
    if(!app->checkcustompush)
    {
        self.navigationItem.rightBarButtonItems = @[backbarbutton];
    }
    
    
}
-(void)back
{
    NSLog(@"Back");
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:NO];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    // new code
    hiderevealButton=[[UIButton alloc]initWithFrame:self.view.bounds];
    [hiderevealButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    hiderevealButton.backgroundColor=[UIColor clearColor];
    [self.view addSubview:hiderevealButton];
    [hiderevealButton setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma tableviewdelegates and datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"Cell";
    Resultlistcell *cell=(Resultlistcell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    //    // new
    //    cell.delegate=(id)self;
    // set text colour and seperator colour
    cell.goalname.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    cell.separatorlabel.backgroundColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    
    if(cell==nil)
    {
        cell=[[Resultlistcell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    
    @try
    {
        cell.startimage.frame=CGRectMake(self.view.bounds.size.width-55,cell.startimage.frame.origin.y, cell.startimage.frame.size.width,cell.startimage.frame.size.height);
        cell.separatorlabel.frame=CGRectMake(0, cell.separatorlabel.frame.origin.y, self.aTableView.frame.size.width, cell.separatorlabel.frame.size.height);
        cell.goalname.text=[goalnamearray objectAtIndex:indexPath.row];
        
        //new code for image load
        __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityIndicator.color=[UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
        activityIndicator.center = CGPointMake(CGRectGetMidX(cell.startimage.bounds), CGRectGetMidY(cell.startimage.bounds));
        activityIndicator.hidesWhenStopped = YES;
        NSString *myImagePath=[startimagearray objectAtIndex:indexPath.row];
        __block BOOL successfetchimage=FALSE;
        __weak UIImageView *setimage = cell.startimage;
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:myImagePath]];
        [cell.startimage setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"no_image.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             setimage.contentMode=UIViewContentModeScaleAspectFit;
             setimage.image=image;
             successfetchimage=TRUE;
             [activityIndicator removeFromSuperview];
         }
                                   failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             [activityIndicator removeFromSuperview];
         }];
        if(!successfetchimage)
        {
            if(![myImagePath isEqualToString:@""])
            {
                [cell.startimage addSubview:activityIndicator];
                [activityIndicator startAnimating];
            }
        }
        // end
#pragma test purpose
        //cell.startimage.image=[UIImage imageNamed:[startimagearray objectAtIndex:indexPath.row]];
       
       
    }
    @catch (NSException *exception)
    {
        NSLog(@"Description=%@",exception.description);
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [goalnamearray count];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"indexpathrow=%ld",(long)indexPath.row);
    [self performSegueWithIdentifier:@"seguetoviewassesment" sender:indexPath];
    
}
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"seguetoviewassesment"])
    {
        essentialGoalListViewController *instance = (essentialGoalListViewController *)segue.destinationViewController;
        if([sender isKindOfClass:[NSIndexPath class]])
        {
            NSIndexPath *indexPath = (NSIndexPath *)sender;
            NSLog(@"row=%ld",(long)indexPath.row);
            NSLog(@"section=%ld",(long)indexPath.section);
            instance.assementarraydata=[alldataarray objectAtIndex:indexPath.row];
            
        }

        
    }
 }

@end
