//
//  GoallistingViewController.h
//  Belinda
//
//  Created by Nivendru on 18/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Goallistingcell.h"
#import "QuickfitnessViewController.h"
#import "Goalstatusviewcontroller.h"

@interface GoallistingViewController :  UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,goallistingprotocol,goalstatusprotocol>
{
    AppDelegate *app;
}
@property (strong, nonatomic) IBOutlet UITableView *aTableView;
@property (weak, nonatomic) IBOutlet UILabel *lblnogoal;
@end
