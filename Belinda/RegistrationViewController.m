//
//  RegistrationViewController.m
//  Belinda
//
//  Created by Nivendru on 28/10/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "RegistrationViewController.h"

@interface RegistrationViewController ()
{
    CGRect screenBounds;
     AppDelegate *app;
    UITapGestureRecognizer *tap;
    NSDictionary *dict;
}

@end

@implementation RegistrationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    [self registerForKeyboardNotifications];
    pointtrackcontentoffset=self.aScrollView.contentOffset;
    screenBounds = [[UIScreen mainScreen] bounds];
   
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    for (UITextField *v in self.innerViewScrollView.subviews)
    {
        if([v isKindOfClass:[UITextField class]])
        {
            [v setValue:[UIColor lightTextColor] forKeyPath:@"_placeholderLabel.textColor"];
            v.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
        }
    }
}

-(void)dismisskeyboard
{
    [self.activeTextField resignFirstResponder];
    [self.view removeGestureRecognizer:tap];
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
   NSDictionary* info = [aNotification userInfo];
   kbsize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
     [self.aScrollView setContentOffset:pointtrackcontentoffset animated:NO];
    [self.view removeGestureRecognizer:tap];
}

// # Text Field Delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
    pointtrack = textField.frame.origin;
    NSLog(@"%f",screenBounds.size.height);
    // dismiss when outside click
    tap = [[UITapGestureRecognizer alloc]
           initWithTarget:self
           action:@selector(dismisskeyboard)];
    tap.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:tap];
    
    if(pointtrack.y>375)
    {
        pointtrack.y=375;
    }
    if(screenBounds.size.height==480)
    {
    [self.aScrollView setContentOffset:CGPointMake(0, pointtrack.y-85) animated:YES];
    }
    else if(screenBounds.size.height==568)
    {
    [self.aScrollView setContentOffset:CGPointMake(0, pointtrack.y-160) animated:YES];
    }
    else if(screenBounds.size.height==667)
    {
       
    [self.aScrollView setContentOffset:CGPointMake(0, pointtrack.y-200) animated:YES];
    }
    else if(screenBounds.size.height==736)
    {
    [self.aScrollView setContentOffset:CGPointMake(0, pointtrack.y-220) animated:YES];
    }
    else 
    {
    [self.aScrollView setContentOffset:CGPointMake(0, pointtrack.y-327) animated:YES];
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
         [self.aScrollView setContentOffset:pointtrackcontentoffset animated:NO];
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    if (textField ==_emailIdText || textField==_passwordText||textField ==_userName || textField==_confirmpwdtext)
    {
        if ([string isEqualToString:@" "])
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)cancelReg:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (BOOL) validateEmail:(NSString *) emailid
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailid];
}
-(void)trimmingtext
{
    _emailIdText.text = [ _emailIdText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _passwordText.text = [ _passwordText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (IBAction)registration:(id)sender
{
    [self trimmingtext];
    if(_emailIdText.text.length<1)
    {
        UIAlertView *al=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please enter your Email Id" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [al show];
        [_emailIdText becomeFirstResponder];
    }
    else if(_userName.text.length<1)
    {
        UIAlertView *al=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please enter your Username" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [al show];
        [_userName becomeFirstResponder];
    }
    else if(_passwordText.text.length<1)
    {
        UIAlertView *al=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please enter your Password" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [al show];
        [_passwordText becomeFirstResponder];
    }
    else if(_confirmpwdtext.text.length<1)
    {
        UIAlertView *al=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please enter your Confirm Password" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [al show];
        _confirmpwdtext.text=@"";
        [_confirmpwdtext becomeFirstResponder];
    }
    
    else
    {
        if ([self validateEmail:_emailIdText.text]==NO)
        {
            UIAlertView *al=[[UIAlertView alloc]initWithTitle:@"! Wrong " message:@"Please enter Correct Email Id" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [al show];
            [_emailIdText becomeFirstResponder];
            return;
        }
         else if([_passwordText.text isEqualToString:_confirmpwdtext.text])
         {
             [self.activeTextField resignFirstResponder];
             [self webserviceRegistration];
         }
        else
        {
            UIAlertView *al=[[UIAlertView alloc]initWithTitle:@"! Wrong " message:@"Please enter Correct password" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [al show];
            [_confirmpwdtext becomeFirstResponder];
            return;
        }
    }
}
-(void)webserviceRegistration
{
    
   // NSString *path =[NSString stringWithFormat:@"%@663",app->parentUrl];
     NSString *path =[NSString stringWithFormat:@"%@663",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:_emailIdText.text,@"email",_userName.text,@"username",_passwordText.text,@"user_pass",@"ios",@"devicetype",app->devicetokenstring,@"devicetoken",nil];
   AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
   // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
     [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Registering.."];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",responseStr);
        dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
        NSLog(@"%@",dict);
        //here is place for code executed in success case
        if([[dict valueForKey:@"status"]integerValue]==0)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"error"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            [SVProgressHUD dismiss];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Successfully Registered" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag=100;
            [alert show];
          //  app->fromregistration=TRUE;
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
           [SVProgressHUD dismiss];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error){
        
        //here is place for code executed in success case
         [SVProgressHUD dismiss];
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        NSLog(@"Error: %@", [error localizedDescription]);
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }];}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==100)
    {
        if(buttonIndex==0)
        {
         [self performSegueWithIdentifier:@"segueTologin" sender:nil];
        }
    }
}

 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segueTologin"])
    {
//       ViewController  *data = (ViewController *)segue.destinationViewController;
//        data->emailtext=_emailIdText.text;
//        data->passwordtext=_passwordText.text;
        app->userid=[[dict valueForKey:@"result"] valueForKey:@"userid"];
        // new code for remember
        NSUserDefaults *credentialdefault=[NSUserDefaults standardUserDefaults];
        
            [credentialdefault setValue:_emailIdText.text forKey:@"emailid"];
            [credentialdefault setValue:_passwordText.text forKey:@"password"];
            // new code for authorization
            [credentialdefault setValue:@"Applogin" forKey:@"LoginType"];
            [credentialdefault setValue:app->userid forKey:@"appid"];
            [credentialdefault setValue:_emailIdText.text forKey:@"futureuseemail"];
            [credentialdefault setValue:_passwordText.text forKey:@"futureusepassword"];
            //end
            [credentialdefault synchronize];
        
        app->fromregistration=FALSE;
        app->loginType=@"Applogin";    
    }
 }


@end
