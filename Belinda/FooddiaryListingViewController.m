//
//  FooddiaryListingViewController.m
//  Belinda
//
//  Created by Nivendru on 31/10/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "FooddiaryListingViewController.h"

@interface FooddiaryListingViewController ()
{
    AppDelegate *app;
    NSMutableArray *foodListingStoreArray;
     BOOL loadermaintain;
    
    NSIndexPath *globalindex;
}

@end

@implementation FooddiaryListingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     app=(AppDelegate *)[UIApplication sharedApplication].delegate;
   // self.aTableView.editing = YES;
    loadermaintain=FALSE;
    
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:220/255.0f green:25/255.0f blue:124/255.0f alpha:1.0f];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.title=@"Food Diary";
    
    // Hide navigation bar and customize
    self.navigationItem.hidesBackButton=YES;
    [self setCustomNavigationButton];
    
    
    
    // call foodlisting webservice
    [self webservicefoodlisting];
    
    
    
   
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:NO];
}
-(void)stopinteraction
{
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
}
-(void)startinteraction
{
     [[UIApplication sharedApplication] endIgnoringInteractionEvents];
   
}


-(void)webservicefoodlisting
{
    NSString *path =[NSString stringWithFormat:@"%@820",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    if(!loadermaintain)
    {
    [self stopinteraction];
    [SVProgressHUD showWithStatus:@"Listing Food Diary.." maskType:SVProgressHUDMaskTypeNone];
    }
    
    //[SVProgressHUD showWithStatus:@"Please wait a moment..."];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             if(!loadermaintain)
             {
             [self startinteraction];
             [SVProgressHUD dismiss];
             }
         }
         else
         {
             foodListingStoreArray=[[NSMutableArray alloc]init];
             foodListingStoreArray=[[dict valueForKey:@"result"] mutableCopy];
            [self.aTableView reloadData];
             if(!loadermaintain)
             {
                 [self startinteraction];
                 [SVProgressHUD dismiss];
             }
             // new code for push
             if(app->pushdictionary)
             {
                 // new code to check index
                 for(int i=0;i<[foodListingStoreArray count];i++)
                 {
                     if([[[app->pushdictionary valueForKey:@"aps"] valueForKey:@"id"]isEqualToString:[[foodListingStoreArray objectAtIndex:i]valueForKey:@"ID"]])
                         {
                            globalindex=[NSIndexPath indexPathForRow:i inSection:0];
                         }
                 }
                 app->pushdictionary=nil;
                 [self performSegueWithIdentifier:@"segueToAddFoodDiary" sender:globalindex];
             }
             if([foodListingStoreArray count]==0)
             {
             UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:@"No Entries Found !" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alrt show];
             }
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         if(!loadermaintain)
         {
             [self startinteraction];
             [SVProgressHUD dismiss];
         }
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}
-(void)back
{
     NSLog(@"Back");
    [self.navigationController popViewControllerAnimated:YES];
   
   
}
-(void)addfoodlibrary
{
    NSLog(@"Add");
    [self performSegueWithIdentifier:@"segueToAddFoodDiary" sender:nil];
}

-(void)setCustomNavigationButton
{
    
    UIButton *Menubutton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 28)];
    UIImageView *buttonImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 20, 18)];
    buttonImage.image=[UIImage imageNamed:@"menu_icon.png" ];
    buttonImage.clipsToBounds=YES;
    buttonImage.contentMode=UIViewContentModeScaleAspectFit;
    [Menubutton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [Menubutton addSubview:buttonImage];
    UIBarButtonItem *menubtn=[[UIBarButtonItem alloc]initWithCustomView:Menubutton];
    self.navigationItem.leftBarButtonItem=menubtn;
    
    // Adding right navigation bar button
    UIBarButtonItem *addbarbutton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addfoodlibrary)];
    addbarbutton.tintColor=[UIColor whiteColor];
    
    // Optional: if you want to add space between the refresh & profile buttons
    UIBarButtonItem *fixedSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpaceBarButtonItem.width =25;
    
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"back_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    
    UIBarButtonItem *backbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombutton];
    //[backbarbutton setAction:@selector(back)];
   // [backbarbutton setTarget:self];
    backbarbutton.style = UIBarButtonItemStylePlain;
    // end
    
    // new code
    if(app->checkcustompush)
    {
        self.navigationItem.rightBarButtonItems = @[addbarbutton];
    }
    else
    {
        self.navigationItem.rightBarButtonItems = @[addbarbutton,fixedSpaceBarButtonItem,backbarbutton];
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    // new code
    hiderevealButton=[[UIButton alloc]initWithFrame:self.view.bounds];
    [hiderevealButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    hiderevealButton.backgroundColor=[UIColor clearColor];
    [self.view addSubview:hiderevealButton];
    [hiderevealButton setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma tableviewdelegates and datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"Cell";
    Fooddiarylistingcell *cell=(Fooddiarylistingcell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    //    // new
    //    cell.delegate=(id)self;
    
    // set text colour and seperator colour
    cell.foodName.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    cell.date.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    cell.seperatorlabel.backgroundColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    
    if(cell==nil)
    {
        cell=[[Fooddiarylistingcell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    
    @try
    {
        cell.date.frame=CGRectMake(self.view.bounds.size.width-115,cell.date.frame.origin.y, cell.date.frame.size.width,cell.date.frame.size.height);
        cell.seperatorlabel.frame=CGRectMake(0, cell.seperatorlabel.frame.origin.y, self.aTableView.frame.size.width, cell.seperatorlabel.frame.size.height);
        
        cell.foodName.text=[[foodListingStoreArray objectAtIndex:indexPath.row] valueForKey:@"foodname"];
        cell.date.text=[[foodListingStoreArray objectAtIndex:indexPath.row] valueForKey:@"date"];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Description=%@",exception.description);
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

-(void)buttonTappedsubmit
{
    loadermaintain=TRUE;
    [self webservicefoodlisting];
}

//delegate addfooddiary
- (void)buttonTappeddelete:(int)indexpath
{
    [foodListingStoreArray removeObjectAtIndex:indexpath];
    [self.aTableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [foodListingStoreArray count];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"indexpathrow=%ld",(long)indexPath.row);
    [self performSegueWithIdentifier:@"segueToAddFoodDiary" sender:indexPath];

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segueToAddFoodDiary"])
    {
         AddFoodDiaryViewController *instance = (AddFoodDiaryViewController *)segue.destinationViewController;
        if([sender isKindOfClass:[NSIndexPath class]])
        {
        NSIndexPath *indexPath = (NSIndexPath *)sender;
        NSLog(@"row=%ld",(long)indexPath.row);
        NSLog(@"section=%ld",(long)indexPath.section);
        instance.delegate=self;
        instance.sendindex=(int)indexPath.row;
        instance.fooddiarypageshowingdata=[foodListingStoreArray objectAtIndex:indexPath.row];
        }
        else
        {
        instance.delegate=self;
        }
      
    }
}


@end
