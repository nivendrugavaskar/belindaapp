//
//  HowtoMakeViewController.h
//  Belinda
//
//  Created by Nivendru on 24/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface HowtoMakeViewController : UIViewController<UIWebViewDelegate>
{
    
}
@property(strong,nonatomic) NSString *descriptiontext,*recipename;
@property (strong, nonatomic) IBOutlet UILabel *Headingtext;
@property (strong, nonatomic) IBOutlet UIWebView *preparewebview;

@end
