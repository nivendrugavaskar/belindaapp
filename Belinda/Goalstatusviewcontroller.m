//
//  Goalstatusviewcontroller.m
//  Belinda
//
//  Created by Nivendru on 05/12/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "Goalstatusviewcontroller.h"
#import "PreviewImageViewController.h"

@interface Goalstatusviewcontroller ()
{
    NSString *sendacheivestatus;
    
     UITapGestureRecognizer *tap;
}

@end

@implementation Goalstatusviewcontroller

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:220/255.0f green:25/255.0f blue:124/255.0f alpha:1.0f];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.title=@"Goals";
    _commentTextbox.layer.borderWidth=2.0;
    _commentTextbox.layer.borderColor=[[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0] CGColor];
#pragma testing with local data without server
    // NSLog(@"Global array count=%lu",(unsigned long)[app.testingpurposearray count]);
    // goalarray=app.testingpurposearray;
    // statusarray=[[NSMutableArray alloc]initWithObjects:@"Completed",@"Incompleted",@"In-Progress", nil];
    
    // Hide navigation bar and customize
    self.navigationItem.hidesBackButton=YES;
    [self setCustomNavigationButton];
    if([_commentstatus isEqualToString:@"Acheived"])
    {
        _comment.text=@"How are you feeling after acheiving Goal:";
        sendacheivestatus=@"1";
    }
    else
    {
        _comment.text=@"Why you didn't acheived your Goal:";
        sendacheivestatus=@"2";
    }
    
    // new code
    if(_fetchdataDict)
    {
        _submitbtn.hidden=TRUE;
        _commentTextbox.editable=NO;
        _commentTextbox.text=[[_fetchdataDict objectAtIndex:0] valueForKey:@"comment"];
        // for image
        NSString *imagepath=[[_fetchdataDict objectAtIndex:0] valueForKey:@"commentimage"];
        //if([imagepath isEqualToString:@""])
        __weak UIButton *cameraweakbtn =_camerabtn;
        cameraweakbtn.userInteractionEnabled=NO;
        
        __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityIndicator.color=[UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
        activityIndicator.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
        activityIndicator.hidesWhenStopped = YES;
        
        __block BOOL successfetchimage=FALSE;
        __weak UIImageView *setimage = _uploadimage;
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imagepath]];
        [_uploadimage setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"no_image.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             setimage.contentMode=UIViewContentModeScaleAspectFit;
             setimage.image=image;
             successfetchimage=TRUE;
             [activityIndicator removeFromSuperview];
             cameraweakbtn.userInteractionEnabled=YES;
         }
        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             [activityIndicator removeFromSuperview];
         }];
        
        if(!successfetchimage)
        {
            [_uploadimage addSubview:activityIndicator];
            [activityIndicator startAnimating];
        }
    }
    
//    UIToolbar *feelingtoolbar;
//    feelingtoolbar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
//    feelingtoolbar.tag = 1000;
//    UIBarButtonItem *space2=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//    UIBarButtonItem *done2=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismisstoolbarkeyboard)];
//    [feelingtoolbar setItems:[NSArray arrayWithObjects:space2,done2,nil]];
//    [_commentTextbox setInputAccessoryView:feelingtoolbar];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
     pointtrackcontentoffset=self.view.frame.origin;
    storerect=self.view.frame;
    screenBounds = [[UIScreen mainScreen] bounds];
    // new code
    hiderevealButton=[[UIButton alloc]initWithFrame:self.view.bounds];
    [hiderevealButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    hiderevealButton.backgroundColor=[UIColor clearColor];
    [self.view addSubview:hiderevealButton];
    [hiderevealButton setHidden:YES];
    
}

-(void)setCustomNavigationButton
{
    
    UIButton *Menubutton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 28)];
    UIImageView *buttonImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 20, 18)];
    buttonImage.image=[UIImage imageNamed:@"menu_icon.png" ];
    buttonImage.clipsToBounds=YES;
    buttonImage.contentMode=UIViewContentModeScaleAspectFit;
    [Menubutton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [Menubutton addSubview:buttonImage];
    UIBarButtonItem *menubtn=[[UIBarButtonItem alloc]initWithCustomView:Menubutton];
    self.navigationItem.leftBarButtonItem=menubtn;
    
    
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"back_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    
    UIBarButtonItem *backbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombutton];
    //[backbarbutton setAction:@selector(back)];
    // [backbarbutton setTarget:self];
    backbarbutton.style = UIBarButtonItemStylePlain;
    // end
    self.navigationItem.rightBarButtonItems = @[backbarbutton];
    
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma swrevealview delegate
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
   // [self dismisskeyboard];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"seguetoshowshare"])
    {
        PreviewImageViewController *instance = (PreviewImageViewController *)segue.destinationViewController;
            instance.previewimagestring=[[_fetchdataDict objectAtIndex:0] valueForKey:@"commentimage"];
            instance.imagetype=@"feedbackphoto";
    }
}


- (IBAction)didTapcamera:(id)sender
{
    if(_fetchdataDict)
    {
       [self performSegueWithIdentifier:@"seguetoshowshare" sender:self]; 
    }
    else
    {
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Upload Image \nFrom:" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Gallery", nil];
    alert.tag=100;
    [alert show];
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==100)
    {
        if(buttonIndex==0)
        {
           // _uploadimage.image=[UIImage imageNamed:@"no_image.png"];
        }
        if(buttonIndex==1)
        {
            //[self performSelector:@selector(takepicture) withObject:nil afterDelay:0.3];
            [self takepicture];
        }
        else if(buttonIndex==2)
        {
            [self selectgallery];
        }
    }
    else if(alertView.tag==101)
    {
        [self back];
    }
    
    
}
-(void)selectgallery
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    ImagePicker=[[UIImagePickerController alloc]init];
    ImagePicker.delegate=(id)self;
    ImagePicker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    // imagepic.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:ImagePicker animated:YES completion:nil];
}
-(void)takepicture
{
    /******************** code for camera ***********************/
    
    ImagePicker=[[UIImagePickerController alloc]init];
    if    (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
    }
    else
    {
        
        ImagePicker.delegate=(id)self;
        ImagePicker.modalPresentationStyle=UIModalPresentationFullScreen;
        // ImagePicker.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        // [ImagePicker setAllowsEditing:YES];
        ImagePicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        // imagepic.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:ImagePicker animated:YES completion:NULL];
        NSLog(@"camera");
        
    }
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    // new code
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [picker dismissViewControllerAnimated:YES completion:nil];
    // [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    selectedimage=[info objectForKey:UIImagePickerControllerOriginalImage];
    selectedimage=[self scaleAndRotateImage:selectedimage];
    _uploadimage.contentMode=UIViewContentModeScaleAspectFit;
    _uploadimage.image=selectedimage;
    [picker dismissViewControllerAnimated:YES completion:nil];
}
// new method
- (UIImage *)scaleAndRotateImage:(UIImage *)image
{
    int kMaxResolution = 640; // Or whatever
    CGImageRef imgRef = image.CGImage;
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

- (IBAction)didTapSubmit:(id)sender
{
    
    [self webservicesubmitstatus];
    
//    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Submitted" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    alert.tag=101;
//    [alert show];
}
-(void)webservicesubmitstatus
{
    
        NSString *path =[NSString stringWithFormat:@"%@1458",app->parentUrl];
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
         [SVProgressHUD showWithStatus:@"Checking Submission ..."];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        imageDataPicker=(NSData *)UIImagePNGRepresentation(selectedimage);
        NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",_fetchgoalid,@"goalid",sendacheivestatus,@"status",_commentTextbox.text,@"comment",nil];
        AFHTTPRequestOperation *op = [manager POST:path parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            //do not put image inside parameters dictionary as I did, but append it!
            if((NSData *)UIImagePNGRepresentation(selectedimage))
            {
                [formData appendPartWithFileData:(NSData *)UIImagePNGRepresentation(selectedimage) name:@"image" fileName:@"optionalphoto.png" mimeType:@"image/png"];
                
            }
        }
        success:^(AFHTTPRequestOperation *operation, id responseObject)
        {
            NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
            NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
            NSLog(@"%@",responseStr);
            NSDictionary *dict1 = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
            NSLog(@"%@",dict1);
            //here is place for code executed in success case
            if([[dict1 valueForKey:@"status"]integerValue]==0)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict1 valueForKey:@"error"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                [self.goalstatusdelegate sendflagtrueingoalstatussuccess:FALSE];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict1 valueForKey:@"result"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                alert.tag=101;
            }
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            [SVProgressHUD dismiss];
            
            }
        failure:^(AFHTTPRequestOperation *operation, NSError *error)
            {
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                [SVProgressHUD dismiss];
                NSLog(@"Error: %@", [error localizedDescription]);
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            ];
                                     
        [op start];
}

-(void)dismisskeyboard
{
    NSLog(@"Gesturecall");
    [self.view removeGestureRecognizer:tap];
//    CGRect rect=self.view.frame;
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.3];
//    rect.origin.y=pointtrackcontentoffset.y;
//    self.view.frame=rect;
//    // new code
//    storerect=rect;
//    [UIView commitAnimations];
//    //[[self.view viewWithTag:1000] removeFromSuperview];
    [self.activeTextView resignFirstResponder];
}


#pragma textviewdelegate
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    // set up gesture
     self.activeTextView=textView;
     pointtrack =textView.frame.origin;
    [self openkeyboard];
    tap = [[UITapGestureRecognizer alloc]
           initWithTarget:self
           action:@selector(dismisskeyboard)];
    tap.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:tap];
    
//    CGRect rect=storerect;
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.3];
//    if(screenBounds.size.height==480)
//    {
//        rect.origin.y-=pointtrack.y-80;
//    }
//    else if(screenBounds.size.height==568)
//    {
//        rect.origin.y-=pointtrack.y-140;
//    }
//    else if(screenBounds.size.height==667)
//    {
//        rect.origin.y-=pointtrack.y-200;
//    }
//    else if(screenBounds.size.height==736)
//    {
//        if(pointtrack.y>580)
//        {
//            pointtrack.y=530;
//        }
//        rect.origin.y-=pointtrack.y-270;
//    }
//    else
//    {
//        
//    }
//    self.view.frame=rect;
//    [UIView commitAnimations];
   
}

-(void)openkeyboard
{
    UIToolbar *feelingtoolbar;
    feelingtoolbar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    feelingtoolbar.tag = 1000;
    feelingtoolbar.backgroundColor=[UIColor colorWithRed:189/255.0f green:188/255.0f blue:188/255.0f alpha:1.0];
    UIBarButtonItem *space2=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *done2=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismisstoolbarkeyboard)];
    [feelingtoolbar setItems:[NSArray arrayWithObjects:space2,done2,nil]];
    [_commentTextbox setInputAccessoryView:feelingtoolbar];
    [_commentTextbox reloadInputViews];
}

-(void)dismisstoolbarkeyboard
{
    [[self.view viewWithTag:1000] removeFromSuperview];
    [self dismisskeyboard];
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    NSLog(@"Endeditingcall");
    [_commentTextbox resignFirstResponder];
    return YES;
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
//    if([text isEqualToString:@"\n"])
//    {
//        [UIView beginAnimations:nil context:nil];
//        [UIView setAnimationDuration:0.3];
//        storerect.origin.y-=pointtrack.y-80;
//        self.view.frame=storerect;
//        [UIView commitAnimations];
//    }
    return YES;
}

@end
