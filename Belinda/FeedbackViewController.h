//
//  FeedbackViewController.h
//  Belinda
//
//  Created by Nivendru on 26/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface FeedbackViewController : UIViewController<UITextFieldDelegate,SWRevealViewControllerDelegate>
{
    AppDelegate *app;
}
@property (strong, nonatomic) IBOutlet UITextField *feedbacktextfield;
@property (strong, nonatomic) IBOutlet UITextField *activetextfield;
- (IBAction)didTapRate:(id)sender;
- (IBAction)didTapsubmit:(id)sender;

@end
