//
//  RecipesViewController.m
//  Belinda
//
//  Created by Nivendru on 10/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "RecipesViewController.h"

@interface RecipesViewController ()
{
    NSMutableArray *headermenu;
    NSMutableArray *recipedetailarray,*fulldetailarray;
}

@end

@implementation RecipesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    // headermenu=[[NSArray alloc]initWithObjects:@"BreakFast",@"Lunch",@"Biryani",@"Testing", nil];
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:220/255.0f green:25/255.0f blue:124/255.0f alpha:1.0f];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.title=@"Recipes";
    
    // Hide navigation bar and customize
    self.navigationItem.hidesBackButton=YES;
    [self setCustomNavigationButton];
    
    // call foodlisting webservice
    [self webserviceadminreciepelisting];
    
}

-(void)setCustomNavigationButton
{
    
    UIButton *Menubutton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 28)];
    UIImageView *buttonImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 20, 18)];
    buttonImage.image=[UIImage imageNamed:@"menu_icon.png" ];
    buttonImage.clipsToBounds=YES;
    buttonImage.contentMode=UIViewContentModeScaleAspectFit;
    [Menubutton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [Menubutton addSubview:buttonImage];
    UIBarButtonItem *menubtn=[[UIBarButtonItem alloc]initWithCustomView:Menubutton];
    self.navigationItem.leftBarButtonItem=menubtn;
    
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"back_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    
    UIBarButtonItem *backbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombutton];
    //[backbarbutton setAction:@selector(back)];
    // [backbarbutton setTarget:self];
    backbarbutton.style = UIBarButtonItemStylePlain;
    // end
    
    // new code
    if(!app->checkcustompush)
    {
        self.navigationItem.rightBarButtonItems = @[backbarbutton];
    }
   
    
}
-(void)back
{
    NSLog(@"Back");
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    // new code
    hiderevealButton=[[UIButton alloc]initWithFrame:self.view.bounds];
    [hiderevealButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    hiderevealButton.backgroundColor=[UIColor clearColor];
    [self.view addSubview:hiderevealButton];
    [hiderevealButton setHidden:YES];
}
-(void)webserviceadminreciepelisting
{
   // NSString *path =[NSString stringWithFormat:@"%@835",app->parentUrl];
     NSString *path =[NSString stringWithFormat:@"%@1119",app->parentUrl];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
     [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Listing Recipes.."];
    [manager POST:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
         }
         else
         {
             fulldetailarray=[[NSMutableArray alloc]init];
             headermenu=[[NSMutableArray alloc]init];
             recipedetailarray=[[NSMutableArray alloc]init];
             NSArray *localfulldetail=[dict valueForKey:@"result"];
             NSArray *storeresponseheader=[localfulldetail valueForKey:@"recipe_type"];
             NSArray *storeresponsechildofheader=[localfulldetail valueForKey:@"recipe_details"];
             for(int i=0;i<[storeresponsechildofheader count];i++)
             {
                 if([[storeresponsechildofheader objectAtIndex:i]count]>0)
                 {
                     [headermenu addObject:[storeresponseheader objectAtIndex:i]];
                     [recipedetailarray addObject:[storeresponsechildofheader objectAtIndex:i]];
                     [fulldetailarray addObject:[localfulldetail objectAtIndex:i]];
                     
                 }
             }
             
             [self.aTableview reloadData];
         }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark TableView Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"Cell";
    reciepeViewCell *cell=(reciepeViewCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    // new
    cell.delegate=(id)self;

    
    if(cell==nil)
    {
        cell=[[reciepeViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    NSString *animal = [[[recipedetailarray objectAtIndex:indexPath.section]objectAtIndex:indexPath.row] valueForKey:@"recipe_name"];
    cell.textlabel.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    cell.textlabel.text = animal;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{

    return 30.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"section=%ld",(long)indexPath.section);
    NSLog(@"indexpathrow=%ld",(long)indexPath.row);
    [self performSegueWithIdentifier:@"seguetoFooddiarydesc" sender:indexPath];
   
}

 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"seguetoFooddiarydesc"])
    {
        RecipedetailViewController *instance = (RecipedetailViewController *)segue.destinationViewController;
        NSIndexPath *indexPath = (NSIndexPath *)sender;
        NSLog(@"row=%ld",(long)indexPath.row);
        NSLog(@"section=%ld",(long)indexPath.section);
        instance.recipedescarray=[[recipedetailarray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        instance.fulldetailarraydesc=[fulldetailarray objectAtIndex:indexPath.section];
        
    }
    
 }
 
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [headermenu count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return [[recipedetailarray objectAtIndex:section]count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    NSString *sectionName;
//    sectionName=[[NSString alloc]init];
//    sectionName=[headermenu objectAtIndex:section];
//    return sectionName;
//}
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)];
    [headerView setBackgroundColor:[UIColor colorWithRed:2/255.0f green:3/255.0f blue:4/255.0f alpha:0.8f]];
    
    UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(20,5,tableView.bounds.size.width,30)];
    tempLabel.backgroundColor=[UIColor clearColor];
    tempLabel.textColor = [UIColor whiteColor]; //here you can change the text color of header.
   // tempLabel.font = [UIFont fontWithName:@"Helvetica" size:fontSizeForHeaders];
    tempLabel.font = [UIFont boldSystemFontOfSize:20.0f];
    tempLabel.text=[headermenu objectAtIndex:section];
    [headerView addSubview:tempLabel];
    
    return headerView;
}
//- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
//{
//    return [recipedetailarray indexOfObject:title];
//}
//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
//{
//    //  return animalSectionTitles;
//    return headermenu;
//}



@end
