//
//  RegistrationViewController.h
//  Belinda
//
//  Created by Nivendru on 28/10/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ViewController.h"

@interface RegistrationViewController : UIViewController<UITextFieldDelegate>
{
    CGPoint pointtrack,pointtrackcontentoffset;
    CGSize kbsize;
}

- (IBAction)cancelReg:(id)sender;
- (IBAction)registration:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *emailIdText;
@property (strong, nonatomic) IBOutlet UITextField *userName;
@property (strong, nonatomic) IBOutlet UITextField *passwordText;
@property (strong, nonatomic) IBOutlet UITextField *confirmpwdtext;

@property (strong, nonatomic) IBOutlet UIScrollView *aScrollView;
@property (strong, nonatomic) IBOutlet UIView *innerViewScrollView;
@property(strong,nonatomic) UITextField *activeTextField;

@end
