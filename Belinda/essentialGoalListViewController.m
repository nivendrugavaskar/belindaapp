//
//  essentialGoalListViewController.m
//  Belinda
//
//  Created by Nivendru on 05/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "essentialGoalListViewController.h"

@interface essentialGoalListViewController ()
{
    AppDelegate *app;
     CGRect screenBounds;
    UITapGestureRecognizer *tap;
    NSMutableArray *questionarraystore,*questionidarray;
    NSDate *selectedtime;
    NSMutableArray *hoursarray,*minutearray,*secondarray;
    CGSize kbsize;
}

@end

@implementation essentialGoalListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    // new code

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    //end
    
    screenBounds = [[UIScreen mainScreen] bounds];
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:220/255.0f green:25/255.0f blue:124/255.0f alpha:1.0f];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.title=@"Assessment";
    // Hide navigation bar and customize
    self.navigationItem.hidesBackButton=YES;
    [self setcustomproperties];
    [self setCustomNavigationButton];
    if(_assementarraydata)
    {
      NSLog(@"%@",_assementarraydata);
        [self closeinteraction];
        [self setquestionsanddata];
      // Close interaction of editing field
        [self.aScrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.btnSubmit.frame.origin.y+self.btnSubmit.frame.size.height+35)];
        _btnSubmit.hidden=YES;
    }
    else
    {
    // question webservice
    [self questionwebservice];
    }
}
-(void)setquestionsanddata
{
   
     _armtextfield.text=[_assementarraydata valueForKey:@"arm"];
     _chesttextfield.text=[_assementarraydata valueForKey:@"chest"];
     _waisttextfield.text=[_assementarraydata valueForKey:@"weist"];
     _hiptextfield.text=[_assementarraydata valueForKey:@"hip"];
     _thightextfield.text=[_assementarraydata valueForKey:@"thigh"];
     _weighttextfield.text=[_assementarraydata valueForKey:@"weight"];
    // below data
    _timetextfield.text=[_assementarraydata valueForKey:@"time"];
    // NSArray *allquestionarray=[[dict valueForKey:@"result"]valueForKey:@"allquestion"];
    //    questionarraystore=[[NSMutableArray alloc]init];
    //    //  questionarraystore=[allquestionarray objectAtIndex:0];
    //
    //    [questionarraystore addObject:@"What is your 1st Answer?"];
    //    [questionarraystore addObject:@"What is your 2nd Answer?"];
    //    [questionarraystore addObject:@"What is your 3rd Answer?"];
#pragma set questions and according uidesign in case future change number of question
    for (int i=0; i<3; i++)
    {
        if(i==0)
        {
            _firstquestionlabel.hidden=FALSE;
            _firstanswertextfield.hidden=FALSE;
            _firstquesimageview.hidden=FALSE;
            //_firstquestionlabel.text=[[questionarraystore objectAtIndex:i] valueForKey:@"question"];
            _firstquestionlabel.text=[_assementarraydata valueForKey:@"first_question"];
            _firstanswertextfield.text=[_assementarraydata valueForKey:@"first_question_answer"];
            
            // new code
            self.questionsScrollView.frame=CGRectMake(self.questionsScrollView.frame.origin.x, self.questionsScrollView.frame.origin.y, self.questionsScrollView.frame.size.width, _firstanswertextfield.frame.origin.y+_firstanswertextfield.frame.size.height+10);
            // submit button content set
            self.btnSubmit.frame=CGRectMake(self.btnSubmit.frame.origin.x, self.questionsScrollView.frame.origin.y+self.questionsScrollView.frame.size.height+10, self.btnSubmit.frame.size.width, self.btnSubmit.frame.size.height);
            // mainscrollviewscontent
            [self.aScrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.btnSubmit.frame.origin.y+self.btnSubmit.frame.size.height+75)];
            pointtrackcontentoffset=self.aScrollView.contentOffset;
            
            //new code
            _firstanswertextfield.tag=i+1;
        }
        if(i==1)
        {
            _secondquestionlabel.hidden=FALSE;
            _secondanswertextfield.hidden=FALSE;
            _secondquestionimageview.hidden=FALSE;
            _secondquestionlabel.text=[_assementarraydata valueForKey:@"second_question"];
            _secondanswertextfield.text=[_assementarraydata valueForKey:@"second_question_answer"];
            // new code
            self.questionsScrollView.frame=CGRectMake(self.questionsScrollView.frame.origin.x, self.questionsScrollView.frame.origin.y, self.questionsScrollView.frame.size.width, _secondanswertextfield.frame.origin.y+_secondanswertextfield.frame.size.height+10);
            // submit button content set
            self.btnSubmit.frame=CGRectMake(self.btnSubmit.frame.origin.x, self.questionsScrollView.frame.origin.y+self.questionsScrollView.frame.size.height+10, self.btnSubmit.frame.size.width, self.btnSubmit.frame.size.height);
            // mainscrollviewscontent
            [self.aScrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.btnSubmit.frame.origin.y+self.btnSubmit.frame.size.height+75)];
            pointtrackcontentoffset=self.aScrollView.contentOffset;
            
            // new code
            _secondanswertextfield.tag=i+1;
        }
        if(i==2)
        {
            _thirdquestionlabel.hidden=FALSE;
            _thirdanswertextfield.hidden=FALSE;
            _thirdquestionimageview.hidden=FALSE;
            _thirdquestionlabel.text=[_assementarraydata valueForKey:@"third_question"];
            _thirdanswertextfield.text=[_assementarraydata valueForKey:@"third_question_answer"];
            
            // new code
            self.questionsScrollView.frame=CGRectMake(self.questionsScrollView.frame.origin.x, self.questionsScrollView.frame.origin.y, self.questionsScrollView.frame.size.width, _thirdanswertextfield.frame.origin.y+_thirdanswertextfield.frame.size.height+10);
            // submit button content set
            self.btnSubmit.frame=CGRectMake(self.btnSubmit.frame.origin.x, self.questionsScrollView.frame.origin.y+self.questionsScrollView.frame.size.height+10, self.btnSubmit.frame.size.width, self.btnSubmit.frame.size.height);
            // mainscrollviewscontent
            [self.aScrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.btnSubmit.frame.origin.y+self.btnSubmit.frame.size.height+75)];
            
            pointtrackcontentoffset=self.aScrollView.contentOffset;
            // new code
            _thirdanswertextfield.tag=i+1;
            break;
        }
    }
    //new code for image load
    __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    __block UIActivityIndicatorView *activityIndicator1 = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    __block UIActivityIndicatorView *activityIndicator2 = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.color=[UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
    activityIndicator1.color=[UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
    activityIndicator2.color=[UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
    activityIndicator.center = CGPointMake(CGRectGetMidX(_frontimageupload.bounds), CGRectGetMidY(_frontimageupload.bounds));
    activityIndicator1.center = CGPointMake(CGRectGetMidX(_leftsideimageupload.bounds), CGRectGetMidY(_leftsideimageupload.bounds));
    activityIndicator2.center = CGPointMake(CGRectGetMidX(_backimageupload.bounds), CGRectGetMidY(_backimageupload.bounds));
    activityIndicator.hidesWhenStopped = YES;
    activityIndicator1.hidesWhenStopped = YES;
    activityIndicator2.hidesWhenStopped = YES;
    
    NSString *myImagePath=[_assementarraydata valueForKey:@"frontimage"];
    NSString *myImagePath1=[_assementarraydata valueForKey:@"leftimage"];
    NSString *myImagePath2=[_assementarraydata valueForKey:@"backimage"];
    
    __block BOOL successfetchimage=FALSE;
    __block BOOL successfetchimage1=FALSE;
    __block BOOL successfetchimage2=FALSE;
    
    __weak UIImageView *setimage = _frontimageupload;
    __weak UIImageView *setimage1 = _leftsideimageupload;
    __weak UIImageView *setimage2 = _backimageupload;
    
    __weak UIButton *frontbtn=_frontbutton;
    __weak UIButton *leftbtn=_leftbutton;
    __weak UIButton *backbtn=_backbutton;
   
    

#pragma for 1st image
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:myImagePath]];
    [_frontimageupload setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"no_image.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
         setimage.contentMode=UIViewContentModeScaleAspectFit;
         setimage.image=image;
         successfetchimage=TRUE;
         [activityIndicator removeFromSuperview];
         frontbtn.userInteractionEnabled=YES;
         
     }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
     {
         [activityIndicator removeFromSuperview];
     }];
    if(!successfetchimage)
    {
        if(![myImagePath isEqualToString:@""])
        {
            [_frontimageupload addSubview:activityIndicator];
            [activityIndicator startAnimating];
        }
    }
    // end
    
#pragma for 2nd image
    NSURLRequest *request1 = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:myImagePath1]];
    [_leftsideimageupload setImageWithURLRequest:request1 placeholderImage:[UIImage imageNamed:@"no_image.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
         setimage1.contentMode=UIViewContentModeScaleAspectFit;
         setimage1.image=image;
         successfetchimage1=TRUE;
         [activityIndicator1 removeFromSuperview];
         leftbtn.userInteractionEnabled=YES;
         
         
     }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
     {
         [activityIndicator1 removeFromSuperview];
     }];
    if(!successfetchimage1)
    {
        if(![myImagePath1 isEqualToString:@""])
        {
            [_leftsideimageupload addSubview:activityIndicator1];
            [activityIndicator1 startAnimating];
        }
    }
    // end
#pragma for 3rd image
    NSURLRequest *request2 = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:myImagePath2]];
    [_backimageupload setImageWithURLRequest:request2 placeholderImage:[UIImage imageNamed:@"no_image.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
         setimage2.contentMode=UIViewContentModeScaleAspectFit;
         setimage2.image=image;
         successfetchimage2=TRUE;
         [activityIndicator2 removeFromSuperview];
         backbtn.userInteractionEnabled=YES;
         
     }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
     {
         [activityIndicator2 removeFromSuperview];
     }];
    if(!successfetchimage2)
    {
        if(![myImagePath2 isEqualToString:@""])
        {
            [_backimageupload addSubview:activityIndicator2];
            [activityIndicator2 startAnimating];
        }
    }
    // end
}
-(void)closeinteraction
{
   // _imageContainerView.userInteractionEnabled=NO;
    for (id v in self.questionsScrollView.subviews)
    {
        UIView *superv=(UIView *)v;
        superv.userInteractionEnabled=NO;
    }
    // new code
    _frontbutton.userInteractionEnabled=NO;
    _leftbutton.userInteractionEnabled=NO;
    _backbutton.userInteractionEnabled=NO;
    

}
// new code to maintain scroll
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    kbsize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [self.aScrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.btnSubmit.frame.origin.y+self.btnSubmit.frame.size.height+75+kbsize.height+10)];
   
   
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    // new code
    [self.aScrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.btnSubmit.frame.origin.y+self.btnSubmit.frame.size.height+75)];
    [self.aScrollView setContentOffset:pointtrackcontentoffset animated:NO];
    [self.view removeGestureRecognizer:tap];
    
}
// end
-(void)questionwebservice
{
    
    NSString *path =[NSString stringWithFormat:@"%@933",app->parentUrl];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
     [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Loading"];
    [manager POST:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
         }
         else
         {
            // NSArray *allquestionarray=[[dict valueForKey:@"result"]valueForKey:@"allquestion"];
             questionarraystore=[[NSMutableArray alloc]init];
             questionidarray=[[NSMutableArray alloc]init];
             questionarraystore=[[dict valueForKey:@"result"] valueForKey:@"question"];
             questionidarray=[[dict valueForKey:@"result"] valueForKey:@"id"];
             
             //[questionarraystore addObject:@"What is your 1st Answer?"];
             //[questionarraystore addObject:@"What is your 2nd Answer?"];
            // [questionarraystore addObject:@"What is your 3rd Answer?"];

             
             for (int i=0; i<[questionarraystore count]; i++)
             {
                 if(i==0)
                 {
                     _firstquestionlabel.hidden=FALSE;
                     _firstanswertextfield.hidden=FALSE;
                     _firstquesimageview.hidden=FALSE;
                     //_firstquestionlabel.text=[[questionarraystore objectAtIndex:i] valueForKey:@"question"];
                     _firstquestionlabel.text=[questionarraystore objectAtIndex:i];
                     
                     
                     // new code
                     self.questionsScrollView.frame=CGRectMake(self.questionsScrollView.frame.origin.x, self.questionsScrollView.frame.origin.y, self.questionsScrollView.frame.size.width, _firstanswertextfield.frame.origin.y+_firstanswertextfield.frame.size.height+10);
                     // submit button content set
                     self.btnSubmit.frame=CGRectMake(self.btnSubmit.frame.origin.x, self.questionsScrollView.frame.origin.y+self.questionsScrollView.frame.size.height+10, self.btnSubmit.frame.size.width, self.btnSubmit.frame.size.height);
                     // mainscrollviewscontent
                     [self.aScrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.btnSubmit.frame.origin.y+self.btnSubmit.frame.size.height+75)];
                     pointtrackcontentoffset=self.aScrollView.contentOffset;
                     
                     //new code
                     _firstanswertextfield.tag=i+1;
                 }
                 if(i==1)
                 {
                     _secondquestionlabel.hidden=FALSE;
                     _secondanswertextfield.hidden=FALSE;
                     _secondquestionimageview.hidden=FALSE;
                     _secondquestionlabel.text=[questionarraystore objectAtIndex:i];
                     
                     // new code
                     self.questionsScrollView.frame=CGRectMake(self.questionsScrollView.frame.origin.x, self.questionsScrollView.frame.origin.y, self.questionsScrollView.frame.size.width, _secondanswertextfield.frame.origin.y+_secondanswertextfield.frame.size.height+10);
                     // submit button content set
                     self.btnSubmit.frame=CGRectMake(self.btnSubmit.frame.origin.x, self.questionsScrollView.frame.origin.y+self.questionsScrollView.frame.size.height+10, self.btnSubmit.frame.size.width, self.btnSubmit.frame.size.height);
                     // mainscrollviewscontent
                     [self.aScrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.btnSubmit.frame.origin.y+self.btnSubmit.frame.size.height+75)];
                     pointtrackcontentoffset=self.aScrollView.contentOffset;
                     
                     // new code
                     _secondanswertextfield.tag=i+1;
                 }
                 if(i==2)
                 {
                     _thirdquestionlabel.hidden=FALSE;
                     _thirdanswertextfield.hidden=FALSE;
                     _thirdquestionimageview.hidden=FALSE;
                     _thirdquestionlabel.text=[questionarraystore objectAtIndex:i];
                     
                     
                     // new code
                     self.questionsScrollView.frame=CGRectMake(self.questionsScrollView.frame.origin.x, self.questionsScrollView.frame.origin.y, self.questionsScrollView.frame.size.width, _thirdanswertextfield.frame.origin.y+_thirdanswertextfield.frame.size.height+10);
                     // submit button content set
                     self.btnSubmit.frame=CGRectMake(self.btnSubmit.frame.origin.x, self.questionsScrollView.frame.origin.y+self.questionsScrollView.frame.size.height+10, self.btnSubmit.frame.size.width, self.btnSubmit.frame.size.height);
                     // mainscrollviewscontent
                     [self.aScrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.btnSubmit.frame.origin.y+self.btnSubmit.frame.size.height+75)];
                     
                     pointtrackcontentoffset=self.aScrollView.contentOffset;
                     
                     // new code
                     _thirdanswertextfield.tag=i+1;
                     
                      break;
                 }
             }
         }
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}


-(void)setcustomproperties
{
    UITextField *lasttextfieldframe=[self.questionsScrollView.subviews lastObject];
   // [self.questionsScrollView setContentSize:CGSizeMake(self.questionsScrollView.frame.size.width, lasttextfieldframe.frame.origin.y+lasttextfieldframe.frame.size.height+50)];
    self.questionsScrollView.frame=CGRectMake(self.questionsScrollView.frame.origin.x, self.questionsScrollView.frame.origin.y, self.questionsScrollView.frame.size.width, lasttextfieldframe.frame.origin.y+lasttextfieldframe.frame.size.height+10);
    // submit button content set
    self.btnSubmit.frame=CGRectMake(self.btnSubmit.frame.origin.x, self.questionsScrollView.frame.origin.y+self.questionsScrollView.frame.size.height+10, self.btnSubmit.frame.size.width, self.btnSubmit.frame.size.height);
    // mainscrollviewscontent
    [self.aScrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.btnSubmit.frame.origin.y+self.btnSubmit.frame.size.height+75)];
    
     pointtrackcontentoffset=self.aScrollView.contentOffset;
    
    for (id v in self.imageContainerView.subviews)
    {
        
        if([v isKindOfClass:[UILabel class]])
        {
            UILabel *label=(UILabel *)v;
            label.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
        }
        else if([v isKindOfClass:[UIImageView class]])
        {
        }
        else if([v isKindOfClass:[UIView class]])
        {
            UIView *view=(UIView *)v;
            view.layer.borderColor=[[UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f] CGColor];
            view.layer.borderWidth=1.0;
        }
        
    }
   
    for (id v2 in self.questionsScrollView.subviews)
    {
        if([v2 isKindOfClass:[UILabel class]])
        {
            UILabel *label=(UILabel *)v2;
            label.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
        }
        else if([v2 isKindOfClass:[UITextField class]])
        {
            UITextField *view=(UITextField *)v2;
            view.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
            [view setValue:[UIColor lightTextColor] forKeyPath:@"_placeholderLabel.textColor"];
            [view setTintColor:[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f]];
        }
    }
    
    // set custom properties
    _startedGoallabel.textColor=[UIColor whiteColor];
     _measurementlabel.textColor=[UIColor whiteColor];
    _fitneslabeltest.textColor=[UIColor whiteColor];
    
    // hidden question properties
    _firstquestionlabel.hidden=YES;
    _firstanswertextfield.hidden=YES;
    _secondquestionlabel.hidden=YES;
    _secondanswertextfield.hidden=YES;
    _thirdquestionlabel.hidden=YES;
    _thirdanswertextfield.hidden=YES;
    
    _firstquesimageview.hidden=YES;
    _secondquestionimageview.hidden=YES;
    _thirdquestionimageview.hidden=YES;
    
    // new thing
    _timetextfield.text=@"00:00";
    
}

-(void)setCustomNavigationButton
{
    
    UIButton *Menubutton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 28)];
    UIImageView *buttonImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 20, 18)];
    buttonImage.image=[UIImage imageNamed:@"menu_icon.png" ];
    buttonImage.clipsToBounds=YES;
    buttonImage.contentMode=UIViewContentModeScaleAspectFit;
    [Menubutton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    // set swrevealcontroller delegate
    self.revealViewController.delegate =self;
    
    [Menubutton addSubview:buttonImage];
    UIBarButtonItem *menubtn=[[UIBarButtonItem alloc]initWithCustomView:Menubutton];
    self.navigationItem.leftBarButtonItem=menubtn;
    
    
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"back_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    
    UIBarButtonItem *backbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombutton];
    //[backbarbutton setAction:@selector(back)];
    //[backbarbutton setTarget:self];
    backbarbutton.style = UIBarButtonItemStylePlain;
    // new code
    if(app->checkcustompush)
    {
        
    }
    else
    {
        self.navigationItem.rightBarButtonItems = @[backbarbutton];
    }

    
}
#pragma swrevealview delegate
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    [self dismisskeyboard];
}
-(void)back
{
    NSLog(@"Back");
    [self.activeTextField resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

-(void)dismisskeyboard
{
     [self.aScrollView setContentOffset:CGPointMake(0, pointtrackcontentoffset.y-5) animated:YES];
     [self.activeTextField resignFirstResponder];
    
    [self.view removeGestureRecognizer:tap];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    // new code
    hiderevealButton=[[UIButton alloc]initWithFrame:self.view.bounds];
    [hiderevealButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    hiderevealButton.backgroundColor=[UIColor clearColor];
    [self.view addSubview:hiderevealButton];
    [hiderevealButton setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// new code
- (void)changeDate1:(UIBarButtonItem *)sender
{
    //  self.activebutton=(UIButton *)sender;
    
    NSDateFormatter *format=[[NSDateFormatter alloc]init];
    [format setDateFormat:@"HH:mm"];
    selectedtime=datePicker.date;
    _timetextfield.text=[NSString stringWithFormat:@"%@",[format stringFromDate:selectedtime]];
    if([_timetextfield.text isEqualToString:@"00:00"])
    {
       _timetextfield.text=@"00:01";
    }
    [self dismissDatePicker:nil];
    
}
- (void)removeViews:(id)object
{
    [[self.view viewWithTag:1000] removeFromSuperview];
    [[self.view viewWithTag:1001] removeFromSuperview];
}

- (void)dismissDatePicker:(id)sender
{
//    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44);
//    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, self.view.bounds.size.width, 216);
//    [UIView beginAnimations:@"MoveOut" context:nil];
//    [self.view viewWithTag:1000].frame = datePickerTargetFrame;
//    [self.view viewWithTag:1001].frame = toolbarTargetFrame;
//    [UIView setAnimationDelegate:self];
    if(sender)
    {
        _timetextfield.text=@"00:00";
    }
    [self dismisskeyboard];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    //[UIView commitAnimations];
}
-(void)opentimepicker
{
  //  CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height-216-44, self.view.bounds.size.width, 44);
  //  CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height-216, self.view.bounds.size.width, 216);
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height+44,self.view.bounds.size.width, 216)];
    datePicker.tag = 1000;
    datePicker.backgroundColor=[UIColor whiteColor];
    
    
    datePicker.datePickerMode=UIDatePickerModeCountDownTimer;
    [_timetextfield setInputView:datePicker];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44)];
    toolBar.tag = 1001;
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.backgroundColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    
    UIBarButtonItem *Cancel=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(dismissDatePicker:)];
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(changeDate1:)];
    [toolBar setItems:[NSArray arrayWithObjects:Cancel,spacer, doneButton, nil]];
    
    [_timetextfield setInputAccessoryView:toolBar];

}
#pragma textfielddelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSCharacterSet *nonNumberSet;
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if(textField==_armtextfield||textField==_chesttextfield||textField==_waisttextfield||textField==_hiptextfield||textField==_thightextfield||textField==_weighttextfield)
    {
        
                if([self.activeTextField.text rangeOfString:@"."].location !=NSNotFound)
                {
                    nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
                }
                else
                {
                    nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@".0123456789"] invertedSet];
                    
                }
                if(newLength<=6)
                {
        
                    return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0) || [string isEqualToString:@""];
                }
                else
                {
                    return NO;
                }
        
    }
    else if(self.activeTextField==_timetextfield)
    {
        return NO;
    }
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
    pointtrack =textField.frame.origin;
   // NSLog(@"%f",screenBounds.size.height);
    // new code
   // pointtrackcontentoffset=self.aScrollView.contentOffset;
    
    tap = [[UITapGestureRecognizer alloc]
           initWithTarget:self
           action:@selector(dismisskeyboard)];
    tap.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:tap];
    
    // new one
    if(self.activeTextField==_timetextfield)
    {
        [self opentimepicker];
    }
    
    // new code
    if(self.activeTextField.tag==[questionarraystore count])
    {
        self.activeTextField.returnKeyType=UIReturnKeyDone;
    }
    
    if([questionarraystore count]==1)
    {
        if(self.activeTextField==_firstanswertextfield)
        {
       pointtrack.y=240;
        }
    }
   
    if(screenBounds.size.height==480)
    {
        if(pointtrack.y>400)
        {
            pointtrack.y=350;
        }
        [self.aScrollView setContentOffset:CGPointMake(0, pointtrack.y+90) animated:YES];
       
    }
    else if(screenBounds.size.height==568)
    {
        if(pointtrack.y>400)
        {
            pointtrack.y=355;
        }
        [self.aScrollView setContentOffset:CGPointMake(0, pointtrack.y+50) animated:YES];
    
    }
    else if(screenBounds.size.height==667)
    {
        if(pointtrack.y>330)
        {
            pointtrack.y=320;
        }
        
        [self.aScrollView setContentOffset:CGPointMake(0, pointtrack.y) animated:YES];
    }
    else if(screenBounds.size.height==736)
    {
        if(pointtrack.y>330)
        {
            pointtrack.y=330;
        }
        if(pointtrack.y>50)
        {
        [self.aScrollView setContentOffset:CGPointMake(0, pointtrack.y-50) animated:YES];
        }
    }
    else
    {
        [self.aScrollView setContentOffset:CGPointMake(0, pointtrack.y-327) animated:YES];
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder)
    {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

-(void)dismissTheLoader{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [SVProgressHUD dismiss];
}

- (IBAction)didTapSubmit:(id)sender
{
       [self.activeTextField resignFirstResponder];
    // Validation
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Please wait a moment..."];
    
    if(!([NSString validation:_armtextfield.text]&&[NSString validation:_chesttextfield.text]&&[NSString validation:_waisttextfield.text]&&[NSString validation:_hiptextfield.text]&&[NSString validation:_thightextfield.text]&&[NSString validation:_weighttextfield.text]&&[NSString validation:_timetextfield.text]))
    {
        [self dismissTheLoader];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"All the fields are mandatory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    
    else if(![NSString validation:_firstanswertextfield.text])
    {
         [self dismissTheLoader];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please fill First answer" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
           [alert show];
      
    }
    
    
    else if(![NSString validation:_secondanswertextfield.text])
    {
        [self dismissTheLoader];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please fill Second answer" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
       
    }
    
    else if(![NSString validation:_thirdanswertextfield.text])
    {
        [self dismissTheLoader];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please fill Third answer" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
       
    }
    
    else if (!(selectedimage&&selectedimage1&&selectedimage2))
    {
         [self dismissTheLoader];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please upload all images" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
       
    }
    else
    {
        //[self assesmentservicecall];
        [self performSelector:@selector(assesmentservicecall) withObject:self afterDelay:0.005];
    }
    
}
-(void)assesmentservicecall
{

    NSString *path =[NSString stringWithFormat:@"%@1171",app->parentUrl];
    // AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://server.url"]];
    // AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
   
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    imageDataPicker=(NSData *)UIImagePNGRepresentation(selectedimage);
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",@"Assessment",@"assmentname",_armtextfield.text,@"arm",_chesttextfield.text,@"chest",_waisttextfield.text,@"weist",_hiptextfield.text,@"hip",_thightextfield.text,@"thigh",_weighttextfield.text,@"weight",_timetextfield.text,@"assmenttime",[questionidarray objectAtIndex:0],@"ques_one_id",[questionidarray objectAtIndex:1],@"ques_two_id",[questionidarray objectAtIndex:2],@"ques_three_id",_firstanswertextfield.text,@"ques_one_answ",_secondanswertextfield.text,@"ques_two_answ",_thirdanswertextfield.text ,@"ques_three_answ",nil];
    AFHTTPRequestOperation *op = [manager POST:path parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //do not put image inside parameters dictionary as I did, but append it!
        if((NSData *)UIImagePNGRepresentation(selectedimage)&&(NSData *)UIImagePNGRepresentation(selectedimage1)&&(NSData *)UIImagePNGRepresentation(selectedimage2))
        {
            [formData appendPartWithFileData:(NSData *)UIImagePNGRepresentation(selectedimage) name:@"frontimage" fileName:@"frontphoto.png" mimeType:@"image/png"];
            [formData appendPartWithFileData:(NSData *)UIImagePNGRepresentation(selectedimage1) name:@"leftimage" fileName:@"leftphoto.png" mimeType:@"image/png"];
            [formData appendPartWithFileData:(NSData *)UIImagePNGRepresentation(selectedimage2) name:@"backimage" fileName:@"backphoto.png" mimeType:@"image/png"];
        }
    }
                                       success:^(AFHTTPRequestOperation *operation, id responseObject)
                                  {
                                      NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
                                      NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
                                      NSLog(@"%@",responseStr);
                                      NSDictionary *dict1 = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
                                      NSLog(@"%@",dict1);
                                      //here is place for code executed in success case
                                      if([[dict1 valueForKey:@"status"]integerValue]==0)
                                      {
                                          UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict1 valueForKey:@"error"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                          [alert show];
                                          [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                          [SVProgressHUD dismiss];
                                      }
                                      else
                                      {
                                          
                                          UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Assessment Completed" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                          [alert show];
                                          alert.tag=101;
                                          //
                                          [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                          [SVProgressHUD dismiss];
                                      }
                                      
                                  }
                                       failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                  
                                  {
                                      [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                      [SVProgressHUD dismiss];
                                      NSLog(@"Error: %@", [error localizedDescription]);
                                      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                      [alert show];
                                  }
                                  ];
    [op start];
}

 #pragma mark - Navigation
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   
    if ([segue.identifier isEqualToString:@"seguetobigimageshowing"])
    {
        PreviewImageViewController *instance = (PreviewImageViewController *)segue.destinationViewController;
        NSString *myImagePath=[_assementarraydata valueForKey:@"frontimage"];
        NSString *myImagePath1=[_assementarraydata valueForKey:@"leftimage"];
        NSString *myImagePath2=[_assementarraydata valueForKey:@"backimage"];
        if(self.activebutton==_frontbutton)
        {
            instance.previewimagestring=myImagePath;
            instance.imagetype=@"frontphoto";
        }
        else if(self.activebutton==_leftbutton)
        {
            instance.previewimagestring=myImagePath1;
            instance.imagetype=@"leftphoto";
            
        }
        else if(self.activebutton==_backbutton)
        {
            instance.previewimagestring=myImagePath2;
            instance.imagetype=@"backphoto";
        }
    }
}

- (IBAction)didTapFront:(id)sender
{
    self.activebutton=(UIButton *)sender;
    
    if(_assementarraydata)
    {
        [self performSegueWithIdentifier:@"seguetobigimageshowing" sender:self];
    }
    else
    {
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Upload Image \nFrom:" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Gallery", nil];
    alert.tag=100;
    [alert show];
    }
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==100)
    {
        if(buttonIndex==1)
        {
            [self takepicture];
        }
        else if(buttonIndex==2)
        {
            [self selectgallery];
        }
    }
    else if(alertView.tag==101)
    {
        [self performSegueWithIdentifier:@"segueToDashboard" sender:self];
    }
}
-(void)selectgallery
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    ImagePicker=[[UIImagePickerController alloc]init];
    ImagePicker.delegate=(id)self;
    ImagePicker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    // imagepic.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:ImagePicker animated:YES completion:nil];
}

- (IBAction)didTapLeft:(id)sender
{
    self.activebutton=(UIButton *)sender;
    if(_assementarraydata)
    {
       [self performSegueWithIdentifier:@"seguetobigimageshowing" sender:self];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Upload Image \nFrom:" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Gallery", nil];
        alert.tag=100;
        [alert show];
    }
   
}

- (IBAction)didTapback:(id)sender
{
     self.activebutton=(UIButton *)sender;
    if(_assementarraydata)
    {
        [self performSegueWithIdentifier:@"seguetobigimageshowing" sender:self];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Upload Image \nFrom:" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Gallery", nil];
        alert.tag=100;
        [alert show];
    }
    
}
-(void)takepicture
{
    /******************** code for camera ***********************/
    
    ImagePicker=[[UIImagePickerController alloc]init];
    if    (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
    }
    else
    {
        
        ImagePicker.delegate=(id)self;
        ImagePicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        // imagepic.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:ImagePicker animated:YES completion:nil];
        NSLog(@"camera");
    }
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    // new code
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [picker dismissViewControllerAnimated:YES completion:nil];
    // [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    
    if(self.activebutton==_frontbutton)
    {
        selectedimage=[info objectForKey:UIImagePickerControllerOriginalImage];
        selectedimage=[self scaleAndRotateImage:selectedimage];
        _frontimageupload.contentMode=UIViewContentModeScaleAspectFit;
        _frontimageupload.image=selectedimage;
    }
    else if(self.activebutton==_leftbutton)
    {
        selectedimage1=[info objectForKey:UIImagePickerControllerOriginalImage];
        selectedimage1=[self scaleAndRotateImage:selectedimage1];
        _leftsideimageupload.contentMode=UIViewContentModeScaleAspectFit;
        _leftsideimageupload.image=selectedimage1;
    }
    else if(self.activebutton==_backbutton)
    {
        selectedimage2=[info objectForKey:UIImagePickerControllerOriginalImage];
        selectedimage2=[self scaleAndRotateImage:selectedimage2];
        _backimageupload.contentMode=UIViewContentModeScaleAspectFit;
        _backimageupload.image=selectedimage2;
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}
// new method
- (UIImage *)scaleAndRotateImage:(UIImage *)image
{
    int kMaxResolution = 640; // Or whatever
    CGImageRef imgRef = image.CGImage;
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

@end
