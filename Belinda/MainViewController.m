//
//  MainViewController.m
//  Belinda
//
//  Created by Nivendru on 17/10/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "MainViewController.h"
#import "SidebarViewController.h"

@interface MainViewController () <SWRevealViewControllerDelegate>
{
    AppDelegate *app;
    UITapGestureRecognizer *tap;
}

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.rowSelectionDelegate = (SidebarViewController *)self.revealViewController.rearViewController;
    if(app->pushdictionary)
    {
    // testing
    if([[[app->pushdictionary valueForKey:@"aps"] valueForKey:@"type"]isEqualToString:@"goal"])
    {
     [self.rowSelectionDelegate selectRowWithHeading:3];
     [self performSegueWithIdentifier:@"segueToquickfitness" sender:self];
    }
    else if([[[app->pushdictionary valueForKey:@"aps"] valueForKey:@"type"]isEqualToString:@"assessment"])
    {
     [self.rowSelectionDelegate selectRowWithHeading:2];
     [self performSegueWithIdentifier:@"segueToessentialgoal" sender:self];
    }
    else if([[[app->pushdictionary valueForKey:@"aps"] valueForKey:@"type"]isEqualToString:@"food"])
    {
        [self.rowSelectionDelegate selectRowWithHeading:1];
        [self performSegueWithIdentifier:@"segueToFooddiarylisting" sender:self];
    }
    }
    // end
}
#pragma mark - SWRevealViewController Delegate Methods

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
   
    NSLog(@"Delegate call");
}

-(void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
}


-(void)setCustomNavigationButton
{
    
    UIButton *Menubutton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 28)];
    UIImageView *buttonImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 20, 18)];
    buttonImage.image=[UIImage imageNamed:@"menu_icon.png" ];
    buttonImage.clipsToBounds=YES;
    buttonImage.contentMode=UIViewContentModeScaleAspectFit;
    [Menubutton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [Menubutton addSubview:buttonImage];
    UIBarButtonItem *menubtn=[[UIBarButtonItem alloc]initWithCustomView:Menubutton];
    self.navigationItem.leftBarButtonItem=menubtn;
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.revealViewController.frontViewController.view setUserInteractionEnabled:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.rowSelectionDelegate selectRowWithHeading:0];
    
    self.title = @"Belinda Carusi Fitness Hub";
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:220/255.0f green:25/255.0f blue:124/255.0f alpha:1.0f];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    // Hide navigation bar and customize
    self.navigationItem.hidesBackButton=YES;
    [self setCustomNavigationButton];
    
    
    // new code
    hiderevealButton=[[UIButton alloc]initWithFrame:self.view.bounds];
    [hiderevealButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    hiderevealButton.backgroundColor=[UIColor clearColor];
    [self.view addSubview:hiderevealButton];
    [hiderevealButton setHidden:YES];
    
   
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:NO];
}

- (IBAction)didTapfooddiary:(id)sender
{
    //made true customappear
    [self.rowSelectionDelegate selectRowWithHeading:1];
    app->checkcustompush=FALSE;
    [self performSegueWithIdentifier:@"segueToFooddiarylisting" sender:self];
}

- (IBAction)didTapessentialgoal:(id)sender
{
    //made true customappear
    // new code
    [self checkassesmentdaytodayservice];
    
    
}
#pragma check on same date any assesment is created or not?
-(void)checkassesmentdaytodayservice
{
    NSString *path =[NSString stringWithFormat:@"%@1309",app->parentUrl];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",nil];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Checking For Assessment.."];
    [manager POST:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"You have already completed the Assessment for today" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             [SVProgressHUD dismiss];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             
         }
         else
         {
//             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"result"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//             [alert show];
             [SVProgressHUD dismiss];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [self.rowSelectionDelegate selectRowWithHeading:2];
             app->checkcustompush=FALSE;
             [self performSegueWithIdentifier:@"segueToessentialgoal" sender:self];
         }
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [SVProgressHUD dismiss];
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}

- (IBAction)didTapQuickfitness:(id)sender
{
    [self.rowSelectionDelegate selectRowWithHeading:3];
    app->checkcustompush=FALSE;
    [self performSegueWithIdentifier:@"segueToquickfitness" sender:self];
}

- (IBAction)didTapResults:(id)sender
{
    [self.rowSelectionDelegate selectRowWithHeading:4];
    app->checkcustompush=FALSE;
    [self performSegueWithIdentifier:@"seguetoresultlist" sender:self];
}

- (IBAction)didTapshoppinglist:(id)sender
{
    [self.rowSelectionDelegate selectRowWithHeading:5];
    app->checkcustompush=FALSE;
    [self performSegueWithIdentifier:@"seguetoshoppinglist" sender:self];
}

- (IBAction)didTapRecipe:(id)sender
{
    [self.rowSelectionDelegate selectRowWithHeading:6];
     app->checkcustompush=FALSE;
    [self performSegueWithIdentifier:@"seguetorecipelist" sender:self];
}
@end
