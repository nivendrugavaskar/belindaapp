//
//  RecipedetailViewController.m
//  Belinda
//
//  Created by Nivendru on 11/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "RecipedetailViewController.h"

@interface RecipedetailViewController ()
{
    AppDelegate *app;
    NSMutableArray *ingradientarray;
}

@end

@implementation RecipedetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:220/255.0f green:25/255.0f blue:124/255.0f alpha:1.0f];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.title=@"Recipes";
    
    // Hide navigation bar and customize
    self.navigationItem.hidesBackButton=YES;
    [self setCustomNavigationButton];
    
    NSLog(@"Array=%@",_recipedescarray);
    NSLog(@"Fulldict=%@",_fulldetailarraydesc);
    if(_recipedescarray)
    {
    ingradientarray=[[NSMutableArray alloc]init];
    ingradientarray=[_recipedescarray valueForKey:@"recipe_ingredients"];
    _titleLabel.text=[_recipedescarray valueForKey:@"recipe_name"];
   
    
    }
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    //new code for image load
    __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.color=[UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
    activityIndicator.center = CGPointMake(CGRectGetMidX(_recipepic.bounds), CGRectGetMidY(_recipepic.bounds));
    activityIndicator.hidesWhenStopped = YES;
    
    NSString *myImagePath=[_recipedescarray valueForKey:@"image"];
    
    __block BOOL successfetchimage=FALSE;
    __weak UIImageView *setimage = _recipepic;
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:myImagePath]];
    [_recipepic setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"no_image.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         setimage.contentMode=UIViewContentModeScaleAspectFit;
         setimage.image=image;
         successfetchimage=TRUE;
         [activityIndicator removeFromSuperview];
     }
                               failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
     {
         [activityIndicator removeFromSuperview];
     }];
    if(!successfetchimage)
    {
        if(![myImagePath isEqualToString:@""])
        {
        [_recipepic addSubview:activityIndicator];
        [activityIndicator startAnimating];
        }
    }
    // end
}

-(void)webserviceforaddtoshoppinglist
{
    NSString *path =[NSString stringWithFormat:@"%@852",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",[_fulldetailarraydesc valueForKey:@"term_taxonomy_id"],@"term_taxonomy_id",[_fulldetailarraydesc valueForKey:@"term_id"],@"term_id",[_recipedescarray valueForKey:@"recipe_id"],@"recipe_id",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Adding To Shopping List.."];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
         }
         else
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Recipe added to Shopping List" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             alert.tag=100;
             
             
         }
         [SVProgressHUD dismiss];
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==100)
    {
        if(buttonIndex==0)
        {
            [self back];
        }
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    // new code
    hiderevealButton=[[UIButton alloc]initWithFrame:self.view.bounds];
    [hiderevealButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    hiderevealButton.backgroundColor=[UIColor clearColor];
    [self.view addSubview:hiderevealButton];
    [hiderevealButton setHidden:YES];
}
-(void)back
{
    NSLog(@"Back");
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

-(void)setCustomNavigationButton
{
    
    UIButton *Menubutton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 28)];
    UIImageView *buttonImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 20, 18)];
    buttonImage.image=[UIImage imageNamed:@"menu_icon.png" ];
    buttonImage.clipsToBounds=YES;
    buttonImage.contentMode=UIViewContentModeScaleAspectFit;
    [Menubutton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [Menubutton addSubview:buttonImage];
    UIBarButtonItem *menubtn=[[UIBarButtonItem alloc]initWithCustomView:Menubutton];
    self.navigationItem.leftBarButtonItem=menubtn;
    
    // Adding right navigation bar button
    UIButton *custombuttonadd=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [custombuttonadd addTarget:self action:@selector(addtoshoppinglist) forControlEvents:UIControlEventTouchUpInside];
    custombuttonadd.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageviewadd=[[UIImageView alloc]initWithFrame:CGRectMake(0,0, 30, 30)];
    buttonimageviewadd.image=[UIImage imageNamed:@"addtoshoppinglist.png"];
    buttonimageviewadd.contentMode=UIViewContentModeScaleAspectFit;
    [custombuttonadd addSubview:buttonimageviewadd];
    
    UIBarButtonItem *addbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombuttonadd];
    addbarbutton.style = UIBarButtonItemStylePlain;
   
    // Optional: if you want to add space between the refresh & profile buttons
        UIBarButtonItem *fixedSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        fixedSpaceBarButtonItem.width =25;
    
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 26, 26)];
    [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 6, 18, 18)];
    buttonimageview.image=[UIImage imageNamed:@"back_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    
    UIBarButtonItem *backbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombutton];
    backbarbutton.style = UIBarButtonItemStylePlain;
    // end
    self.navigationItem.rightBarButtonItems = @[addbarbutton,fixedSpaceBarButtonItem
                                                ,backbarbutton];
   
}
-(void)addtoshoppinglist
{
    [self webserviceforaddtoshoppinglist];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"seguetoingradientdetails"])
    {
        ingradientlistdetailViewController *instance = (ingradientlistdetailViewController *)segue.destinationViewController;
        instance.ingradientArray=ingradientarray;
        
        
        
    }
    else
    {
           HowtoMakeViewController *instance = (HowtoMakeViewController *)segue.destinationViewController;
           instance.descriptiontext=[_recipedescarray valueForKey:@"recipe_description"];
           instance.recipename=[_recipedescarray valueForKey:@"recipe_name"];
        
    }
        
    
}


- (IBAction)didTapingradient:(id)sender
{
    [self performSegueWithIdentifier:@"seguetoingradientdetails" sender:self];
}
- (IBAction)didTapHowtomake:(id)sender
{
    [self performSegueWithIdentifier:@"seguetoHowtomake" sender:self];
}
@end
