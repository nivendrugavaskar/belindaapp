//
//  Goalstatusviewcontroller.h
//  Belinda
//
//  Created by Nivendru on 05/12/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@protocol goalstatusprotocol <NSObject>
-(void)sendflagtrueingoalstatussuccess:(BOOL)flag;
@end

@interface Goalstatusviewcontroller : UIViewController<SWRevealViewControllerDelegate,UIAlertViewDelegate,UIImagePickerControllerDelegate,UITextViewDelegate>
{
    AppDelegate *app;
    UIImagePickerController *ImagePicker;
    UIImage *selectedimage;
    NSData *imageDataPicker;
    
    CGRect screenBounds,storerect;
    CGPoint pointtrack,pointtrackcontentoffset;
}
@property (strong, nonatomic) IBOutlet UITextView *commentTextbox;
- (IBAction)didTapcamera:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *camerabtn;
- (IBAction)didTapSubmit:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *submitbtn;
@property (strong, nonatomic) IBOutlet UIImageView *uploadimage;
@property (strong, nonatomic) IBOutlet UILabel *comment;
@property(strong,nonatomic)UITextView *activeTextView;

@property(strong,nonatomic)NSString *commentstatus;
@property(strong,nonatomic)NSString *fetchgoalid;
//// new code for testing
@property (assign) id <goalstatusprotocol> goalstatusdelegate;

@property(strong,nonatomic) NSMutableArray *fetchdataDict;

@end
