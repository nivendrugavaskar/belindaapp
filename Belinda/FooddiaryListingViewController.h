//
//  FooddiaryListingViewController.h
//  Belinda
//
//  Created by Nivendru on 31/10/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Fooddiarylistingcell.h"
#import "AppDelegate.h"
#import "AddFoodDiaryViewController.h"

@interface FooddiaryListingViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,addfooddiaryprotocal>
{
    
}
@property (strong, nonatomic) IBOutlet UITableView *aTableView;

@end
