//
//  AppDelegate.h
//  Belinda
//
//  Created by Nivendru on 16/10/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "SWRevealViewController.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "UIKit+AFNetworking.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "Reachability.h"
#import "NSString+Validation.h"
#import <MapKit/MapKit.h>

#pragma it was for testing when we were not storing device token in nsuserdefault

@protocol TokenSendFromAppDele <NSObject>
-(void)sendtoken:(NSString *)devicetoken;
@end

#pragma create an invisible button over front view when side table is open at that time this button will active so that we can 1st hide side bar and then can do other thing
UIButton *hiderevealButton;

@interface AppDelegate : UIResponder <UIApplicationDelegate,UIAlertViewDelegate>
{
@public
    NSString *parentUrl;
    // check custompageappear
    BOOL checkcustompush,fromregistration,statusactive;
    NSString *userid,*loginType,*clickedstatusgoal,*devicetokenstring;
    NSMutableDictionary *pushdictionary;
}

@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (BOOL)returnInternetConnectionStatus;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@property (strong,nonatomic)NSMutableArray *testingpurposearray;
@property (assign) id <TokenSendFromAppDele> appdelegate;

@end

