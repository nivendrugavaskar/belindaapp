//
//  ViewController.h
//  Belinda
//
//  Created by Nivendru on 16/10/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "MainViewController.h"
#import "AppDelegate.h"



@interface ViewController : UIViewController<UITextFieldDelegate>
{
     CGPoint pointtrack,pointtrackcontentoffset;
     @public
     NSString *emailtext,*passwordtext;
}
@property (strong, nonatomic) IBOutlet UIScrollView *aScrollView;
@property (strong, nonatomic) IBOutlet UIView *innerViewScrollview;

- (IBAction)didTapLogin:(id)sender;
- (IBAction)didTapRegistration:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *regButton;

- (IBAction)didTapForgetPassword:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *forgetpwdButton;
@property (strong, nonatomic) IBOutlet UIImageView *imgviewforgetpassword;

@property (strong, nonatomic) IBOutlet UITextField *emailIdText;
@property (strong, nonatomic) IBOutlet UITextField *passwordText;
@property(strong,nonatomic) UITextField *activeTextField;
@property (strong, nonatomic) IBOutlet UIImageView *imageviewsignup;
- (IBAction)didTaprememberme:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *remembermebutton;
@property (strong, nonatomic) IBOutlet UIImageView *tickuntickimageview;
- (IBAction)didTapFacebooklogin:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *fbloginbutton;



@end

