//
//  ingradientlistdetailViewController.h
//  Belinda
//
//  Created by Nivendru on 21/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Ingradientcell.h"

@interface ingradientlistdetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    AppDelegate *app;
}
@property (strong, nonatomic) IBOutlet UITableView *aTableview;
@property (strong,nonatomic) NSMutableArray *ingradientArray;

@end
