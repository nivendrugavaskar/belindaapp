//
//  shoppinglistViewController.m
//  Belinda
//
//  Created by Nivendru on 11/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "shoppinglistViewController.h"

@interface shoppinglistViewController ()
{
    AppDelegate *app;
    NSMutableArray *headermenu,*shownheadermenu;
    NSMutableArray *recipedetailarray,*fulldetailarray,*userrecipedetailid,*adminrecipeid,*term_taxonomy_idarray,*term_idarray,*ingredientidarray;
    NSDictionary *dict;
    NSString *useringid,*selectedingradient,*recipeid,*term_id,*term_taxonomy_id,*ingredientid,*selectedquantity,*sendmyowningredient;
    
    BOOL loadermaintain;
    NSIndexPath *indexPathdeleterow;
}

@end

@implementation shoppinglistViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    // set bool false
    loadermaintain=FALSE;
    // headermenu=[[NSArray alloc]initWithObjects:@"BreakFast",@"Lunch",@"Biryani",@"Testing", nil];
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:220/255.0f green:25/255.0f blue:124/255.0f alpha:1.0f];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.title=@"Shopping List";
    // Hide navigation bar and customize
    self.navigationItem.hidesBackButton=YES;
    [self setCustomNavigationButton];
    // call webservice userrecipelisting
    [self webserviceuserreciepelisting];
}
-(void)webserviceuserreciepelisting
{
    NSString *path =[NSString stringWithFormat:@"%@864",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    if(!loadermaintain)
    {
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Fetching Your Shopping List.."];
    }
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         dict=[[NSDictionary alloc]init];
         dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         //here is place for code executed in success case
         if([[dict valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
         }
         else
         {
             fulldetailarray=[[NSMutableArray alloc]init];
             headermenu=[[NSMutableArray alloc]init];
             recipedetailarray=[[NSMutableArray alloc]init];
             adminrecipeid=[[NSMutableArray alloc]init];
             term_taxonomy_idarray=[[NSMutableArray alloc]init];
             term_idarray=[[NSMutableArray alloc]init];
             ingredientidarray=[[NSMutableArray alloc]init];
            
             NSArray *localfulldetail=[dict valueForKey:@"result"];
             NSArray *storeresponseheader=[localfulldetail valueForKey:@"recipe_type"];
             NSArray *storeresponsechildofheader=[[localfulldetail valueForKey:@"recipe_details"]mutableCopy];
             for(int i=0;i<[storeresponsechildofheader count];i++)
             {
                 if([[storeresponsechildofheader objectAtIndex:i]count]>0)
                 {
                     for (int k=0; k<[[storeresponsechildofheader objectAtIndex:i]count]; k++)
                     {
                    [headermenu addObject:[NSString stringWithFormat:@"%@%@",[storeresponseheader objectAtIndex:i],[[[storeresponsechildofheader objectAtIndex:i] objectAtIndex:k] valueForKey:@"recipe_name"]]];
                         
                    // storing term_taxonomy_id and term_id for all subcategory
                    [term_taxonomy_idarray addObject:[[localfulldetail objectAtIndex:i]valueForKey:@"term_taxonomy_id"]];
                    [term_idarray addObject:[[localfulldetail objectAtIndex:i]valueForKey:@"term_id"]];
                    // new code for ingradient
                    [ingredientidarray addObject:[[[storeresponsechildofheader objectAtIndex:i]objectAtIndex:k]valueForKey:@"recipe_ingredients"]];
                    // store value of user recipe
                    [recipedetailarray addObject:[[[[[storeresponsechildofheader objectAtIndex:i] objectAtIndex:k] valueForKey:@"recipe_ingredients"] valueForKey:@"value"] mutableCopy]];
                    [adminrecipeid addObject:[[[storeresponsechildofheader objectAtIndex:i] objectAtIndex:k] valueForKey:@"recipe_id"]];
                    }
                   
                 }
             }
             [self.aTableview reloadData];
             loadermaintain=FALSE;
         }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    // new code
    hiderevealButton=[[UIButton alloc]initWithFrame:self.view.bounds];
    [hiderevealButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    hiderevealButton.backgroundColor=[UIColor clearColor];
    [self.view addSubview:hiderevealButton];
    [hiderevealButton setHidden:YES];
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)setCustomNavigationButton
{
    
    UIButton *Menubutton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 28)];
    UIImageView *buttonImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 20, 18)];
    buttonImage.image=[UIImage imageNamed:@"menu_icon.png" ];
    buttonImage.clipsToBounds=YES;
    buttonImage.contentMode=UIViewContentModeScaleAspectFit;
    [Menubutton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [Menubutton addSubview:buttonImage];
    UIBarButtonItem *menubtn=[[UIBarButtonItem alloc]initWithCustomView:Menubutton];
    self.navigationItem.leftBarButtonItem=menubtn;
    
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"back_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    
    UIBarButtonItem *backbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombutton];
    backbarbutton.style = UIBarButtonItemStylePlain;
    // new code
    if(!app->checkcustompush)
    {
        self.navigationItem.rightBarButtonItems = @[backbarbutton];
    }
    
    
}


#pragma mark -
#pragma mark TableView Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"Cell";
    Shoppinglistcell *cell=(Shoppinglistcell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    if(cell==nil)
    {
    cell=[[Shoppinglistcell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    NSString *animal = [[recipedetailarray objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    cell.textlabel.frame=CGRectMake(20,0, self.aTableview.bounds.size.width-60, 30);
    cell.textlabel.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    cell.textlabel.text = animal;
    if([[NSString stringWithFormat:@"%@",[headermenu objectAtIndex:indexPath.section]]isEqualToString:@"My Own"])
    {
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(tableView.bounds.size.width-60, 0, 50, 30);
    UIImageView *buttonimage=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 10, 10)];
    buttonimage.contentMode=UIViewContentModeScaleAspectFit;
    buttonimage.image=[UIImage imageNamed:@"close_icon"];
    [button addTarget:self action:@selector(deleteButtonPressed1:) forControlEvents:UIControlEventTouchUpInside];
      //  button.backgroundColor=[UIColor redColor];
    [button addSubview:buttonimage];
    [cell.contentView addSubview:button];
    }
    else
    {
       
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(tableView.bounds.size.width-60, 0, 50, 30);
        UIImageView *buttonimage=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 10, 10)];
        buttonimage.contentMode=UIViewContentModeScaleAspectFit;
        buttonimage.image=[UIImage imageNamed:@"close_icon"];
        [button addTarget:self action:@selector(deleteButtonPressedadmin:) forControlEvents:UIControlEventTouchUpInside];
       // button.backgroundColor=[UIColor blueColor];
        [button addSubview:buttonimage];
        [cell.contentView addSubview:button];
    }
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 30.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [headermenu count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[recipedetailarray objectAtIndex:section]count];
   
}
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width-60, 40)];
    [headerView setBackgroundColor:[UIColor colorWithRed:2/255.0f green:3/255.0f blue:4/255.0f alpha:0.8f]];
    
    UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(20,5,headerView.frame.size.width-15,30)];
    tempLabel.backgroundColor=[UIColor clearColor];
    tempLabel.textColor = [UIColor whiteColor]; //here you can change the text color of header.
    tempLabel.font = [UIFont boldSystemFontOfSize:19.0f];
    tempLabel.text=[headermenu objectAtIndex:section];
    [headerView addSubview:tempLabel];
    if(![tempLabel.text isEqualToString:@"My Own"])
    {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.tag = section + 1000;
    button.frame = CGRectMake(headerView.frame.size.width-5, 0, 60, 40);
    UIImageView *buttonimage=[[UIImageView alloc]initWithFrame:CGRectMake(12.5,12.5, 15, 15)];
    buttonimage.contentMode=UIViewContentModeScaleAspectFit;
    buttonimage.image=[UIImage imageNamed:@"close_icon"];
    [button addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button addSubview:buttonimage];
    [headerView addSubview:button];
    }
    return headerView;
}
- (IBAction)deleteButtonPressed1:(UIButton *)sender
{
    self.tracksectiondelete=sender;
//    Shoppinglistcell *clickedbutton=(Shoppinglistcell *)sender.superview.superview.superview;
//    indexPathdeleterow = [self.aTableview indexPathForCell:clickedbutton];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:self.aTableview];
    indexPathdeleterow = [self.aTableview indexPathForRowAtPoint:buttonPosition];
    // alert
    UIAlertView *showalert=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"Would you like to remove %@",[[recipedetailarray objectAtIndex:indexPathdeleterow.section]objectAtIndex:indexPathdeleterow.row]] message:nil delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    showalert.tag=101;
    [showalert show];
    
}
// new code
- (IBAction)deleteButtonPressedadmin:(UIButton *)sender
{
    self.trackrowdelete=sender;
//    Shoppinglistcell *clickedbutton=(Shoppinglistcell *)sender.superview.superview.superview;
//    
//    indexPathdeleterow=[[NSIndexPath alloc] init];
//    indexPathdeleterow = [self.aTableview indexPathForCell:clickedbutton];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:self.aTableview];
    indexPathdeleterow = [self.aTableview indexPathForRowAtPoint:buttonPosition];
    NSLog(@"sect=%ld",(long)indexPathdeleterow.section);
    NSLog(@"row=%ld",(long)indexPathdeleterow.row);
    // alert
    UIAlertView *showalert=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"Would you like to remove %@",[[recipedetailarray objectAtIndex:indexPathdeleterow.section]objectAtIndex:indexPathdeleterow.row]] message:nil delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    showalert.tag=104;
    [showalert show];
    
}
- (IBAction)deleteButtonPressed:(UIButton *)sender
{
    self.tracksectiondelete=sender;
    // alert
    UIAlertView *showalert=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"Would you like to remove %@",[headermenu objectAtIndex:sender.tag - 1000]] message:nil delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    showalert.tag=100;
    [showalert show];
    
}

-(void)deleteshoppinglistsectionwebservice:(UIButton *)sender
{

   
    NSString *path =[NSString stringWithFormat:@"%@858",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",recipeid,@"recipe_id",term_id,@"term_id",term_taxonomy_id,@"term_taxonomy_id",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Deleting Recipe.."];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict1 = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         //here is place for code executed in success case
         if([[dict1 valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
         }
         else
         {
             [adminrecipeid removeObject:recipeid];
             NSInteger section = sender.tag - 1000;
             [headermenu removeObjectAtIndex:section];
             [recipedetailarray removeObjectAtIndex:section];
             [self.aTableview deleteSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationFade];
             loadermaintain=TRUE;
             [self webserviceuserreciepelisting];
         }
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}
// newcode for row delete
-(void)deleteshoppinglistsectionwebservicerow:(NSIndexPath *)indexPath
{
    NSString *path =[NSString stringWithFormat:@"%@1117",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",recipeid,@"recipe_id",term_id,@"term_id",term_taxonomy_id,@"term_taxonomy_id",ingredientid,@"ingredientid",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Deleting Ingredient.."];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict1 = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         //here is place for code executed in success case
         if([[dict1 valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
         }
         else
         {
             [[recipedetailarray objectAtIndex:indexPath.section] removeObjectAtIndex:indexPath.row];
             NSArray *deletedrow = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section]];
             [self.aTableview deleteRowsAtIndexPaths:deletedrow
                                    withRowAnimation:UITableViewRowAnimationFade];
             
             if([[recipedetailarray objectAtIndex:indexPath.section]count]==0)
             {
                 [headermenu removeObjectAtIndex:indexPath.section];
                 [recipedetailarray removeObjectAtIndex:indexPath.section];
                 [self.aTableview deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
             }
             loadermaintain=TRUE;
             [self webserviceuserreciepelisting];
         }
        
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
         
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
     }];
}


-(void)webservicedeleteuseringradients:(NSIndexPath *)indexPath
{
    NSString *path =[NSString stringWithFormat:@"%@861",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",useringid,@"useringredientsid",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Deleting Ingradient.."];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict1 = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         //here is place for code executed in success case
         if([[dict1 valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
         }
         else
         {
             [[recipedetailarray objectAtIndex:indexPath.section] removeObjectAtIndex:indexPath.row];
             NSArray *deletedrow = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section]];
             [self.aTableview deleteRowsAtIndexPaths:deletedrow
                                    withRowAnimation:UITableViewRowAnimationFade];
             
             if([[recipedetailarray objectAtIndex:indexPath.section]count]==0)
             {
                 [headermenu removeObjectAtIndex:indexPath.section];
                 [recipedetailarray removeObjectAtIndex:indexPath.section];
                 
                 [self.aTableview deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
                 
             }
             //[userrecipedetailid removeObject:useringid];
             loadermaintain=TRUE;
             [self webserviceuserreciepelisting];
             
             
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didTapaddingradients:(id)sender
{
    alertx=[[UIAlertView alloc]initWithTitle:@"Add Ingredients & Quantity" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    alertx.tag=102;
   // UIAlertViewStyleLoginAndPasswordInput
    [alertx setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    [alertx textFieldAtIndex:1].secureTextEntry = NO;
    [alertx textFieldAtIndex:0].autocapitalizationType = UITextAutocapitalizationTypeNone;
    [alertx textFieldAtIndex:1].autocapitalizationType = UITextAutocapitalizationTypeNone;
    [alertx textFieldAtIndex:0].keyboardType = UIKeyboardTypeDefault;
    [alertx textFieldAtIndex:1].keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [alertx textFieldAtIndex:0].returnKeyType = UIReturnKeyNext;
    [alertx textFieldAtIndex:1].returnKeyType = UIReturnKeyDone;
    [alertx textFieldAtIndex:0].delegate=(id)self;
    [alertx textFieldAtIndex:1].delegate=(id)self;
    [alertx textFieldAtIndex:0].placeholder=@"Enter Ingredient";
    [alertx textFieldAtIndex:1].placeholder=@"Enter Quantity (gms,kgs,pcs)";
    [alertx textFieldAtIndex:0].tintColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    [alertx textFieldAtIndex:1].tintColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    [alertx textFieldAtIndex:0].tag=1;
    [alertx textFieldAtIndex:1].tag=2;
    [alertx show];
   
}
-(void)webserviceaddingradientstouser
{
    NSString *path =[NSString stringWithFormat:@"%@855",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",sendmyowningredient,@"ingredientsname",nil];
  //  NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",selectedingradient,@"ingredientsname",selectedquantity,@"measurement",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Adding Ingredient.."];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dictlocal = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         //here is place for code executed in success case
         if([[dictlocal valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
         }
         else
         {
             loadermaintain=TRUE;
             [self webserviceuserreciepelisting];
            
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        if(buttonIndex==1)
        {
           
            NSInteger section =  self.tracksectiondelete.tag - 1000;
            term_taxonomy_id=[[NSString alloc]init];
            term_id=[[NSString alloc]init];
            recipeid=[[NSString alloc]init];
            recipeid=[NSString stringWithFormat:@"%@",[adminrecipeid objectAtIndex:section]];
            term_taxonomy_id=[NSString stringWithFormat:@"%@",[term_taxonomy_idarray objectAtIndex:section]];
            term_id=[NSString stringWithFormat:@"%@",[term_idarray objectAtIndex:section]];
            [self deleteshoppinglistsectionwebservice: self.tracksectiondelete];
        }
    }
    
    else if(alertView.tag==101)
    {
        if(buttonIndex==1)
        {
            //newcode
            useringid=[[NSString alloc]init];
            useringid=[NSString stringWithFormat:@"%@",[[[ingredientidarray objectAtIndex:indexPathdeleterow.section] objectAtIndex:indexPathdeleterow.row] valueForKey:@"id"]];
            // new code
            [self webservicedeleteuseringradients:indexPathdeleterow];
        }
    }
    
    else if(alertView.tag==102)
    {
        if (buttonIndex==1)
        {
            NSString *trimmedString = [[[alertView textFieldAtIndex:0] text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
             NSString *trimmedString1 = [[[alertView textFieldAtIndex:1] text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if ([trimmedString length]>0&&[trimmedString1 length]>0)
            {
                selectedingradient=trimmedString;
                selectedquantity=trimmedString1;
                sendmyowningredient=[NSString stringWithFormat:@"%@-%@",selectedingradient,selectedquantity];
                [self webserviceaddingradientstouser];
                
            }
            else
            {
            [[alertView textFieldAtIndex:0] resignFirstResponder];
            [[alertView textFieldAtIndex:1] resignFirstResponder];
            alertx=[[UIAlertView alloc]initWithTitle:@"! Required" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertx show];
               
            }
        }
    }
    else if(alertView.tag==103)
    {
        [self back];
    }
    else if(alertView.tag==104)
    {
        if(buttonIndex==1)
        {
            NSLog(@"%ld",(long)indexPathdeleterow.section);
            NSLog(@"%ld",(long)indexPathdeleterow.row);
            term_taxonomy_id=[[NSString alloc]init];
            term_id=[[NSString alloc]init];
            term_taxonomy_id=[NSString stringWithFormat:@"%@",[term_taxonomy_idarray objectAtIndex:indexPathdeleterow.section]];
            term_id=[NSString stringWithFormat:@"%@",[term_idarray objectAtIndex:indexPathdeleterow.section]];
            ingredientid=[NSString stringWithFormat:@"%@",[[[ingredientidarray objectAtIndex:indexPathdeleterow.section] objectAtIndex:indexPathdeleterow.row] valueForKey:@"ingredientid"]];
            recipeid=[NSString stringWithFormat:@"%@",[adminrecipeid objectAtIndex:indexPathdeleterow.section]];
            [self deleteshoppinglistsectionwebservicerow:indexPathdeleterow];
        }
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO;
}
@end
