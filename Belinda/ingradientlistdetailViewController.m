//
//  ingradientlistdetailViewController.m
//  Belinda
//
//  Created by Nivendru on 21/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "ingradientlistdetailViewController.h"

@interface ingradientlistdetailViewController ()

@end

@implementation ingradientlistdetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:220/255.0f green:25/255.0f blue:124/255.0f alpha:1.0f];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.title=@"Ingredient List";
    
    // Hide navigation bar and customize
    self.navigationItem.hidesBackButton=YES;
    [self setCustomNavigationButton];
    if(_ingradientArray)
    {
        [self.aTableview reloadData];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    // new code
    hiderevealButton=[[UIButton alloc]initWithFrame:self.view.bounds];
    [hiderevealButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    hiderevealButton.backgroundColor=[UIColor clearColor];
    [self.view addSubview:hiderevealButton];
    [hiderevealButton setHidden:YES];
}
-(void)back
{
    NSLog(@"Back");
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(void)setCustomNavigationButton
{
    
    UIButton *Menubutton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 28)];
    UIImageView *buttonImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 20, 18)];
    buttonImage.image=[UIImage imageNamed:@"menu_icon.png" ];
    buttonImage.clipsToBounds=YES;
    buttonImage.contentMode=UIViewContentModeScaleAspectFit;
    [Menubutton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [Menubutton addSubview:buttonImage];
    UIBarButtonItem *menubtn=[[UIBarButtonItem alloc]initWithCustomView:Menubutton];
    self.navigationItem.leftBarButtonItem=menubtn;
    
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 26, 26)];
    [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 6, 18, 18)];
    buttonimageview.image=[UIImage imageNamed:@"back_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    
    UIBarButtonItem *backbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombutton];
    backbarbutton.style = UIBarButtonItemStylePlain;
    // end
    self.navigationItem.rightBarButtonItems = @[backbarbutton];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"Cell";
    Ingradientcell *cell=(Ingradientcell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    
    if(cell==nil)
    {
        cell=[[Ingradientcell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    
    cell.seperatorLabel.frame=CGRectMake(0, cell.seperatorLabel.frame.origin.y, self.view.frame.size.width, cell.seperatorLabel.frame.size.height);
    cell.quantity.frame=CGRectMake(self.aTableview.frame.size.width-90, cell.quantity.frame.origin.y, cell.quantity.frame.size.width, cell.quantity.frame.size.height);
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    NSString *ingradient = [[_ingradientArray objectAtIndex:indexPath.row] valueForKey:@"name"];
    NSString *quantity = [[_ingradientArray objectAtIndex:indexPath.row] valueForKey:@"measurement"];
    cell.ingradientnamelabel.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    cell.ingradientnamelabel.text = ingradient;
    cell.quantity.text = quantity;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 50.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_ingradientArray count];
   
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
