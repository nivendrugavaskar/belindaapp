//
//  reciepeViewCell.h
//  Belinda
//
//  Created by Nivendru on 10/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol recipecellprotocol
@end

@interface reciepeViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *textlabel;
@property (assign) id <recipecellprotocol> delegate;

@end
