//
//  MyInfoViewController.m
//  Belinda
//
//  Created by Nivendru on 13/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "MyInfoViewController.h"

@interface MyInfoViewController ()
{
    CGRect screenBounds;
    AppDelegate *app;
    UITapGestureRecognizer *tap;
    NSDictionary *dict;
    NSString *imagepath,*specialnotificationstatus,*goalnotificationstatus;
    CGSize kbsize,contentsize;
}

@end

@implementation MyInfoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    // new code
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    //end
    
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:220/255.0f green:25/255.0f blue:124/255.0f alpha:1.0f];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.title=@"My Info";
    
    // Hide navigation bar and customize
    self.navigationItem.hidesBackButton=YES;
    [self setCustomNavigationButton];
    // by default look
    [self didtapoffspecialnotification:nil];
    [self didtapoffgoalnotification:nil];
    
#pragma Condition maintain for logintype
    if([app->loginType isEqualToString:@"FBlogin"])
    {
        _newpasswordtextfield.userInteractionEnabled=NO;
        _oldpasswordtextfield.userInteractionEnabled=NO;
        _confirmpasswordtextfield.userInteractionEnabled=NO;
        _mobileno.returnKeyType=UIReturnKeyDone;
    }
    else
    {
        _newpasswordtextfield.userInteractionEnabled=YES;
        _oldpasswordtextfield.userInteractionEnabled=YES;
        _confirmpasswordtextfield.userInteractionEnabled=YES;
        _mobileno.returnKeyType=UIReturnKeyNext;
    }
    
    // new code
    [self webservicemyfoget];
    
    
}
-(void)webservicemyfoget
{
    NSString *path =[NSString stringWithFormat:@"%@1115",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
   
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Fetching Your Info.." maskType:SVProgressHUDMaskTypeNone];
    //[SVProgressHUD showWithStatus:@"Please wait a moment..."];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
            
            
         }
         else
         {
             [self loaddata];
         }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}
-(void)loaddata
{
    NSLog(@"Response dictionary=%@",dict);
    _emailshow.text=[[dict valueForKey:@"result"] valueForKey:@"user_email"];
    _fullname.text=[[dict valueForKey:@"result"] valueForKey:@"user_full_name"];
    _mobileno.text=[[dict valueForKey:@"result"] valueForKey:@"user_mobileno"];
    if([[[dict valueForKey:@"result"] valueForKey:@"special_notification"]isEqualToString:@"1"])
    {
        [self didtaponspecialnotification:nil];
    }
    else
    {
        [self didtapoffspecialnotification:nil];
    }
    if([[[dict valueForKey:@"result"] valueForKey:@"goal_notification"]isEqualToString:@"1"])
    {
        [self didtapongoalnotification:nil];
    }
    else
    {
        [self didtapoffgoalnotification:nil];
    }
    imagepath=[[dict valueForKey:@"result"] valueForKey:@"user_profile_pic"];
    //new code for image load
    __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.color=[UIColor colorWithRed:255/255.0f green:105/255.0f blue:180/255.0f alpha:1.0f];
    activityIndicator.center = CGPointMake(CGRectGetMidX(_uploadimage.bounds), CGRectGetMidY(_uploadimage.bounds));
    activityIndicator.hidesWhenStopped = YES;
    
    __block BOOL successfetchimage=FALSE;
    __weak UIImageView *setimage = _uploadimage;
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imagepath]];
    [_uploadimage setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"no_image.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         setimage.contentMode=UIViewContentModeScaleAspectFit;
         setimage.image=image;
         successfetchimage=TRUE;
         [activityIndicator removeFromSuperview];
     }
                                 failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
     {
         [activityIndicator removeFromSuperview];
     }];
    if(!successfetchimage)
    {
        if(![imagepath isEqualToString:@""]||imagepath)
        {
            [_uploadimage addSubview:activityIndicator];
            [activityIndicator startAnimating];
        }
    }
    // end
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    pointtrackcontentoffset=self.aScrollview.contentOffset;
    screenBounds = [[UIScreen mainScreen] bounds];
    if(screenBounds.size.height==480)
    {
        [self.aScrollview setContentSize:CGSizeMake(self.aScrollview.frame.size.width, self.aScrollview.frame.size.height+120)];
    }
    else if(screenBounds.size.height==568)
    {
        [self.aScrollview setContentSize:CGSizeMake(self.aScrollview.frame.size.width, self.aScrollview.frame.size.height+40)];
    }
    else if(screenBounds.size.height==667)
    {
        
       
    }
    else if(screenBounds.size.height==736)
    {
     // [self.aScrollview setContentSize:CGSizeMake(self.aScrollview.frame.size.width, self.aScrollview.frame.size.height+40)];
    }
    else
    {
       
    }
    // new code
    contentsize=self.aScrollview.contentSize;
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    // new code
    hiderevealButton=[[UIButton alloc]initWithFrame:self.view.bounds];
    [hiderevealButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    hiderevealButton.backgroundColor=[UIColor clearColor];
    [self.view addSubview:hiderevealButton];
    [hiderevealButton setHidden:YES];
    
    // set custom properties
    self.innerviewscrollviewimagecontentview.layer.borderWidth=3.0;
    self.innerviewscrollviewimagecontentview.layer.borderColor=[[UIColor whiteColor] CGColor];
    
    for (id v in self.aScrollview.subviews)
    {
        if([v isKindOfClass:[UILabel class]])
        {
            UILabel *label=(UILabel *)v;
            label.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
        }
        else  if([v isKindOfClass:[UITextField class]])
        {
            UITextField *typecast=(UITextField *)v;
            typecast.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
            [typecast setValue:[UIColor lightTextColor] forKeyPath:@"_placeholderLabel.textColor"];
            typecast.tintColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];

        }
    }
}
-(void)setCustomNavigationButton
{
    UIButton *Menubutton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 28)];
    UIImageView *buttonImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 20, 18)];
    buttonImage.image=[UIImage imageNamed:@"menu_icon.png" ];
    buttonImage.clipsToBounds=YES;
    buttonImage.contentMode=UIViewContentModeScaleAspectFit;
    [Menubutton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    // set swrevealcontroller delegate
    self.revealViewController.delegate =self;
    [Menubutton addSubview:buttonImage];
    UIBarButtonItem *menubtn=[[UIBarButtonItem alloc]initWithCustomView:Menubutton];
    self.navigationItem.leftBarButtonItem=menubtn;
    
}

#pragma swrevealview delegate
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    [self dismisskeyboard];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    kbsize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [self.aScrollview setContentSize:CGSizeMake(self.view.bounds.size.width, self.aScrollview.frame.origin.y+self.aScrollview.frame.size.height+60+kbsize.height)];
    
    
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.aScrollview setContentOffset:pointtrackcontentoffset animated:NO];
    [self.view removeGestureRecognizer:tap];
    [self.aScrollview setContentSize:contentsize];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
    pointtrack = textField.frame.origin;
    // new code
    pointtrackcontentoffset=self.aScrollview.contentOffset;
    
    NSLog(@"%f",screenBounds.size.height);
    // dismiss when outside click
    tap = [[UITapGestureRecognizer alloc]
           initWithTarget:self
           action:@selector(dismisskeyboard)];
    tap.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:tap];
    
    if(pointtrack.y>350)
    {
        pointtrack.y=320;
    }
    if(screenBounds.size.height==480)
    {
        if(pointtrack.y>70)
        {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-70) animated:YES];
        }
    }
    else if(screenBounds.size.height==568)
    {
        if(pointtrack.y>160)
        {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-160) animated:YES];
        }
    }
    else if(screenBounds.size.height==667)
    {
        if(pointtrack.y>200)
        {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-200) animated:YES];
        }
    }
    else if(screenBounds.size.height==736)
    {
        if(pointtrack.y>220)
        {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-220) animated:YES];
        }
    }
    else
    {
        if(pointtrack.y>327)
        {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-327) animated:YES];
        }
    }
    
}
-(void)dismisskeyboard
{
    [self.activeTextField resignFirstResponder];
    [self.view removeGestureRecognizer:tap];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
#pragma code in case of facebook
    if([app->loginType isEqualToString:@"FBlogin"])
    {
        if(self.activeTextField==_mobileno)
        {
            nextResponder=nil;
        }
    }
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        [self.aScrollview setContentOffset:pointtrackcontentoffset animated:NO];
        [textField resignFirstResponder];
    }
    //[self.activeTextField resignFirstResponder];
    return NO; // We do not want UITextField to insert line-breaks.
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didTappicchangebutton:(id)sender
{
    [self dismisskeyboard];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Upload Image \nFrom:" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Gallery", nil];
    alert.tag=100;
    [alert show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==100)
    {
        if(buttonIndex==0)
        {
            //_uploadimage.image=[UIImage imageNamed:@"no_image.png"];
        }
        if(buttonIndex==1)
        {
            [self takepicture];
        }
        else if(buttonIndex==2)
        {
            [self selectgallery];
        }
    }
    if(alertView.tag==101)
    {
        [self performSegueWithIdentifier:@"seguetomainviewcontroller" sender:self];
    }
}
-(void)selectgallery
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    ImagePicker=[[UIImagePickerController alloc]init];
    ImagePicker.delegate=(id)self;
    ImagePicker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    // imagepic.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:ImagePicker animated:YES completion:nil];
}
-(void)takepicture
{
    /******************** code for camera ***********************/
    
    ImagePicker=[[UIImagePickerController alloc]init];
    if    (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
    }
    else
    {
        
        ImagePicker.delegate=(id)self;
        ImagePicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        // imagepic.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:ImagePicker animated:YES completion:nil];
        NSLog(@"camera");
    }
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    // new code
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [picker dismissViewControllerAnimated:YES completion:nil];
    // [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    selectedimage=[info objectForKey:UIImagePickerControllerOriginalImage];
    selectedimage=[self scaleAndRotateImage:selectedimage];
    
    _uploadimage.contentMode=UIViewContentModeScaleAspectFit;
    _uploadimage.image=selectedimage;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
// new method
- (UIImage *)scaleAndRotateImage:(UIImage *)image
{
    int kMaxResolution = 640; // Or whatever
    CGImageRef imgRef = image.CGImage;
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

- (IBAction)didtapoffspecialnotification:(id)sender
{
    specialnotificationstatus=@"0";
    [self.activeTextField resignFirstResponder];
    _offbuttonspecialnotification.backgroundColor=[UIColor colorWithRed:26/255.0f green:24/255.0f blue:24/255.0f alpha:1.0f];
    _onbuttonspecialnotification.backgroundColor=[UIColor whiteColor];
}
- (IBAction)didtaponspecialnotification:(id)sender
{
     specialnotificationstatus=@"1";
     [self.activeTextField resignFirstResponder];
    _offbuttonspecialnotification.backgroundColor=[UIColor whiteColor];
    _onbuttonspecialnotification.backgroundColor=[UIColor colorWithRed:26/255.0f green:24/255.0f blue:24/255.0f alpha:1.0f];
}

- (IBAction)didtapoffgoalnotification:(id)sender
{
     goalnotificationstatus=@"0";
     [self.activeTextField resignFirstResponder];
    _offbuttongoalnotification.backgroundColor=[UIColor colorWithRed:26/255.0f green:24/255.0f blue:24/255.0f alpha:1.0f];
    _onbuttongoalnotification.backgroundColor=[UIColor whiteColor];
}
- (IBAction)didtapongoalnotification:(id)sender
{
     goalnotificationstatus=@"1";
     [self.activeTextField resignFirstResponder];
    _offbuttongoalnotification.backgroundColor=[UIColor whiteColor];
    _onbuttongoalnotification.backgroundColor=[UIColor colorWithRed:26/255.0f green:24/255.0f blue:24/255.0f alpha:1.0f];
}
// dynamic method
-(BOOL)validation:(NSString *)sendstring
{
    NSString *trimmed=sendstring;
    trimmed=[trimmed stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if([trimmed length]==0)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

- (IBAction)didTapupdate:(id)sender
{
     [self.activeTextField resignFirstResponder];
        
        if(![self validation:_fullname.text])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please Fill Your Name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        else if(![self validation:_mobileno.text])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please Fill Mobile Number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
//        else if(![self validation:_oldpasswordtextfield.text])
//        {
//            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please Fill Old Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
//            return;
//        }
//        else if(![self validation:_newpasswordtextfield.text])
//        {
//            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please Fill New Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
//            return;
//        }
//        else if(![self validation:_confirmpasswordtextfield.text])
//        {
//            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please Fill Confirm Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
//            return;
//        }
    if(![app->loginType isEqualToString:@"FBlogin"])
    {
      if(![_newpasswordtextfield.text isEqualToString:_confirmpasswordtextfield.text])
      {
          UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please Fill Correct Confirm Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
          [alert show];
          return;
      }
        else
        {
        [self webserviceupdatemyinfo];
        }
    }
    else
    {
        [self webserviceupdatemyinfo];
    }
    
}
-(void)webserviceupdatemyinfo
{
     //userid,user_full_name,user_mobileno,goal_notification,special_notification,new_password
    
    NSString *path =[NSString stringWithFormat:@"%@1106",app->parentUrl];
    // AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://server.url"]];
    // AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Updating Your Info.."];
    imageDataPicker=(NSData *)UIImagePNGRepresentation(selectedimage);
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:app->userid,@"userid",_fullname.text,@"user_full_name",_mobileno.text,@"user_mobileno",_oldpasswordtextfield.text,@"old_password",_newpasswordtextfield.text,@"new_password",specialnotificationstatus,@"special_notification",goalnotificationstatus,@"goal_notification",nil];
    AFHTTPRequestOperation *op = [manager POST:path parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //do not put image inside parameters dictionary as I did, but append it!
        if(imageDataPicker)
        {
            [formData appendPartWithFileData:imageDataPicker name:@"image" fileName:@"photo.png" mimeType:@"image/png"];
        }
    }
    success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
        NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",responseStr);
        NSDictionary *dict1 = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
        NSLog(@"%@",dict1);
        //here is place for code executed in success case
        if([[dict1 valueForKey:@"status"]integerValue]==0)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict1 valueForKey:@"error"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            [SVProgressHUD dismiss];
        }
        else
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict1 valueForKey:@"result"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            alert.tag=101;
            //
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            [SVProgressHUD dismiss];
        }
        
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                  
    {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
        NSLog(@"Error: %@", [error localizedDescription]);
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    ];
    [op start];
    }


@end
