//
//  Resultlistcell.h
//  Belinda
//
//  Created by Nivendru on 18/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Resultlistcell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *goalname;
@property (strong, nonatomic) IBOutlet UILabel *separatorlabel;
@property (strong, nonatomic) IBOutlet UIImageView *startimage;

@end
