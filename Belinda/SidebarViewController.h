//
//  SidebarViewController.h
//  SidebarDemo
//
//  Created by Simon on 29/6/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
#import "AppDelegate.h"

@interface SidebarViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate,RowSelectionDelegate,UIAlertViewDelegate>

@property (strong,nonatomic) UITableView *activetableview;

@end
