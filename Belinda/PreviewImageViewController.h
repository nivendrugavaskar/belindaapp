//
//  PreviewImageViewController.h
//  Belinda
//
//  Created by Nivendru on 20/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface PreviewImageViewController : UIViewController<UIDocumentInteractionControllerDelegate>
{
    AppDelegate *app;
}

@property (strong, nonatomic) IBOutlet UIImageView *bigImageView;

@property(strong,nonatomic)NSString *previewimagestring,*imagetype;
- (IBAction)didTapdone:(id)sender;
- (IBAction)didtapshare:(id)sender;

@end
