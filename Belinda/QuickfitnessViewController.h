//
//  QuickfitnessViewController.h
//  Belinda
//
//  Created by Nivendru on 06/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@protocol goallistingprotocol <NSObject>
-(void)sendflagtrue:(BOOL)flag;
@end

@interface QuickfitnessViewController : UIViewController<UITextFieldDelegate,UIAlertViewDelegate,UIImagePickerControllerDelegate,UIToolbarDelegate,SWRevealViewControllerDelegate>
{
    AppDelegate *app;
    CGRect screenBounds;
    CGPoint pointtrack,pointtrackcontentoffset;
    UIImagePickerController *ImagePicker;
    UIImage *selectedimage;
    NSData *imageDataPicker;
    UIDatePicker *datePicker;
    UIAlertView *alertx;
}

@property (strong, nonatomic) IBOutlet UILabel *goaltypelabel;
@property (strong,nonatomic) UITextField *activeTextField;
@property (strong, nonatomic) IBOutlet UITextField *whatisyourgoaltextfield;
- (IBAction)didTapsubmit:(id)sender;
- (IBAction)didTaptakepicture:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *uploadimage;
@property (strong, nonatomic) IBOutlet UIView *imagecontainerview;
- (IBAction)didTapweekly:(id)sender;
- (IBAction)didTapmonthly:(id)sender;
- (IBAction)didTapexercise:(id)sender;
- (IBAction)didTapFood:(id)sender;
- (IBAction)didTaplifestyle:(id)sender;
- (IBAction)didTappersonal:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *weeklybutton;
@property (strong, nonatomic) IBOutlet UIButton *monthlybutton;
@property (strong, nonatomic) IBOutlet UIButton *exercisebutton;
@property (strong, nonatomic) IBOutlet UIButton *foodbutton;
@property (strong, nonatomic) IBOutlet UIButton *lifestylebutton;
@property (strong, nonatomic) IBOutlet UIButton *personalbutton;
@property (strong, nonatomic) IBOutlet UIView *createnewgoalview;
@property (strong, nonatomic) IBOutlet UIView *goaltypeview;
- (IBAction)didTapstartdate:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *startdatelabel;
@property (strong, nonatomic) IBOutlet UILabel *enddatelabel;
@property (strong, nonatomic) IBOutlet UITextField *goalname;
@property (strong, nonatomic) IBOutlet UIButton *startdatebutton;
@property (strong, nonatomic) IBOutlet UIButton *submitbtn;
@property (strong, nonatomic) IBOutlet UIButton *camerabtn;
@property (strong, nonatomic) IBOutlet UIView *lastimagecontainerview;


// new code
@property(strong,nonatomic) NSMutableArray *specificgoaldata;
@property (strong, nonatomic) IBOutlet UILabel *picturestatuslabel;

//// new code for testing
@property (assign) id <goallistingprotocol> goallistingdelegate;
@end
