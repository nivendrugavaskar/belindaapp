//
//  RecipedetailViewController.h
//  Belinda
//
//  Created by Nivendru on 11/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ingradientlistdetailViewController.h"
#import "HowtoMakeViewController.h"


@interface RecipedetailViewController : UIViewController
{
    
}
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
- (IBAction)didTapingradient:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *ingradientbutton;
- (IBAction)didTapHowtomake:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *howtomakebutton;

@property(strong,nonatomic) NSMutableArray *recipedescarray;
@property(strong,nonatomic) NSMutableDictionary *fulldetailarraydesc;
@property (strong, nonatomic) IBOutlet UIImageView *recipepic;



@end
