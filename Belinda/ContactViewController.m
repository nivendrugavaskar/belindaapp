//
//  ContactViewController.m
//  Belinda
//
//  Created by Nivendru on 10/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import "ContactViewController.h"

@interface ContactViewController ()
{
    NSString *address,*phone,*email;
}

@end

@implementation ContactViewController
@synthesize coordinate;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    [self webservicecontactus];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"Contact";
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:220/255.0f green:25/255.0f blue:124/255.0f alpha:1.0f];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    // Hide navigation bar and customize
    self.navigationItem.hidesBackButton=YES;
    [self setCustomNavigationButton];
    
        // new code
    hiderevealButton=[[UIButton alloc]initWithFrame:self.view.bounds];
    [hiderevealButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    hiderevealButton.backgroundColor=[UIColor clearColor];
    [self.view addSubview:hiderevealButton];
    [hiderevealButton setHidden:YES];
}

-(void)setCustomNavigationButton
{
    
    UIButton *Menubutton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 28)];
    UIImageView *buttonImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 20, 18)];
    buttonImage.image=[UIImage imageNamed:@"menu_icon.png" ];
    buttonImage.clipsToBounds=YES;
    buttonImage.contentMode=UIViewContentModeScaleAspectFit;
    [Menubutton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [Menubutton addSubview:buttonImage];
    UIBarButtonItem *menubtn=[[UIBarButtonItem alloc]initWithCustomView:Menubutton];
    self.navigationItem.leftBarButtonItem=menubtn;
    
}
-(void)loadhud
{
    [SVProgressHUD showWithStatus:@"Fetching Contact Details.." maskType:SVProgressHUDMaskTypeClear];
}
-(void)webservicecontactus
{
    NSString *path =[NSString stringWithFormat:@"%@659",app->parentUrl];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
     [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [self performSelectorInBackground:@selector(loadhud) withObject:self];
    [manager POST:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"status"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             [SVProgressHUD dismiss];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];

         }
         else
         {
              address=[[dict valueForKey:@"result"] valueForKey:@"address"];
              phone=[[dict valueForKey:@"result"] valueForKey:@"phone"];
              email=[[dict valueForKey:@"result"] valueForKey:@"email"];
             _contactaddresstextview.text=address;
             _phonetextview.text=phone;
             _emailtextview.text=email;
             
             // new code to show address on map
             //2/114 Canterbury Rd, Kilsyth South 3137
             CLGeocoder *geocoder = [[CLGeocoder alloc] init];
             [geocoder geocodeAddressString:address
                          completionHandler:^(NSArray* placemarks, NSError* error){
                              if (placemarks && placemarks.count > 0) {
                                  CLPlacemark *topResult = [placemarks objectAtIndex:0];
                                  MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
                                  MKCoordinateRegion region = self.mapview.region;
                                  region.center = [(CLCircularRegion *)placemark.region center];
                                  region.span.longitudeDelta /= 1000.0;
                                  region.span.latitudeDelta /= 1000.0;
                                  [self.mapview setRegion:region animated:YES];
                                  [self.mapview addAnnotation:placemark];
                                  //[self.mapview selectAnnotation:placemark animated:YES];
                                  //[self mapView:self.mapview didAddAnnotationViews:placemarks];
                                  
                                  [SVProgressHUD dismiss];
                                  [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                              }
                          }
              ];
             
             // end
             
            

         }
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [SVProgressHUD dismiss];
          [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}

//- (void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views
//{
//    MKAnnotationView *annotationView = [views objectAtIndex:0];
//    id<MKAnnotation> mp = [annotationView annotation];
//    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([mp coordinate] ,350,350);
//    
//    [mv setRegion:region animated:YES];
//    
//    [self.mapview selectAnnotation:mp animated:YES];
//    
//}

//- (void)webViewDidFinishLoad:(UIWebView *)webView {
//    if ([[webView stringByEvaluatingJavaScriptFromString:@"document.readyState"] isEqualToString:@"complete"])
//    {
//        // UIWebView object has fully loaded.
//         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
//        [SVProgressHUD dismiss];
//    }
//}
//-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
//{
//    NSLog(@"Error for WEBVIEW: %@", [error description]);
//     [[UIApplication sharedApplication] endIgnoringInteractionEvents];
//    [SVProgressHUD dismiss];
//}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didTapmap:(id)sender
{
  //  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?q=%@",address]]];
//    UIApplication *app1 = [UIApplication sharedApplication];
//    NSString *showaddress=[NSString stringWithFormat:@"http://maps.google.com/maps?q=%@",[address stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
//    [app1 openURL:[NSURL URLWithString:showaddress]];
    //[app1 openURL:[NSURL URLWithString: @"http://maps.google.com/maps?q=2/114,Canterbury Rd,KilsythSouth3137"]];
}
- (IBAction)didTapphone:(id)sender
{
    NSString *phoneNumber=[phone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"%@",phoneNumber);
    phoneNumber=[phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", phoneNumber]]];
}
- (IBAction)didTapemail:(id)sender
{
}
@end
